CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Inquiry_Report` AS
    select 
        `Inquiry`.`mongo_id` AS `Host Inquiry ID`,
        `Host`.`mongo_id` AS `Host Mongo ID`,
        `Host`.`record_id` AS `Host Record ID`,
        `Inquiry`.`first_name` AS `First Name`,
        `Inquiry`.`last_name` AS `Last Name`,
        `Inquiry`.`street` AS `Street Address`,
        `Inquiry`.`city` AS `City`,
        `Inquiry`.`state` AS `State`,
        `Inquiry`.`zip` AS `Zip Code`,
        `Inquiry`.`phone` AS `Phone`,
        `Inquiry`.`email` AS `Email`,
        `Inquiry`.`created_date` AS `Create Date`,
        `Inquiry`.`Nearby_Schools_Blob` AS `Nearby Schools`,
        `Host`.`harmony_page_link` AS `Harmony Record URL`,
        concat('https://www.ciiedu.net/gp.html#/public/hostFamilyApp/householdMembers?inquiryId=',
                `Inquiry`.`mongo_id`) AS `Application Form URL`,
        concat(`School`.`HRC_first_name`,
                `School`.`HRC_last_name`) AS `HRC Full Name Connected School`,
        `Host`.`status` AS `Host Complicance Status`,
        `Host`.`season_status` AS `Host Season Status`,
        `Host`.`latest_season_status` AS `Host Latest Season Status`,
        floor(((length(`Host_Season`.`Matched_Students`) - length(replace(`Host_Season`.`Matched_Students`,
                            'SELECTED',
                            ''))) / 8)) AS `Number of Currently Hosting Students`,
        `Host_Season`.`Matched_Students` AS `Currently Hosting Students`,
        `Host`.`Created_On` AS `Application Date`,
        `Host`.`initial_approval_date` AS `Approval Date(Initial)`,
        `Host`.`Initial_Matched_Date` AS `Matched Date(Initial)`,
        `School`.`short_name` AS `Connected School Short Name`,
        `School`.`region` AS `Connected School Region`,
        `Host`.`how_hear_of_us` AS `How Did You Hear About Us`,
        `Host`.`how_hear_of_us_detail` AS `How Did You Hear About Us Detail`,
        `Host`.`gp_portal_link` AS `Host Portal URL`,
        `Inquiry`.`Default_Hrc_Name` AS `HRC Name Nearby School`
    from
        (((`Inquiry`
        left join `Host` ON ((`Inquiry`.`mongo_id` = `Host`.`inquiry_id`)))
        left join `Host_Season` ON (((`Host`.`mongo_id` = `Host_Season`.`host_mongo_id`)
            and (`Host_Season`.`school_year` = '2017/18'))))
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))