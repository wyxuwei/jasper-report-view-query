CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_ADLA_Training_Status_Report` AS
    select 
        `Student_To_Host`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`chinese_name` AS `Student Chinese Name`,
        `Student`.`english_name` AS `Student English Name`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`diocese_name` AS `Diocese`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `School`.`region` AS `School Region`,
        `Student_To_Host`.`school_year` AS `School Year`,
        `Host`.`primary_contact_name` AS `Primary Host Family name`,
        concat(`Host_Member`.`First_Name`,
                ' ',
                `Host_Member`.`Last_Name`) AS `Host Member Name`,
        `Host_Season`.`status` AS `Host Family Season Status`,
        if((`Host`.`Virtus_Training_Completed` = '1'),
            'Yes',
            'No') AS `Virtus Training Complete (Y/N)`,
        `Host_Member`.`Virtus_Training_Completion_Date` AS `Virtus Training Completion Date`,
        `Host_Member`.`Virtus_Training_Expiration_Date` AS `Virtus Expiration Date`,
        `Student_To_Host`.`status` AS `Host Matching Status`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`
    from
        (((((`Student_To_Host`
        left join `School` ON ((`Student_To_Host`.`student_enrolled_school_mongo_id` = `School`.`mongo_id`)))
        left join `Student` ON ((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Host_Season` ON (((`Student_To_Host`.`host_mongo_id` = `Host_Season`.`host_mongo_id`)
            and (`Student_To_Host`.`school_year` = `Host_Season`.`school_year`))))
        left join `Host` ON ((`Student_To_Host`.`host_mongo_id` = `Host`.`mongo_id`)))
        left join `Host_Member` ON (((`Host`.`mongo_id` = `Host_Member`.`Host_Mongo_Id`)
            and (`Host_Member`.`Age` >= 18))))
    where
        ((`Student_To_Host`.`status` in ('SELECTED' , 'TEMPORARY'))
            and (`School`.`diocese_name` in ('CA-Archdiocese of LA' , 'CA-Archdiocese LA Affiliated')))