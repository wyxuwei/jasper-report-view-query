CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Enrolling` AS
    select 
        `Host`.`primary_contact_name` AS `primary_contact_name`,
        `Host`.`state` AS `state`,
        `Host`.`city` AS `city`,
        `Host`.`status` AS `1`,
        `School`.`short_name` AS `short_name`,
        `School`.`full_name` AS `full_name`,
        `School`.`SSC_first_name` AS `SSC_first_name`,
        `School`.`SSC_last_name` AS `SSC_last_name`,
        `School`.`HPC_first_name` AS `HPC_first_name`,
        `School`.`HPC_last_name` AS `HPC_last_name`,
        `Host_Season`.`school_year` AS `school_year`,
        `Host_Season`.`host_type` AS `host_type`,
        `Host_Season`.`status` AS `2`,
        `Host_Season`.`do_we_want_the_host` AS `do_we_want_the_host`,
        `Host_Season`.`reason_we_dont_want_the_host` AS `reason_we_dont_want_the_host`,
        `Host_Season`.`does_host_want_to_host` AS `does_host_want_to_host`,
        `Host_Season`.`reason_host_doesnt_want_to_host` AS `reason_host_doesnt_want_to_host`,
    from
        ((`Host`
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))
        left join `Host_Season` ON ((`Host`.`mongo_id` = `Host_Season`.`host_mongo_id`)))