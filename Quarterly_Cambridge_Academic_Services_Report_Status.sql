CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Quarterly_Cambridge_Academic_Services_Report_Status` AS
    select 
        `Student_To_School`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        `Student`.`english_name` AS `Student English Name`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `Student_To_School`.`enrolled_grade` AS `Student Enrolled Grade`,
        `casr`.`Quarter` AS `Quarter Number`,
        `casr1`.`Submitted_Date` AS `1st Quarter Report Submitted Date`,
        `casr1`.`Submitter_Name` AS `1st Quarter Report Submitted By`,
        `casr1`.`Program_Type` AS `1st Quarter Report Program Type`,
        `casr1`.`Course_Name` AS `1st Quarter Report Course`,
        `casr1`.`Attendance_Percentage` AS `1st Quarter Report Attendance Rate`,
        `casr1`.`Accomplishments` AS `1st Quarter Report Overview of Student Accomplishment`,
        `casr1`.`Course_Average` AS `1st Quarter Report Course Average`,
        `casr1`.`Areas_And_Skills` AS `1st Quarter Report Areas Student Needs to Improvement`,
        `casr1`.`Additional_Tutoring` AS `1st Quarter Report Recommend Tutoring`,
        `casr2`.`Submitted_Date` AS `2nd Quarter Report Submitted Date`,
        `casr2`.`Submitter_Name` AS `2nd Quarter Report Submitted By`,
        `casr2`.`Program_Type` AS `2nd Quarter Report Program Type`,
        `casr2`.`Course_Name` AS `2nd Quarter Report Course`,
        `casr2`.`Attendance_Percentage` AS `2nd Quarter Report Attendance Rate`,
        `casr2`.`Accomplishments` AS `2nd Quarter Report Overview of Student Accomplishment`,
        `casr2`.`Course_Average` AS `2nd Quarter Report Course Average`,
        `casr2`.`Areas_And_Skills` AS `2nd Quarter Report Areas Student Needs to Improvement`,
        `casr2`.`Additional_Tutoring` AS `2nd Quarter Report Recommend Tutoring`,
        `casr3`.`Submitted_Date` AS `3rd Quester Report Submitted Date`,
        `casr3`.`Submitter_Name` AS `3rd Quester Report Submitted By`,
        `casr3`.`Program_Type` AS `3rd Quarter Report Program Type`,
        `casr3`.`Course_Name` AS `3rd Quarter Report Course`,
        `casr3`.`Attendance_Percentage` AS `3rd Quarter Report Attendance Rate`,
        `casr3`.`Accomplishments` AS `3rd Quarter Report Overview of Student Accomplishment`,
        `casr3`.`Course_Average` AS `3rd Quarter Report Course Average`,
        `casr3`.`Areas_And_Skills` AS `3rd Quarter Report Areas Student Needs to Improvement`,
        `casr3`.`Additional_Tutoring` AS `3rd Quarter Report Recommend Tutoring`,
        `casr4`.`Submitted_Date` AS `4th Quarter Report Submitted Date`,
        `casr4`.`Submitter_Name` AS `4th Quarter Report Submitted By`,
        `casr4`.`Program_Type` AS `4th Quarter Report Program Type`,
        `casr4`.`Course_Name` AS `4th Quarter Report Course`,
        `casr4`.`Attendance_Percentage` AS `4th Quarter Report Attendance Rate`,
        `casr4`.`Accomplishments` AS `4th Quarter Report Overview of Student Accomplishment`,
        `casr4`.`Course_Average` AS `4th Quarter Report Course Average`,
        `casr4`.`Areas_And_Skills` AS `4th Quarter Report Areas Student Needs to Improvement`,
        `casr4`.`Additional_Tutoring` AS `4th Quarter Report Recommend Tutoring`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`
    from
        (((((((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Cambridge_Academic_Services_Report` `casr` ON ((`Student_To_School`.`mongo_id` = `casr`.`Student_To_School_Mongo_Id`)))
        left join `Cambridge_Academic_Services_Report` `casr1` ON (((`Student_To_School`.`mongo_id` = `casr1`.`Student_To_School_Mongo_Id`)
            and (`casr1`.`Type` = 'Full')
            and (`casr1`.`Quarter` = 'First Quarter'))))
        left join `Cambridge_Academic_Services_Report` `casr2` ON (((`Student_To_School`.`mongo_id` = `casr2`.`Student_To_School_Mongo_Id`)
            and (`casr2`.`Type` = 'Full')
            and (`casr2`.`Quarter` = 'Second Quarter'))))
        left join `Cambridge_Academic_Services_Report` `casr3` ON (((`Student_To_School`.`mongo_id` = `casr3`.`Student_To_School_Mongo_Id`)
            and (`casr3`.`Type` = 'Full')
            and (`casr3`.`Quarter` = 'Third Quarter'))))
        left join `Cambridge_Academic_Services_Report` `casr4` ON (((`Student_To_School`.`mongo_id` = `casr4`.`Student_To_School_Mongo_Id`)
            and (`casr4`.`Type` = 'Full')
            and (`casr4`.`Quarter` = 'Fourth Quarter'))))
    where
        ((`Student_To_School`.`status` = 'ENROLLED')
            and (`School`.`is_academic` = 1))