CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Current_Recruitment_Season` AS
    select 
        (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end) AS `Current Recruitment Season`,
        (case
            when
                ((month(curdate()) >= 1)
                    and (dayofmonth(curdate()) > 15))
            then
                concat(year(curdate()), '_Fall')
            else concat(year(curdate()), '_Fall')
        end) AS `Current Recruitment Fall Season`,
        (case
            when
                (month(curdate()) >= 9)
            then
                concat(year(curdate()),
                        '/',
                        (right(year(curdate()), 2) + 1))
            else concat((year(curdate()) - 1),
                    '/',
                    right(year(curdate()), 2))
        end) AS `School Year`,
        concat(year(curdate()), '__Spring') AS `Current Year Spring Season`,
        concat(year(curdate()), '_Fall') AS `Current Year Fall Season`,
        concat((year(curdate()) - 1), '_Fall') AS `Last Year Fall Season`,
        (case
            when
                (month(curdate()) >= 9)
            then
                concat(year(curdate()),
                        '/',
                        (right(year(curdate()), 2) + 1))
            else concat((year(curdate()) - 1),
                    '/',
                    right(year(curdate()), 2))
        end) AS `Current School Year`,
        (case
            when
                (month(curdate()) >= 9)
            then
                concat((year(curdate()) + 1),
                        '/',
                        (right(year(curdate()), 2) + 2))
            else concat(year(curdate()),
                    '/',
                    (right(year(curdate()), 2) + 1))
        end) AS `Next School Year`,
        (case
            when (month(curdate()) >= 9) then concat(year(curdate()), '_Fall')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '_Fall')
            else concat(year(curdate()), '__Spring')
        end) AS `Current Academic Season`,
        (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat((year(curdate()) + 1), '__Spring')
            else concat(year(curdate()), '_Fall')
        end) AS `Current Student Recruitment Season`