CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Student_with_Host` AS
    select distinct
        `Student_To_Host`.`mongo_id` AS `mongo_id`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`english_name` AS `Student English Name`,
        `Student`.`gender` AS `Gender`,
        `Student`.`current_enrolled_grade` AS `Student Current Grade`,
        `Student`.`new_phone_number` AS `Student US Cell Number(Hidden)`,
        `Student`.`email` AS `Student Email(Hidden)`,
        `Student`.`wechat` AS `Student WeChat(Hidden)`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `Student_To_Host`.`status` AS `Host Status`,
        `Student_To_Host`.`season` AS `Hosting Season`,
        `Student_To_Host`.`start_date` AS `Hosting Start Date`,
        `Student_To_Host`.`end_date` AS `Hosting End Date`,
        `Host`.`mongo_id` AS `Host Family Mongo ID`,
        `Host`.`record_id` AS `Host Family ID`,
        `Host`.`primary_contact_name` AS `Primary Host Family Name`,
        `Host`.`another_member_name` AS `Secondary Host Family Name`,
        `Host`.`another_member_relationship` AS `The relationship between the primary and secondary host`,
        `Host`.`primary_email_address` AS `Primary Host 1st Email(Hidden)`,
        `Host`.`another_member_email` AS `Primary Host 2nd Email(Hidden)`,
        `Host`.`primary_phone` AS `Primary Host 1st Phone(Hidden)`,
        `Host`.`another_member_phone` AS `Primary Host 2nd Phone(Hidden)`,
        `Host`.`address` AS `Host Address(Hidden)`,
        `Host`.`city` AS `Host City`,
        `Host`.`state` AS `Host State`,
        `Host`.`zip` AS `Host Zip`,
        `Host_Season`.`host_type` AS `Host Type`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`
    from
        (((((`Student_To_Host`
        left join `Student` ON ((`Student`.`mongo_id` = `Student_To_Host`.`student_mongo_id`)))
        left join `Host` ON ((`Host`.`mongo_id` = `Student_To_Host`.`host_mongo_id`)))
        left join `Student_To_School` ON (((`Student_To_Host`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
            and (`Student_To_Host`.`school_year` = `Student_To_School`.`school_year`)
            and (`Student_To_School`.`status` = 'ENROLLED'))))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Host_Season` ON (((`Student_To_Host`.`host_mongo_id` = `Host_Season`.`host_mongo_id`)
            and (`Student_To_Host`.`school_year` = `Host_Season`.`school_year`))))
    where
        ((`Student_To_Host`.`status` = 'SELECTED')
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (not ((`School`.`short_name` like '%Test%'))))