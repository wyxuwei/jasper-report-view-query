CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Nearby_School_Host_Tracking` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`mailing_address` AS `School Mailing Address`,
        `School`.`region` AS `School Region`,
        `School`.`relationship_status` AS `School Relationship Status`,
        `School`.`partnership_type` AS `School Partnership Type`,
        `School`.`school_status` AS `School Partnership Status`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        `SR_Plan_with_Calculation`.`Recruitment Season` AS `Recruitment Season`,
        (case
            when
                (month(curdate()) < 9)
            then
                concat((year(curdate()) - 1),
                        '/',
                        right(year(curdate()), 2))
            else concat(year(curdate()),
                    '/',
                    (right(year(curdate()), 2) + 1))
        end) AS `Hosting Year`,
        `School`.`HPC_first_name` AS `HCC First Name`,
        `School`.`HPC_last_name` AS `HCC Last Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        `School`.`HRC_first_name` AS `HRC First Name`,
        `School`.`HRC_last_name` AS `HRC Last Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Regional Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        `School`.`current_approved_nearby_host_number` AS `Total Approved Hosts`,
        `School`.`Number_Of_Active_Nearby_Hosts_Current_School_Year` AS `Total Active Hosts For Current School Year`,
        `School`.`Open_Beds_From_Active_Nearby_Hosts_Current_School_Year` AS `Beds Available For Current School Year`,
        `School`.`Open_Beds_From_Active_Nearby_Hosts_Next_School_Year` AS `Beds Available For Next School Year`,
        `School`.`Number_Of_Estimated_Returning_Nearby_Hosts_Next_School_Year` AS `Estimated Returning Hosts`,
        `School`.`Number_Of_Confirmed_Returning_Nearby_Hosts_Next_School_Year` AS `Confirmed Returning Hosts`,
        `School`.`Number_Of_Confirmed_New_Nearby_Hosts_Next_School_Year` AS `Confirmed New Hosts`,
        (`School`.`Number_Of_Confirmed_Returning_Nearby_Hosts_Next_School_Year` + `School`.`Number_Of_Confirmed_New_Nearby_Hosts_Next_School_Year`) AS `Total Confirmed Hosts for Next School Year`,
        `School`.`Open_Beds_From_Confirmed_Returning_Nearby_Hosts_Next_School_Year` AS `Beds of Returning Hosts`,
        `School`.`Open_Beds_From_Confirmed_New_Nearby_Hosts_Next_School_Year` AS `Beds of New Hosts`,
        (`School`.`Open_Beds_From_Confirmed_Returning_Nearby_Hosts_Next_School_Year` - `School`.`Open_Beds_From_Confirmed_New_Nearby_Hosts_Next_School_Year`) AS `Total Confirmed Beds`,
        `School_Program_Size_By_Season`.`Number of New Enrolled Female Students` AS `New GP Enrolled Students - Female`,
        `School_Program_Size_By_Season`.`Number of New Enrolled Male Students` AS `New GP Enrolled Students - Male`,
        `School_Program_Size_By_Season`.`Total New Enrolled Students` AS `Total New GP Enrolled Students`,
        `School_Program_Size_By_Season`.`Number of Re-Enrolled Female Students` AS `GP Re-Enrolled Students - Female`,
        `School_Program_Size_By_Season`.`Number of Re-Enrolled Male Students` AS `GP Re-Enrolled Students - Male`,
        `School_Program_Size_By_Season`.`Total Re-Enrolled Students` AS `Total GP Re-enrolled Students`,
        `School_Program_Size_By_Season`.`Total Enrolled Students` AS `Total GP Enrolled Student (New + Re-enrolled)`,
        `SR_Plan_Spring`.`status` AS `Recruitment Season SR Plan Status`,
        `School`.`priority_level` AS `School Priority Level`,
        `School`.`cluster` AS `School Cluster`,
        `School`.`market_ability_x` AS `School Marketability (X)`,
        `School`.`market_ability_y` AS `School Marketability (Y)`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`
    from
        ((((((`School`
        left join `SR_Plan` `SR_Plan_Spring` ON (((`SR_Plan_Spring`.`school_mongo_id` = `School`.`mongo_id`)
            and (`SR_Plan_Spring`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            else concat(year(curdate()), '__Spring')
        end))
            and (`SR_Plan_Spring`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `SR_Plan` `SR_Plan_Fall` ON (((`SR_Plan_Fall`.`school_mongo_id` = `School`.`mongo_id`)
            and (`SR_Plan_Fall`.`season` = (case
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '_Fall')
            else concat(year(curdate()), '_Fall')
        end))
            and (`SR_Plan_Fall`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `SR_Plan_with_Calculation` ON (((`School`.`mongo_id` = `SR_Plan_with_Calculation`.`School Mongo ID`)
            and (`SR_Plan_with_Calculation`.`Recruitment Season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end)))))
        left join `School_Program_Size_By_Season` ON (((`School`.`mongo_id` = `School_Program_Size_By_Season`.`School Mongo ID`)
            and (`School_Program_Size_By_Season`.`Recruitment Season` = `SR_Plan_with_Calculation`.`Recruitment Season`))))
        left join `School_Program_Size_By_Season_Matched` ON (((`School`.`mongo_id` = `School_Program_Size_By_Season_Matched`.`School Mongo ID`)
            and (`School_Program_Size_By_Season_Matched`.`Recruitment Season` = `SR_Plan_with_Calculation`.`Recruitment Season`))))
        left join `Re_Enrollment_Summary_By_School` ON ((`School`.`mongo_id` = `Re_Enrollment_Summary_By_School`.`School Mongo ID`)))
    where
        ((`School`.`short_name` not in ('TS' , 'GS', 'TEST SCHOOL'))
            and (`School`.`school_status` in ('Active' , 'Terminated_Active'))
            and (`School`.`is_GP` = '1'))