CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `SR_Plan_with_Calculation` AS
    select 
        `SR_Plan`.`school_mongo_id` AS `School Mongo ID`,
        `SR_Plan`.`school_record_id` AS `School ID`,
        `SR_Plan`.`school_short_name` AS `School Short Name`,
        `SR_Plan`.`state` AS `School State`,
        `School`.`priority_level` AS `School Priority Level`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`region` AS `School Region`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `School`.`SSA_TL_first_name` AS `SSA Team Lead First Name`,
        `School`.`SSA_TL_last_name` AS `SSA Team Lead Last Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        `School`.`market_ability_x` AS `School Marketability (X)`,
        `School`.`market_ability_y` AS `School Marketability (Y)`,
        `School`.`cluster` AS `School Cluster Name`,
        `SR_Plan`.`season` AS `Recruitment Season`,
        if((`SR_Plan`.`academic_support` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if(((`SR_Plan`.`not_show_in_marketing` = '0')
                or isnull(`SR_Plan`.`not_show_in_marketing`)),
            'Yes',
            'No') AS `On PSAR Report (Y/N)`,
        if((`SR_Plan`.`tuition_confirmed_for_season` = '1'),
            'Yes',
            'No') AS `Tuition Confirmed (Y/N)`,
        if(isnull(`SR_Plan`.`china_contract_template_uploaded_date`),
            'No',
            'Yes') AS `School China Contract Template is Ready (Y/N)`,
        if(isnull(`SR_Plan`.`english_only_contract_template_uploaded_date`),
            'No',
            'Yes') AS `School English-only Contract Template is Ready (Y/N)`,
        if((`SR_Plan`.`re_mat_pak_required` = '1'),
            'Yes',
            'No') AS `Re-MatPak Required (Y/N)`,
        (select 
                `Mat_Pak_School`.`ready_date`
            from
                `Mat_Pak_School`
            where
                ((`Mat_Pak_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
                    and (`Mat_Pak_School`.`season` = `SR_Plan`.`season`)
                    and (`Mat_Pak_School`.`language` = 'Chinese-English')
                    and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'School Mat-Pak'))
            group by `Mat_Pak_School`.`school_mongo_id` , `Mat_Pak_School`.`season` , `Mat_Pak_School`.`mat_pak_combination_mat_pak_type` , `Mat_Pak_School`.`language`) AS `School Mat-Pak Ready Date`,
        (select 
                `Mat_Pak_School`.`ready_date`
            from
                `Mat_Pak_School`
            where
                ((`Mat_Pak_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
                    and (`Mat_Pak_School`.`season` = `SR_Plan`.`season`)
                    and (`Mat_Pak_School`.`language` = 'Chinese-English')
                    and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-School Mat-Pak'))
            group by `Mat_Pak_School`.`school_mongo_id` , `Mat_Pak_School`.`season` , `Mat_Pak_School`.`mat_pak_combination_mat_pak_type` , `Mat_Pak_School`.`language`) AS `School Re-Mat-Pak Ready Date`,
        `SR_Plan`.`createdOn` AS `School Date Opened`,
        `SR_Plan`.`date_closed` AS `School Date Closed`,
        `SR_Plan`.`close_reason` AS `Reason for Closing`,
        `SR_Plan`.`accepted_grades` AS `Grade Accepted`,
        `SR_Plan`.`grade_specific_targets` AS `Grade Specific Target`,
        `SR_Plan`.`admission_notes` AS `Admission Notes`,
        `SR_Plan`.`status` AS `School SR Plan Status`,
        `SR_Plan`.`open_seats` AS `School Open Seats (based on student enrollment status)`,
        (`SR_Plan`.`agreed_target` - count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end))) AS `School Open Seats (based on number of deposit)`,
        `SR_Plan`.`agreed_target` AS `School Agreed Target`,
        `SR_Plan`.`sales_approved_target` AS `Sales Approved School Target`,
        `SR_Plan`.`GP_approved_target` AS `GP Approved School Target`,
        `SR_Plan`.`plan_applicants` AS `Number of Applications`,
        `SR_Plan`.`plan_enrollment` AS `Number of Enrolled Students`,
        `SR_Plan`.`plan_selected` AS `Number of Selected Students`,
        `SR_Plan`.`open_bed` AS `GP Open Beds`,
        `SR_Plan`.`test_requirements` AS `School Test Requirements`,
        `SR_Plan`.`min_toefl_score` AS `Minimum TOEFL Score`,
        `SR_Plan`.`min_toefljr_score` AS `Minimum TOEFL JR Score`,
        `SR_Plan`.`min_itep_score` AS `Minimum ITEP Score`,
        `SR_Plan`.`min_slep_score` AS `Minimum SLEP Score`,
        `SR_Plan`.`new_arrival_date` AS `New Student Arrival Date`,
        `SR_Plan`.`returning_arrival_date` AS `Returning Student Arrival Date`,
        `SR_Plan`.`new_latest_arrvial_date` AS `New Student Latest Arrival Date`,
        `SR_Plan`.`returning_latest_arrvial_date` AS `Returning Student Latest Arrival Date`,
        `SR_Plan`.`first_day_of_class` AS `First Day of Class`,
        `SR_Plan`.`program_end_date` AS `Program End Date`,
        `SR_Plan`.`New_Student_Orientation_Domestic_Date` AS `Domestic Orientation Date`,
        `SR_Plan`.`Host_Orientation_Date` AS `Host Orientation Date`,
        `SR_Plan`.`School_Orientation_Dates` AS `School Orientation Dates`,
        count((case
            when
                (((`Student_To_School`.`application_fee_date` is not null)
                    or (`Student_To_School`.`interview_date` is not null)
                    or (`Student_To_School`.`interview_school_decision_date` is not null)
                    or (`Student_To_School`.`deposit_date` is not null)
                    or (`Student_To_School`.`tuition_date` is not null)
                    or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) AS `Number of Applicant`,
        count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) AS `Number of Deposit`,
        count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (`Student_To_School`.`residential_type` in ('BOARDING_THROUGH_CAMBRIDGE' , 'HOST_FAMILY_THROUGH_CAMBRIDGE',
                    'FAMILY_REFERRAL'))
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) AS `New GP Enrolled Student`,
        count((case
            when
                ((`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL'))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) AS `Number of Offer Letter`,
        count((case
            when
                ((`Student_To_School`.`interview_result` = 'REJECTED')
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) AS `Number of Rejection`,
        (count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) / `SR_Plan`.`agreed_target`) AS `School Target Completion Rate`,
        (count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) / count((case
            when
                (((`Student_To_School`.`application_fee_date` is not null)
                    or (`Student_To_School`.`interview_date` is not null)
                    or (`Student_To_School`.`interview_school_decision_date` is not null)
                    or (`Student_To_School`.`deposit_date` is not null)
                    or (`Student_To_School`.`tuition_date` is not null)
                    or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end))) AS `School Enrollment Rate`,
        if(isnull((count((case
                        when
                            ((`Student_To_School`.`deposit_date` is not null)
                                and (`Student_To_School`.`status` <> 'DELETED')
                                and (isnull(`Student_To_School`.`application_type`)
                                or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                                and ((`SR_Plan`.`season` = '2016__Spring')
                                or (`SR_Plan`.`season` = '2016_Fall')))
                        then
                            1
                        else NULL
                    end)) / count((case
                        when
                            (((`Student_To_School`.`application_fee_date` is not null)
                                or (`Student_To_School`.`interview_date` is not null)
                                or (`Student_To_School`.`interview_school_decision_date` is not null)
                                or (`Student_To_School`.`deposit_date` is not null)
                                or (`Student_To_School`.`tuition_date` is not null)
                                or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                                and (`Student_To_School`.`status` <> 'DELETED')
                                and (isnull(`Student_To_School`.`application_type`)
                                or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                                and ((`SR_Plan`.`season` = '2016__Spring')
                                or (`SR_Plan`.`season` = '2016_Fall')))
                        then
                            1
                        else NULL
                    end)))),
            0.6,
            (count((case
                when
                    ((`Student_To_School`.`deposit_date` is not null)
                        and (`Student_To_School`.`status` <> 'DELETED')
                        and (isnull(`Student_To_School`.`application_type`)
                        or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                        and ((`SR_Plan`.`season` = '2016__Spring')
                        or (`SR_Plan`.`season` = '2016_Fall')))
                then
                    1
                else NULL
            end)) / count((case
                when
                    (((`Student_To_School`.`application_fee_date` is not null)
                        or (`Student_To_School`.`interview_date` is not null)
                        or (`Student_To_School`.`interview_school_decision_date` is not null)
                        or (`Student_To_School`.`deposit_date` is not null)
                        or (`Student_To_School`.`tuition_date` is not null)
                        or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                        and (`Student_To_School`.`status` <> 'DELETED')
                        and (isnull(`Student_To_School`.`application_type`)
                        or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                        and ((`SR_Plan`.`season` = '2016__Spring')
                        or (`SR_Plan`.`season` = '2016_Fall')))
                then
                    1
                else NULL
            end)))) AS `2016 Year School Enrollment Rate`,
        (count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                    and (`SR_Plan`.`season` = '2016__Spring'))
            then
                1
            else NULL
        end)) / count((case
            when
                (((`Student_To_School`.`application_fee_date` is not null)
                    or (`Student_To_School`.`interview_date` is not null)
                    or (`Student_To_School`.`interview_school_decision_date` is not null)
                    or (`Student_To_School`.`deposit_date` is not null)
                    or (`Student_To_School`.`tuition_date` is not null)
                    or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                    and (`SR_Plan`.`season` = '2016__Spring'))
            then
                1
            else NULL
        end))) AS `2016 spring enrollment rate`,
        (count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                    and (`SR_Plan`.`season` = '2016_Fall'))
            then
                1
            else NULL
        end)) / count((case
            when
                (((`Student_To_School`.`application_fee_date` is not null)
                    or (`Student_To_School`.`interview_date` is not null)
                    or (`Student_To_School`.`interview_school_decision_date` is not null)
                    or (`Student_To_School`.`deposit_date` is not null)
                    or (`Student_To_School`.`tuition_date` is not null)
                    or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY')))
                    and (`SR_Plan`.`season` = '2016_Fall'))
            then
                1
            else NULL
        end))) AS `2016 fall enrollment rate`,
        (count((case
            when
                ((`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL'))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) / count((case
            when
                (((`Student_To_School`.`application_fee_date` is not null)
                    or (`Student_To_School`.`interview_date` is not null)
                    or (`Student_To_School`.`interview_school_decision_date` is not null)
                    or (`Student_To_School`.`deposit_date` is not null)
                    or (`Student_To_School`.`tuition_date` is not null)
                    or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end))) AS `School Acceptance Rate`,
        (count((case
            when
                ((`Student_To_School`.`interview_result` = 'REJECTED')
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) / count((case
            when
                (((`Student_To_School`.`application_fee_date` is not null)
                    or (`Student_To_School`.`interview_date` is not null)
                    or (`Student_To_School`.`interview_school_decision_date` is not null)
                    or (`Student_To_School`.`deposit_date` is not null)
                    or (`Student_To_School`.`tuition_date` is not null)
                    or (`Student_To_School`.`ssa_queue_i20_created_on` is not null))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end))) AS `School Rejection Rate`,
        (count((case
            when
                ((`Student_To_School`.`deposit_date` is not null)
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end)) / count((case
            when
                ((`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL'))
                    and (`Student_To_School`.`status` <> 'DELETED')
                    and (isnull(`Student_To_School`.`application_type`)
                    or (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'GPHOMESTAY_ONLY', 'HOMESTAY_ONLY'))))
            then
                1
            else NULL
        end))) AS `School Conversion Rate`,
        if(isnull(`SR_Plan`.`acceptance_enrollment_contract_created_date`),
            'No',
            'Yes') AS `Confirmed Enrollment Contract Uploaded (Y/N)`,
        if(isnull(`SR_Plan`.`re_enrollment_offer_contract_created_date`),
            'No',
            'Yes') AS `Confirmed Re-enrollment Contract Uploaded (Y/N)`,
        `SR_Plan`.`girls_residential_system` AS `Girls Residential Accommodation`,
        `SR_Plan`.`boys_residential_system` AS `Boys Residential Accommodation`,
        `SR_Plan`.`open_for_admission` AS `Confirmed SR Plan (Y/N)`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`,
        if(isnull(`Application_In_Pipeline`.`number_of_application_in_pipeline`),
            0,
            `Application_In_Pipeline`.`number_of_application_in_pipeline`) AS `Number of Applicants in Pipeline`
    from
        (((`SR_Plan`
        left join `Student_To_School` ON (((`SR_Plan`.`school_mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`SR_Plan`.`season` = `Student_To_School`.`season`))))
        left join `School` ON ((`SR_Plan`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Application_In_Pipeline` ON (((`Application_In_Pipeline`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Application_In_Pipeline`.`season` = `SR_Plan`.`season`))))
    where
        ((`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW'))
            and (`SR_Plan`.`school_short_name` not in ('TS' , 'GS', 'TEST SCHOOL')))
    group by `SR_Plan`.`mongo_id`