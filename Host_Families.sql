CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Families` AS
    select 
        `Host`.`mongo_id` AS `Host Family Mongo ID`,
        `Host`.`record_id` AS `Host Family ID`,
        `Host`.`inquiry_id` AS `Host Inquiry ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `Host`.`state` AS `Host Family State`,
        `Host`.`city` AS `Host Family City`,
        `Host`.`address` AS `Host Family Address(Hidden)`,
        if((length(`Host`.`zip`) = 4),
            concat('0', `Host`.`zip`),
            `Host`.`zip`) AS `Host Family Zip Code`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `Host`.`primary_contact_name` AS `Host Family Name`,
        `Host`.`primary_host_age` AS `Primary Host Member Age`,
        `Host`.`primary_host_gender` AS `Primary Host Member Gender`,
        if((`Host`.`primary_host_married` = '1'),
            'Yes',
            'No') AS `Primary Host Member Married (Y/N)`,
        `Host`.`another_member_name` AS `Secondary Host Member Name`,
        `Host`.`another_member_relationship` AS `Secondary Host Member Relationship to Primary`,
        `Host`.`another_member_email` AS `Seondary Host Email(Hidden)`,
        `Host`.`type` AS `Host Type`,
        (select 
                `Host_Season`.`host_type`
            from
                `Host_Season`
            where
                ((`Host`.`mongo_id` = `Host_Season`.`host_mongo_id`)
                    and (`Host_Season`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat(year(curdate()),
                                '/',
                                (right(year(curdate()), 2) + 1))
                    else concat((year(curdate()) - 1),
                            '/',
                            right(year(curdate()), 2))
                end)))
            limit 1) AS `Host Application Type`,
        (select 
                `Host_Season`.`host_type`
            from
                `Host_Season`
            where
                ((`Host`.`mongo_id` = `Host_Season`.`host_mongo_id`)
                    and (`Host_Season`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat(year((curdate() + 1)),
                                '/',
                                (right(year(curdate()), 2) + 2))
                    else concat(year(curdate()),
                            '/',
                            (right(year(curdate()), 2) + 1))
                end)))
            limit 1) AS `Next Year Host Application Type`,
        `Host`.`income` AS `Host Family Combined Income`,
        `Host`.`member_income` AS `Primary Member Income`,
        `Host`.`children_age` AS `Host Family Children age`,
        `Host`.`number_of_children` AS `Number of Children`,
        `Host`.`distance_from_school` AS `Distance from School`,
        `Host`.`distance_to_connected_school` AS `Straight Line Distance from School`,
        `Host`.`how_to_get_to_school` AS `How To Get To School`,
        `Host`.`primary_host_education_degree` AS `Primary Host Member Education Level`,
        `Host`.`primary_host_school_attended` AS `Primary Host Member School Attended`,
        `Host`.`primary_host_industry` AS `Primary Host Member Industry`,
        `Host`.`primary_host_occupation` AS `Primary Host Member Occupation`,
        `Host`.`student_accommodation` AS `Student Accommodation`,
        `Host`.`primary_language` AS `Primary Language`,
        `Host`.`how_long_lived_in_us` AS `How Long Lived in US`,
        `Host`.`primary_email_address` AS `Host Family Email Address(Hidden)`,
        cast(`Host`.`applicant_date_of_birth` as date) AS `Host Family Member Date of Birth`,
        `Host`.`status` AS `Host Family Compliance Status`,
        `Host`.`approved_date` AS `Host Last Approval Date`,
        `Host`.`season_status` AS `Host Family Season Status`,
        `Host`.`nearby_school_short_names` AS `Nearby Schools`,
        `Host`.`current_housing_capacity` AS `Housing Capacity`,
        `Host`.`current_open_beds` AS `Host Current Open Beds`,
        `Host`.`current_gender_comfortable_hosting` AS `Gender Comfortable Hosting`,
        `Host`.`harmony_page_link` AS `Link to Host Page`,
        `Host`.`latitude` AS `Host Family Latitude`,
        `Host`.`longitude` AS `Host Family Longitude`,
        `Host`.`primary_phone` AS `Primary Host Phone Number(Hidden)`,
        `Host`.`another_member_phone` AS `Secondary Host Phone(Hidden)`,
        if((`Host`.`bgc_up_to_date` = '1'),
            'Yes',
            'No') AS `Current Background Check Is Up To Date (Y/N)`,
        `Host`.`most_recent_expiring_bgc_date` AS `Upcoming Background Check`,
        `Host`.`how_hear_of_us` AS `How Did You Hear About GP`,
        `Host`.`how_hear_of_us_detail` AS `How Did You Hear About GP details`,
        count(`Student_To_Host`.`student_mongo_id`) AS `Number of Current Hosting Students`,
        `Host_Season`.`Matched_Students` AS `Matched Student Info`,
        `Host`.`latest_season_status` AS `Latest Host Family Season Status`,
        `Host`.`initial_approval_date` AS `Host Initial Approval Date`,
        concat('https://www.ciiedu.net/ciie.html#/public/showHosQuiz?hostFamilyId=',
                `Host`.`mongo_id`,
                '&schoolYear=2017/18') AS `Host Questionnaire URL`,
        `Host`.`Created_On` AS `Host Initial Application Date`,
        `Host`.`Current_School_Year_Selling_Point` AS `Current School Year Selling Point`,
        `Host`.`Next_School_Year_Selling_Point` AS `Next School Year Selling Point`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        `Host`.`Latest_Season_Status_Changed_From` AS `Latest Season Status Changed From`,
        `Host`.`Latest_Season_Status_Change_Reason` AS `Latest Season Status Change Reason`,
        `Host`.`Latest_Compliance_Status_Changed_From` AS `Latest Compliance Status Changed From`,
        `Host`.`Latest_Compliance_Status_Change_Reason` AS `Latest Compliance Status Change Reason`,
        `Host`.`Latest_Designations` AS `Designations`,
        `Host`.`Next_School_Year_Questionnaire_Link` AS `Questionnaire Link`,
        `Host`.`Set_Password_Link` AS `Set Password Link`,
        `Host`.`Gp_Portal_User_Id` AS `Gp Portal User Id`
    from
        (((`Host`
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))
        left join `Student_To_Host` ON (((`Host`.`mongo_id` = `Student_To_Host`.`host_mongo_id`)
            and (`Student_To_Host`.`status` = 'SELECTED')
            and ((`Student_To_Host`.`season` = (case
            when (month(curdate()) >= 9) then concat(year(curdate()), '_Fall')
            else concat((year(curdate()) - 1), '_Fall')
        end))
            or (`Student_To_Host`.`season` = (case
            when (month(curdate()) >= 9) then 'not exist value'
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                'not exist value'
            else concat(year(curdate()), '__Spring')
        end))))))
        left join `Host_Season` ON (((`Host_Season`.`host_mongo_id` = `Host`.`mongo_id`)
            and ((`Host_Season`.`Matched_Students_With_Previous_Host_And_Selected` like '%2017_Fall%')
            or (`Host_Season`.`Matched_Students_With_Previous_Host_And_Selected` like '%2018__Spring%')))))
    group by `Host`.`mongo_id`
    group by `Host`.`mongo_id`