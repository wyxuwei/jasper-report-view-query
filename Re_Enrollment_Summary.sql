CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Re_Enrollment_Summary` AS
    select distinct
        count(0) AS `Potential Re-Enrollments`,
        count((case `School`.`relationship_status`
            when 'Terminated' then 1
            else NULL
        end)) AS `Students enrolled at schools with the status Terminated`,
        (select 
                count((case `Student_To_School`.`status`
                        when 'WITHDREW' then 1
                        when 'EXPELLED' then 1
                        else NULL
                    end))
            from
                `Student_To_School`
            where
                (`Student_To_School`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat(year(curdate()),
                                '/',
                                (right(year(curdate()), 2) + 1))
                    else concat((year(curdate()) - 1),
                            '/',
                            right(year(curdate()), 2))
                end))) AS `Expelled/Withdraw Students`,
        count((case `Student_To_School`.`interview_result`
            when 'APPROVED' then 1
            when 'CONDITIONAL' then 1
            else NULL
        end)) AS `Offers Uploaded`,
        count((case
            when
                (isnull(`Student_To_School`.`interview_result`)
                    or (`Student_To_School`.`interview_result` in ('REJECTED' , 'NONE', 'NA')))
            then
                1
            else NULL
        end)) AS `Re-Enrollment Not Offered`,
        count((case `Student_To_School`.`interview_result`
            when 'PENDING' then 1
            else NULL
        end)) AS `Pending Re-Enrollment Offers`,
        count((case
            when
                ((`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL'))
                    and (`Student_To_School`.`student_decision` = 'SELECTED'))
            then
                1
            else NULL
        end)) AS `Student Accepted Offer`,
        count((case
            when (`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL')) then 1
            else NULL
        end)) AS `Student Chose Not to Re-Enroll`,
        count((case
            when
                ((`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL'))
                    and ((`Student_To_School`.`student_decision` = 'NONE')
                    or isnull(`Student_To_School`.`student_decision`)))
            then
                1
            else NULL
        end)) AS `Pending Student Decision`,
        count((case
            when
                ((`Student_To_School`.`deposit_amount` is not null)
                    or (`Student_To_School`.`tuition_amount` is not null))
            then
                1
            else NULL
        end)) AS `Deposit Paid`,
        count((case `Student_To_School`.`status`
            when 'ENROLLED' then 1
            else NULL
        end)) AS `Re-Enrolled Students (by Application Status)`,
        count((case `Student_To_School`.`status`
            when 'Abandoned Tuition' then 1
            when 'Abandoned Deposit' then 1
            else NULL
        end)) AS `Abandoned Tuition/Deposit`
    from
        ((`Student`
        left join `Student_To_School` ON (((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)
            and (`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`Student_To_School`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '_Fall')
            else concat(year(curdate()), '_Fall')
        end)))))
        left join `School` ON ((`Student`.`current_enrolled_school_mongo_id` = `School`.`mongo_id`)))
    where
        ((`Student`.`current_status` = 'Enrolled')
            and (`Student`.`current_enrolled_grade` <> 'Grade_12')
            and (`School`.`partnership_type` <> 'Public')
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (not ((`School`.`short_name` like '%Test%'))))