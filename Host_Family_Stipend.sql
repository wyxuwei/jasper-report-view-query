CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Family_Stipend` AS
    select distinct
        `Host`.`mongo_id` AS `Host Family Mongo ID`,
        `Host`.`record_id` AS `Host Family ID`,
        `Host`.`primary_contact_name` AS `Host Family Full Name`,
        `Host`.`state` AS `Host Family State`,
        `Connected_School`.`mongo_id` AS `Host Connected School Mongo ID`,
        `Connected_School`.`region` AS `Host Family Region`,
        `Connected_School`.`id` AS `Host Connected School ID`,
        `Connected_School`.`short_name` AS `Host Connected School Short Name`,
        `Connected_School`.`SSC_first_name` AS `Host Connected School SSC First Name`,
        `Connected_School`.`SSC_last_name` AS `Host Connected School SSC Last Name`,
        concat(`Connected_School`.`SSC_first_name`,
                ' ',
                `Connected_School`.`SSC_last_name`) AS `Host Connected School SSC Full Name`,
        `Connected_School`.`school_manager_TL_first_name` AS `Host Connected School Regional Manager First Name`,
        `Connected_School`.`school_manager_TL_last_name` AS `Host Connected School Regional Manager Last Name`,
        concat(`Connected_School`.`school_manager_TL_first_name`,
                ' ',
                `Connected_School`.`school_manager_TL_last_name`) AS `Host Connected School Regional Manager Full Name`,
        `Host`.`payee_name` AS `Payee Name`,
        if((right(`Student_To_School`.`season`, 4) = 'Fall'),
            round((`Student_To_School`.`tpb_stipend_fee` / 10),
                    2),
            round((`Student_To_School`.`tpb_stipend_fee` / 6),
                    2)) AS `Student Paid Monthly Stipend Amount`,
        `Student_To_School`.`season` AS `Student Recruitment Season`,
        `Student_To_Host`.`school_year` AS `School Year`,
        `Student_To_Host`.`Initial_Stipend_Date` AS `Initial Stipend Date`,
        `Student_To_School`.`tpb_stipend_fee` AS `Student Paid Total Stipend Amount`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`english_name` AS `Student English Name`,
        `School`.`mongo_id` AS `Student School Mongo ID`,
        `School`.`id` AS `Student School ID`,
        `School`.`short_name` AS `Student School Short Name`,
        `School`.`SSC_first_name` AS `Student School SSC First Name`,
        `School`.`SSC_last_name` AS `Student School SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `Student School SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Student School Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Student School Regional Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Student School Regional Manager Full Name`,
        `School`.`HPC_first_name` AS `Student School HPC First Name`,
        `School`.`HPC_last_name` AS `Student School HPC Last Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `Student School HPC Full Name`,
        `School`.`HRC_first_name` AS `Student School HRC First Name`,
        `School`.`HRC_last_name` AS `Student School HRC Last Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `Student School HRC Full Name`,
        `Student_To_School`.`status` AS `Student's Application/Enrollment Status`,
        `Student_To_Host`.`status` AS `Hosting Status`,
        `Student_To_Host`.`start_date` AS `Move In Date`,
        `Student_To_Host`.`end_date` AS `Move Out Date`,
        `Student_To_Host`.`days_in_aug` AS `Days Stay with Host in August`,
        `Student_To_Host`.`days_in_sept` AS `Days Stay with Host in September`,
        `Student_To_Host`.`days_in_oct` AS `Days Stay with Host in October`,
        `Student_To_Host`.`days_in_nov` AS `Days Stay with Host in November`,
        `Student_To_Host`.`days_in_dec` AS `Days Stay with Host in December`,
        `Student_To_Host`.`days_in_jan` AS `Days Stay with Host in January`,
        `Student_To_Host`.`days_in_feb` AS `Days Stay with Host in Febuary`,
        `Student_To_Host`.`days_in_mar` AS `Days Stay with Host in March`,
        `Student_To_Host`.`days_in_apr` AS `Days Stay with Host in April`,
        `Student_To_Host`.`days_in_may` AS `Days Stay with Host in May`,
        `Student_To_Host`.`days_in_jun` AS `Days Stay with Host in June`,
        `Student_To_Host`.`days_in_jul` AS `Days Stay with Host in July`,
        round(`Student_To_Host`.`stipend_in_aug`, 2) AS `Stipend to Pay in August`,
        round(`Student_To_Host`.`stipend_in_sept`, 2) AS `Stipend to Pay in September`,
        round(`Student_To_Host`.`stipend_in_oct`, 2) AS `Stipend to Pay in October`,
        round(`Student_To_Host`.`stipend_in_nov`, 2) AS `Stipend to Pay in November`,
        round(`Student_To_Host`.`stipend_in_dec`, 2) AS `Stipend to Pay in December`,
        round(`Student_To_Host`.`stipend_in_jan`, 2) AS `Stipend to Pay in January`,
        round(`Student_To_Host`.`stipend_in_feb`, 2) AS `Stipend to Pay in Febuary`,
        round(`Student_To_Host`.`stipend_in_mar`, 2) AS `Stipend to Pay in March`,
        round(`Student_To_Host`.`stipend_in_apr`, 2) AS `Stipend to Pay in April`,
        round(`Student_To_Host`.`stipend_in_may`, 2) AS `Stipend to Pay in May`,
        round(`Student_To_Host`.`stipend_in_jun`, 2) AS `Stipend to Pay in June`,
        round(`Student_To_Host`.`stipend_in_jul`, 2) AS `Stipend to Pay in July`
    from
        ((((((`Student_To_Host`
        left join `Student` ON ((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Host` ON ((`Student_To_Host`.`host_mongo_id` = `Host`.`mongo_id`)))
        left join `School` `Connected_School` ON ((`Host`.`connected_school_mongo_id` = `Connected_School`.`mongo_id`)))
        left join `Host_Season` ON (((`Student_To_Host`.`host_mongo_id` = `Host_Season`.`host_mongo_id`)
            and (`Student_To_Host`.`school_year` = `Host_Season`.`school_year`))))
        left join `Student_To_School` ON (((`Student_To_Host`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
            and (`Student_To_Host`.`season` = `Student_To_School`.`season`)
            and (`Student_To_Host`.`student_enrolled_school_mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Student_To_School`.`status` = 'ENROLLED'))))
        left join `School` ON ((`Student_To_Host`.`student_enrolled_school_mongo_id` = `School`.`mongo_id`)))
    where
        ((`Student_To_Host`.`status` in ('SELECTED' , 'PREVIOUS_HOST', 'TEMPORARY'))
            and ((not ((`School`.`short_name` like '%Test%')))
            or isnull(`School`.`mongo_id`))
            and (`Student_To_Host`.`school_year` = '2017/18'))