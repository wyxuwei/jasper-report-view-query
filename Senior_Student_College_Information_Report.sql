CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Senior_Student_College_Information_Report` AS
    select 
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`english_name` AS `Student English Name`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        `Student_To_School`.`season` AS `Season`,
        `Student_To_School`.`school_year` AS `School Year`,
        `Student`.`First_Enrolled_Season` AS `First Enrolled Season`,
        `Student`.`Graduation_Year` AS `Graduation Year`,
        `Student_To_School`.`enrolled_grade` AS `Enrolled Grade`,
        `Student_To_School`.`status` AS `Student Enrollment Status`,
        `Student`.`Composite_Score_SAT` AS `SAT Composite Score`,
        `Student`.`Critical_Reading_Score_SAT` AS `SAT Critical Reading Score_`,
        `Student`.`Math_Score_SAT` AS `SAT Math Score`,
        `Student`.`Writing_Score_SAT` AS `SAT Writing Score`,
        `Student`.`Composite_Score_ACT` AS `ACT Composite Score`,
        `Student`.`English_Score_ACT` AS `ACT English Score`,
        `Student`.`Reading_Score_ACT` AS `ACT Reading Score`,
        `Student`.`Math_Science_Score_ACT` AS `ACT Math Science Score`,
        `Student`.`Score_Of_TOEFL` AS `Score Of TOEFL`,
        `Student`.`Potential_Major` AS `Potential Major`,
        `Student`.`Gpa_Number` AS `Gpa Number`,
        `Student`.`Gpa_Weighted` AS `Gpa Weighted`,
        `Student`.`Other_Notes` AS `Other Notes`,
        `Student`.`App_Infos` AS `App Infos`,
        `Student`.`Sat_Subject_Tests` AS `Sat Subject Tests`,
        `Student`.`Number_Of_Colleges_Applied_To` AS `Number of Schools Applied To`,
        `Student`.`Selected_Colleges` AS `Selected School`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        `Student_To_School`.`residential_type` AS `Student Residential Choice`,
        `Agency`.`english_name` AS `Agency Name in English`,
        `Agency`.`name` AS `Agency Name in Native Language`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`
    from
        (((`Student`
        left join `Student_To_School` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Agency` ON ((`Agency`.`agency_id` = `Student`.`agency_id`)))
    where
        ((`Student_To_School`.`enrolled_grade` = 'Grade_12')
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (not ((`School`.`short_name` like '%Test%'))))
    order by `Student`.`mongo_id`