CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Reason_To_Change_Host_Report` AS
    select distinct
        `Student_To_Host`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        `Change_Host`.`Residential_Choice` AS `Residential Choice`,
        `Host`.`primary_contact_name` AS `Host Family Name`,
        `Host`.`state` AS `Host Location`,
        `Student_To_Host`.`season` AS `Season`,
        `Student_To_Host`.`status` AS `Host Matching Status`,
        `School`.`full_name` AS `Connected School Name`,
        `School`.`short_name` AS `Connected School Short Name`,
        `School`.`region` AS `Connected School Region`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        `Change_Host`.`Host_Change` AS `Type Of Change`,
        `Change_Host`.`Last_Day_Current_Host` AS `Last Day At This Host`,
        (case
            when (`Change_Host`.`Reason_For_Change` = 'STUDENT_HAS_WITHDRAWN') then 'Student has withdrawn'
            when (`Change_Host`.`Reason_For_Change` = 'STUDENT_ASKED_TO_LEAVE_HOST_HOME') then 'Student asked to leave host home'
            else `Change_Host`.`Reason_For_Change`
        end) AS `Reason For Change`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`
    from
        ((((`Change_Host`
        left join `Student_To_Host` ON ((`Change_Host`.`Student_To_Host_Mongo_Id` = `Student_To_Host`.`mongo_id`)))
        left join `Student` ON ((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Host` ON ((`Student_To_Host`.`host_mongo_id` = `Host`.`mongo_id`)))
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))
    where
        (`Student_To_Host`.`status` = 'PREVIOUS_HOST')