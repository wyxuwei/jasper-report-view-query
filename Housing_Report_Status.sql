CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Housing_Report_Status` AS
    select distinct
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`current_enrolled_grade` AS `Student Current Grade`,
        `Student`.`current_status` AS `Enrollment Status`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`state` AS `School State`,
        `School`.`region` AS `School Region`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if(((`School`.`is_GP` = '1')
                and (`Student_To_Host`.`status` = 'SELECTED')),
            'GP Student with Host Family',
            if((`School`.`is_GP` = '0'),
                'CIIE Student',
                'GP Student without Host Family')) AS `Students Residential Type`,
        `Student_To_Host`.`status` AS `Host Family Matching Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student_To_School`.`student_travel_arrival_time` AS `Student Arrival Date`,
        concat('https://www.ciiedu.net/ciie.html#/student/studentDetail/studentReportingCards?studentId=',
                `Student`.`mongo_id`) AS `URL to Student Monthly Housing Report Page`,
        `SR_Plan`.`program_end_date` AS `Program End Date`,
        `HRSP_10`.`adjusted_status` AS `Oct Monthly Report (Previous Year)`,
        `HRSP_10`.`adjusted_submitted_date` AS `Oct Submitted Date (Previous Year)`,
        `HRSP_10`.`flag` AS `Oct Flag (Previous Year)`,
        `HRSP_10`.`type` AS `Oct Type (Previous Year)`,
        `HRSP_11`.`adjusted_status` AS `Nov Monthly Report (Previous Year)`,
        `HRSP_11`.`adjusted_submitted_date` AS `Nov Submitted Date (Previous Year)`,
        `HRSP_11`.`flag` AS `Nov Flag (Previous Year)`,
        `HRSP_11`.`type` AS `Nov Type (Previous Year)`,
        `HRSP_12`.`adjusted_status` AS `Dec Monthly Report (Previous Year)`,
        `HRSP_12`.`adjusted_submitted_date` AS `Dec Submitted Date (Previous Year)`,
        `HRSP_12`.`flag` AS `Dec Flag (Previous Year)`,
        `HRSP_12`.`type` AS `Dec Type (Previous Year)`,
        `HRSP1`.`adjusted_status` AS `Jan Monthly Report (Current Year)`,
        `HRSP1`.`adjusted_submitted_date` AS `Jan Submitted Date (Current Year)`,
        `HRSP1`.`flag` AS `Jan Flag (Current Year)`,
        `HRSP1`.`type` AS `Jan Type (Current Year)`,
        `HRSP2`.`adjusted_status` AS `Feb Monthly Report (Current Year)`,
        `HRSP2`.`adjusted_submitted_date` AS `Feb Submitted Date (Current Year)`,
        `HRSP2`.`flag` AS `Feb Flag (Current Year)`,
        `HRSP2`.`type` AS `Feb Type (Current Year)`,
        `HRSP3`.`adjusted_status` AS `Mar Monthly Report (Current Year)`,
        `HRSP3`.`adjusted_submitted_date` AS `Mar Submitted Date (Current Year)`,
        `HRSP3`.`flag` AS `Mar Flag (Current Year)`,
        `HRSP3`.`type` AS `Mar Type (Current Year)`,
        `HRSP4`.`adjusted_status` AS `Apr Monthly Report (Current Year)`,
        `HRSP4`.`adjusted_submitted_date` AS `Apr Submitted Date (Current Year)`,
        `HRSP4`.`flag` AS `Apr Flag (Current Year)`,
        `HRSP4`.`type` AS `Apr Type (Current Year)`,
        `HRSP5`.`adjusted_status` AS `May Monthly Report (Current Year)`,
        `HRSP5`.`adjusted_submitted_date` AS `May Submitted Date (Current Year)`,
        `HRSP5`.`flag` AS `May Flag (Current Year)`,
        `HRSP5`.`type` AS `May Type (Current Year)`,
        `HRSP6`.`adjusted_status` AS `Jun Monthly Report (Current Year)`,
        `HRSP6`.`adjusted_submitted_date` AS `Jun Submitted Date (Current Year)`,
        `HRSP6`.`flag` AS `Jun Flag (Current Year)`,
        `HRSP6`.`type` AS `Jun Type (Current Year)`,
        `HRSP7`.`adjusted_status` AS `Jul Monthly Report (Current Year)`,
        `HRSP7`.`adjusted_submitted_date` AS `Jul Submitted Date (Current Year)`,
        `HRSP7`.`flag` AS `Jul Flag (Current Year)`,
        `HRSP7`.`type` AS `Jul Type (Current Year)`,
        `HRSP8`.`adjusted_status` AS `Aug Monthly Report (Current Year)`,
        `HRSP8`.`adjusted_submitted_date` AS `Aug Submitted Date (Current Year)`,
        `HRSP8`.`flag` AS `Aug Flag (Current Year)`,
        `HRSP8`.`type` AS `Aug Type (Current Year)`,
        `HRSP9`.`adjusted_status` AS `Sep Monthly Report (Current Year)`,
        `HRSP9`.`adjusted_submitted_date` AS `Sep Submitted Date (Current Year)`,
        `HRSP9`.`flag` AS `Sep Flag (Current Year)`,
        `HRSP9`.`type` AS `Sep Type (Current Year)`,
        `HRSP10`.`adjusted_status` AS `Oct Monthly Report (Current Year)`,
        `HRSP10`.`adjusted_submitted_date` AS `Oct Submitted Date (Current Year)`,
        `HRSP10`.`flag` AS `Oct Flag (Current Year)`,
        `HRSP10`.`type` AS `Oct Type (Current Year)`,
        `HRSP11`.`adjusted_status` AS `Nov Monthly Report (Current Year)`,
        `HRSP11`.`adjusted_submitted_date` AS `Nov Submitted Date (Current Year)`,
        `HRSP11`.`flag` AS `Nov Flag (Current Year)`,
        `HRSP11`.`type` AS `Nov Type (Current Year)`,
        `HRSP12`.`adjusted_status` AS `Dec Monthly Report (Current Year)`,
        `HRSP12`.`adjusted_submitted_date` AS `Dec Submitted Date (Current Year)`,
        `HRSP12`.`flag` AS `Dec Flag (Current Year)`,
        `HRSP12`.`type` AS `Dec Type (Current Year)`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        `Student_To_School`.`season` AS `Recruitment Season`
    from
        (((((((((((((((((((`Student`
        left join `School` ON ((`Student`.`current_enrolled_school_mongo_id` = `School`.`mongo_id`)))
        left join `Student_To_Host` ON (((`Student`.`mongo_id` = `Student_To_Host`.`student_mongo_id`)
            and (`Student_To_Host`.`status` = 'SELECTED'))))
        left join `Student_To_School` ON (((`Student`.`mongo_id` = `Student_To_School`.`student_mongo_id`)
            and (`Student`.`current_status` = 'Enrolled')
            and (`Student`.`current_enrolled_school_mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (`Student_To_School`.`residential_type` not in ('HOUSING_OPT_OUT' , 'HOUSING_OPT_OUT_HOMESTAY', 'HOUSING_OPT_OUT_SCHOOL'))
            and (`Student_To_School`.`school_year` = (case
            when (month(curdate()) >= 9) then concat(year(curdate()), '/', (right(year(curdate()), 2) + 1))
            else concat((year(curdate()) - 1), '/', right(year(curdate()), 2))
        end)))))
        left join `SR_Plan` ON ((`SR_Plan`.`mongo_id` = `Student_To_School`.`srplan_id`)))
        left join `Housing_Report_Status_Prep` `HRSP_10` ON (((`Student`.`mongo_id` = `HRSP_10`.`student_mongo_id`)
            and (`HRSP_10`.`month` = concat((year(now()) - 1), '-10')))))
        left join `Housing_Report_Status_Prep` `HRSP_11` ON (((`Student`.`mongo_id` = `HRSP_11`.`student_mongo_id`)
            and (`HRSP_11`.`month` = concat((year(now()) - 1), '-11')))))
        left join `Housing_Report_Status_Prep` `HRSP_12` ON (((`Student`.`mongo_id` = `HRSP_12`.`student_mongo_id`)
            and (`HRSP_12`.`month` = concat((year(now()) - 1), '-12')))))
        left join `Housing_Report_Status_Prep` `HRSP1` ON (((`Student`.`mongo_id` = `HRSP1`.`student_mongo_id`)
            and (`HRSP1`.`month` = concat(year(now()), '-01')))))
        left join `Housing_Report_Status_Prep` `HRSP2` ON (((`Student`.`mongo_id` = `HRSP2`.`student_mongo_id`)
            and (`HRSP2`.`month` = concat(year(now()), '-02')))))
        left join `Housing_Report_Status_Prep` `HRSP3` ON (((`Student`.`mongo_id` = `HRSP3`.`student_mongo_id`)
            and (`HRSP3`.`month` = concat(year(now()), '-03')))))
        left join `Housing_Report_Status_Prep` `HRSP4` ON (((`Student`.`mongo_id` = `HRSP4`.`student_mongo_id`)
            and (`HRSP4`.`month` = concat(year(now()), '-04')))))
        left join `Housing_Report_Status_Prep` `HRSP5` ON (((`Student`.`mongo_id` = `HRSP5`.`student_mongo_id`)
            and (`HRSP5`.`month` = concat(year(now()), '-05')))))
        left join `Housing_Report_Status_Prep` `HRSP6` ON (((`Student`.`mongo_id` = `HRSP6`.`student_mongo_id`)
            and (`HRSP6`.`month` = concat(year(now()), '-06')))))
        left join `Housing_Report_Status_Prep` `HRSP7` ON (((`Student`.`mongo_id` = `HRSP7`.`student_mongo_id`)
            and (`HRSP7`.`month` = concat(year(now()), '-07')))))
        left join `Housing_Report_Status_Prep` `HRSP8` ON (((`Student`.`mongo_id` = `HRSP8`.`student_mongo_id`)
            and (`HRSP8`.`month` = concat(year(now()), '-08')))))
        left join `Housing_Report_Status_Prep` `HRSP9` ON (((`Student`.`mongo_id` = `HRSP9`.`student_mongo_id`)
            and (`HRSP9`.`month` = concat(year(now()), '-09')))))
        left join `Housing_Report_Status_Prep` `HRSP10` ON (((`Student`.`mongo_id` = `HRSP10`.`student_mongo_id`)
            and (`HRSP10`.`month` = concat(year(now()), '-10')))))
        left join `Housing_Report_Status_Prep` `HRSP11` ON (((`Student`.`mongo_id` = `HRSP11`.`student_mongo_id`)
            and (`HRSP11`.`month` = concat(year(now()), '-11')))))
        left join `Housing_Report_Status_Prep` `HRSP12` ON (((`Student`.`mongo_id` = `HRSP12`.`student_mongo_id`)
            and (`HRSP12`.`month` = concat(year(now()), '-12')))))
    where
        ((`Student`.`current_status` = 'Enrolled')
            and (not ((`School`.`short_name` like '%test%'))))
    order by `Student Mongo ID`