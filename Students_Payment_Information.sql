CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Students_Payment_Information` AS
    select 
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `Payment`.`recipient` AS `Payment Received by`,
        `Payment`.`type` AS `Payment Type`,
        `Payment`.`status` AS `Payment Status`,
        `Payment`.`source` AS `Payment Source`,
        `Payment`.`season` AS `Recruitment Season`,
        cast(`Payment`.`date` as date) AS `Payment Date`,
        `Payment`.`documentName` AS `Payment Document`,
        format(`Payment`.`amount`, 2) AS `Payment Amount`
    from
        ((`Payment`
        left join `Student` ON ((`Payment`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Payment`.`school_mongo_id` = `School`.`mongo_id`)))
    where
        ((not ((`School`.`short_name` like '%Test%')))
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%'))))