CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Inventory_Tracking_Report--Spring Season` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`priority_level` AS `School Priority Level`,
        `School`.`cluster` AS `School Cluster`,
        `School`.`market_ability_x` AS `School Marketability (X)`,
        `School`.`market_ability_y` AS `School Marketability (Y)`,
        `School`.`mailing_address` AS `School Mailing Address`,
        `School`.`region` AS `School Region`,
        `School`.`relationship_status` AS `School Relationship Status`,
        `School`.`partnership_type_china` AS `Partnership Type China`,
        `School`.`partnership_type_global` AS `Partnership Type Global`,
        `School`.`partnership_type_hongkong` AS `Partnership Type HongKong`,
        `School`.`partnership_type_korea` AS `Partnership Type Korea`,
        `School`.`partnership_type_macau` AS `Partnership Type Macau`,
        `School`.`partnership_type_residential` AS `Partnership Type Residential`,
        `School`.`partnership_type_taiwan` AS `Partnership Type Taiwan`,
        `School`.`partnership_type_vietnam` AS `Partnership Type Vietnam`,
        `School`.`partnership_type_original_all` AS `Partnership Type Original All`,
        `School`.`partnership_type_original_china` AS `Partnership Type Original China`,
        `School`.`partnership_type_original_global` AS `Partnership Type Original Global`,
        `School`.`partnership_type_original_hongkong` AS `Partnership Type Original HongKong`,
        `School`.`partnership_type_original_korea` AS `Partnership Type Original Korea`,
        `School`.`partnership_type_original_macau` AS `Partnership Type Original Macau`,
        `School`.`partnership_type_original_residential` AS `Partnership Type Original Residential`,
        `School`.`partnership_type_original_taiwan` AS `Partnership Type Original Taiwan`,
        `School`.`partnership_type_original_vietnam` AS `Partnership Type Original Vietnam`,
        `School`.`relationship_status` AS `School Partnership Status`,
        `School`.`school_status` AS `School Status`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay School (Y/N)`,
        'Spring__2018' AS `Report Season`,
        '2017/18' AS `2017/18`,
        `School`.`HPC_first_name` AS `HCC First Name`,
        `School`.`HPC_last_name` AS `HCC Last Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        `School`.`HRC_first_name` AS `HRC First Name`,
        `School`.`HRC_last_name` AS `HRC Last Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`inquiry_number` AS `Host Inquiries (Include Applications Sent)`,
        `School`.`number_of_hosts_at_background_check` AS `Host Apps - Outstanding BGC`,
        `School`.`number_Of_hosts_at_site_visit` AS `Host Apps - Outstanding SV`,
        `School`.`number_of_hosts_at_contract` AS `Host Apps - Outstanding Contract`,
        `School`.`number_of_hosts_at_reference` AS `Host Apps - Outstanding Reference Checks`,
        `School`.`current_pipeline_host_number` AS `Hosts in Pipeline`,
        `School_Season`.`Matched_Active_Hosts_Number` AS `Matched Active Hosts`,
        `School_Season`.`Unmatched_Active_Hosts_Number` AS `Unmatched/Active Hosts`,
        (`School_Season`.`Matched_Active_Hosts_Number` + `School_Season`.`Unmatched_Active_Hosts_Number`) AS `Total Active Hosts`,
        `School_Season`.`Bed_Capacity_From_Unmatched_Active_Hosts` AS `Unmatched Beds Available`,
        (`School_Season`.`New_Enrolled_Homestay_Female_Student_Number` + `School_Season`.`New_Enrolled_Homestay_Only_Female_Student_Number`) AS `New Homestay Enrolled Students - Female`,
        (`School_Season`.`New_Enrolled_Homestay_Male_Student_Number` + `School_Season`.`New_Enrolled_Homestay_Only_Male_Student_Number`) AS `New Homestay Enrolled Students - Male`,
        (((`School_Season`.`New_Enrolled_Homestay_Female_Student_Number` + `School_Season`.`New_Enrolled_Homestay_Male_Student_Number`) + `School_Season`.`New_Enrolled_Homestay_Only_Female_Student_Number`) + `School_Season`.`New_Enrolled_Homestay_Only_Male_Student_Number`) AS `Total New Homestay Enrolled Students`,
        (`School_Season`.`Enrolled_Homestay_Only_Student_Number_For_School_Year` + `School_Season`.`Enrolled_Homestay_Student_Number_For_School_Year`) AS `Total Homestay Enrolled Student for Current School Year`,
        `SR_Plan`.`agreed_target` AS `Spring Student Recruitment Agreed Target`,
        `SR_Plan`.`Committed_Number_Of_Fillable_Seats` AS `Spring Student Recruitment Committed Target`,
        (`School_Season`.`Number_Of_New_Student_Application` + `School_Season`.`Number_Of_Homestay_Only_Student_Application`) AS `New Student Applicants`,
        `School_Season`.`Number_Of_Offer_Sent_Out_Student_Application` AS `New Student Enrollment Offer Sent Out`,
        `School`.`Host_Recruitment_Threshold` AS `Host Inventory Threshold`,
        `School`.`Host_Inventory_Indicator_Actual_Enrollment2` AS `Host Inventory Indicator - Actual Enrollment`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_Inventory_Indicator_Actual_Enrollment2` >= 1)
                        and (`School`.`Host_Inventory_Indicator_Actual_Enrollment2` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Inventory_Indicator_Actual_Enrollment2` >= 3) then 'Inventory Sufficient'
                when isnull(`School`.`Host_Inventory_Indicator_Actual_Enrollment2`) then 'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_Inventory_Indicator_Actual_Enrollment2` >= 1)
                        and (`School`.`Host_Inventory_Indicator_Actual_Enrollment2` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Inventory_Indicator_Actual_Enrollment2` >= 1.25) then 'Inventory Sufficient'
                when isnull(`School`.`Host_Inventory_Indicator_Actual_Enrollment2`) then 'NA'
                else 'Inventory Warning'
            end)) AS `Host Inventory Indictor-Actual Enrollment (Text)`,
        `School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` AS `Active Host Inventory Indicator(Bed)-Actual Enrollment`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` >= 1)
                        and (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` >= 3) then 'Inventory Sufficient'
                when isnull(`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2`) then 'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` >= 1)
                        and (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2` >= 1.25) then 'Inventory Sufficient'
                when isnull(`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment2`) then 'NA'
                else 'Inventory Warning'
            end)) AS `Active Host Inventory Indicator(Bed)-Actual Enrollment(Text)`,
        `School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` AS `AUR Host Inventory Indicator for Spring Recruitment Season`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` >= 1)
                        and (`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` >= 3) then 'Inventory Sufficient'
                when isnull(`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status`) then 'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` >= 1)
                        and (`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status` >= 1.25) then 'Inventory Sufficient'
                when isnull(`School`.`Host_II_Actual_Enrollment2_Special_Host_Season_Status`) then 'NA'
                else 'Inventory Warning'
            end)) AS `AUR Host Inventory Indicator for Spring Recruitment Season(Text)`,
        `School`.`Current_Under_Review_Connected_Approved_Bgce_Host_Number` AS `Under Review Host`,
        (`School_Season`.`Matched_New_Enrolled_Homestay_Only_Student_Number` + `School_Season`.`Matched_New_Enrolled_Homestay_Student_Number`) AS `Matched Homestay New Enrolled Students`,
        ((`School_Season`.`Matched_New_Enrolled_Homestay_Only_Student_Number` + `School_Season`.`Matched_New_Enrolled_Homestay_Student_Number`) / (((`School_Season`.`New_Enrolled_Homestay_Female_Student_Number` + `School_Season`.`New_Enrolled_Homestay_Male_Student_Number`) + `School_Season`.`New_Enrolled_Homestay_Only_Female_Student_Number`) + `School_Season`.`New_Enrolled_Homestay_Only_Male_Student_Number`)) AS `% Of Matched New Homestay Students By Enrolled New GP Students`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `SR_Plan`.`status` AS `SR_Plan Status`,
        `School_Season`.`Number_Of_Homestay_Only_Student_Application` AS `Number Of Homestay Only Student Application`,
        `School_Season`.`Number_Of_Homestay_Only_Student_Application_With_Deposit` AS `Number Of Homestay Only Student Application With Deposit`,
        `School_Season`.`Number_Of_Potential_Or_Applying_Application` AS `Number Of Potential Or Applying Application`
    from
        ((`School`
        left join `School_Season` ON (((`School`.`mongo_id` = `School_Season`.`school_mongo_id`)
            and (`School_Season`.`season` = (select 
                `Current_Recruitment_Season`.`Current Year Spring Season`
            from
                `Current_Recruitment_Season`)))))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`SR_Plan`.`season` = (select 
                `Current_Recruitment_Season`.`Current Year Spring Season`
            from
                `Current_Recruitment_Season`)))))
    where
        (`School`.`school_status` in ('Active' , 'Terminated_Active'))