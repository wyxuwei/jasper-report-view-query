CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Student_Mat-Pak_Status_Report` AS
    select 
        `Student`.`record_id` AS `Student ID`,
        `Student_To_School`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`english_name` AS `Student English Name`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`Student`.`processing_rep_first_name`,
                ' ',
                `Student`.`processing_rep_last_name`) AS `Processing Rep Full Name`,
        concat(`Student`.`processing_rep_secondary_first_name`,
                ' ',
                `Student`.`processing_rep_secondary_last_name`) AS `Secondary Processing Rep Full Name`,
        `Student_To_School`.`deposit_date` AS `Deposit Date Of Payment`,
        `Student_To_School`.`tuition_date` AS `Tuition Date Of Payment`,
        `SR_Plan`.`new_arrival_date` AS `New Students Arrival Date`,
        `SR_Plan`.`returning_arrival_date` AS `Returning Students Arrival Date`,
        if((`SR_Plan`.`re_mat_pak_required` = '1'),
            'Yes',
            'No') AS `Re-Mat-Pak_Required`,
        `Mat_Pak_Student`.`DocuSign_Document_Envelope_Id` AS `Docusign ID`,
        `Mat_Pak_Student`.`tag` AS `Mat-Pak Type`,
        `Mat_Pak_Student`.`send_date` AS `Student Mat-Pak Send Date`,
        if((`Mat_Pak_Student`.`receive_date` is not null),
            'Yes',
            'No') AS `Student Mat-Pak Received (Y/N)`,
        `Mat_Pak_Student`.`receive_date` AS `Student Mat-Pak Received Date`,
        if((`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'BOARDING_THROUGH_CAMBRIDGE',
                'FAMILY_REFERRAL',
                'HOUSING_OPT_OUT_HOMESTAY')),
            'Yes',
            'No') AS `Cambridge Homestay(Y/N)`,
        if((`Mat_Pak_Student`.`is_completed` = '1'),
            'Yes',
            'No') AS `Student Mat-Pak Completed (Y/N)`
    from
        ((((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `SR_Plan` ON (((`Student_To_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`))))
        left join `Mat_Pak_Student` ON ((`Student_To_School`.`mongo_id` = `Mat_Pak_Student`.`student_to_school_mongo_id`)))
    where
        ((`Student_To_School`.`status` = 'ENROLLED')
            and ((`Student_To_School`.`application_type` <> 'RE_ENROLLING')
            or (`SR_Plan`.`re_mat_pak_required` <> '0')))