CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `I-20_Summary` AS
    select 
        count((case
            when (`Student_To_School`.`deposit_amount` is not null) then 1
            when (`Student_To_School`.`tuition_amount` is not null) then 1
            else NULL
        end)) AS `Total Number of Students Paid Deposit`,
        count((case `Student_To_School`.`status`
            when 'ABANDONED DEPOSIT' then 1
            when 'ABANDONED TUITION' then 1
            else NULL
        end)) AS `Total Number of Students Abandon Deposit`,
        count((case
            when
                (isnull(`Student_To_School`.`i20_sent_date`)
                    and (`Student_To_School`.`status` = 'ENROLLED'))
            then
                1
            else NULL
        end)) AS `Total Number of Students Waiting I-20`,
        count((case
            when
                (isnull(`Student_To_School`.`i20_sent_date`)
                    and (`Student_To_School`.`status` = 'ENROLLED')
                    and (`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
                    'SCHOOL_PLACEMENT_NEW',
                    'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER')))
            then
                1
            else NULL
        end)) AS `Total Number of First Time Applicants Waiting I-20`,
        count((case
            when
                (isnull(`Student_To_School`.`i20_sent_date`)
                    and (`Student_To_School`.`status` = 'ENROLLED')
                    and (`Student_To_School`.`application_type` = 'IN_NETWORK_TRANSFER'))
            then
                1
            else NULL
        end)) AS `Total Number of Transfer Students Waiting I-20`,
        count((case
            when
                (isnull(`Student_To_School`.`i20_sent_date`)
                    and (`Student_To_School`.`status` = 'ENROLLED')
                    and (`School`.`is_affiliated_with_diocese` = '1'))
            then
                1
            else NULL
        end)) AS `Total Number of Diocesan School Students Waiting I-20`,
        count((case
            when (`Student_To_School`.`i20_sent_date` is not null) then 1
            else NULL
        end)) AS `Total Number of Sent Out I-20`,
        count(if(isnull(`Student_To_School`.`i20_sent_date`),
            if(((to_days(curdate()) - to_days(if(isnull(`Student_To_School`.`deposit_date`),
                            `Student_To_School`.`tuition_date`,
                            `Student_To_School`.`deposit_date`))) > 10),
                1,
                NULL),
            NULL)) AS `Deposit Paid Over 10 Days`
    from
        ((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
    where
        ((`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            and (`Student_To_School`.`status` <> 'DELETED')
            and (`Student_To_School`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end))
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (not ((`School`.`short_name` like '%Test%'))))