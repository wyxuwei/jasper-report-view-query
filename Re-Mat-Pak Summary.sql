CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Re-Mat-Pak Summary` AS
    select 
        `Student_To_School`.`season` AS `Recruitment Season`,
        count((select 
                `Mat_Pak_School`.`mat_pak_combination_mat_pak_type`
            from
                `Mat_Pak_School`
            where
                ((`Mat_Pak_School`.`SRPlan_id` = `SR_Plan`.`mongo_id`)
                    and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-School Mat-Pak'))
            limit 1)) AS `Number of Re-School Mat-Pak needed`,
        count(if((`Re_School_Mat_Pak`.`is_completed` = '1'),
            '1',
            NULL)) AS `Number of Re-School Mat-Pak Completed`,
        count((select 
                `Mat_Pak_School`.`mat_pak_combination_mat_pak_type`
            from
                `Mat_Pak_School`
            where
                ((`Mat_Pak_School`.`SRPlan_id` = `SR_Plan`.`mongo_id`)
                    and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-GP Mat-Pak'))
            limit 1)) AS `Number of Re-GP Mat-Pak needed`,
        count(if((`Re_GP_Mat_Pak`.`is_completed` = '1'),
            '1',
            NULL)) AS `NUmber of Re-GP Mat-Pak Completed`
    from
        (((`Student_To_School`
        left join `SR_Plan` ON (((`Student_To_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`))))
        left join `Mat_Pak_Student` `Re_School_Mat_Pak` ON (((`Student_To_School`.`mongo_id` = `Re_School_Mat_Pak`.`student_to_school_mongo_id`)
            and (`Re_School_Mat_Pak`.`tag` = 'Re-School Mat-Pak'))))
        left join `Mat_Pak_Student` `Re_GP_Mat_Pak` ON (((`Student_To_School`.`mongo_id` = `Re_GP_Mat_Pak`.`student_to_school_mongo_id`)
            and (`Re_GP_Mat_Pak`.`tag` = 'Re-GP Mat-Pak'))))
    where
        ((`SR_Plan`.`re_mat_pak_required` = '1')
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))
    group by `Student_To_School`.`season`