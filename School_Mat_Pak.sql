CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `School_Mat_Pak` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`region` AS `School Region`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        `SR_Plan`.`season` AS `Recruitment Season`,
        `SR_Plan`.`status` AS `School SR Plan status`,
        if((`SR_Plan`.`tuition_confirmed_for_season` = '1'),
            'Yes',
            'No') AS `Tuition Confirmed (Y/N)`,
        if(isnull(`SR_Plan`.`acceptance_enrollment_contract_created_date`),
            'No',
            'Yes') AS `Confirmed Enrollment Contract Uploaded (Y/N)`,
        if(isnull(`SR_Plan`.`re_enrollment_offer_contract_created_date`),
            'No',
            'Yes') AS `Confirmed Re-enrollment Contract Uploaded (Y/N)`,
        if((`SR_Plan`.`re_mat_pak_required` = '1'),
            'Yes',
            'No') AS `Re-MatPak Required (Y/N)`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `Mat_Pak_School`.`group_name` AS `group_name`,
        `Mat_Pak_School`.`template_name` AS `template_name`,
        `Mat_Pak_School`.`docuSign_id` AS `DocuSign ID`,
        `Mat_Pak_School`.`type` AS `type`,
        `Mat_Pak_School`.`mat_pak_combination_mat_pak_type` AS `Mat-Pak Type`,
        `Mat_Pak_School`.`role_student` AS `role_student`,
        `Mat_Pak_School`.`role_parent` AS `role_parent`,
        `Mat_Pak_School`.`role_witness1` AS `role_witness1`,
        `Mat_Pak_School`.`role_witness2` AS `role_witness2`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`
    from
        ((`SR_Plan`
        left join `School` ON ((`SR_Plan`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Mat_Pak_School` ON (((`SR_Plan`.`school_mongo_id` = `Mat_Pak_School`.`school_mongo_id`)
            and (`SR_Plan`.`season` = `Mat_Pak_School`.`season`))))
    where
        ((`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW'))
            and (`School`.`short_name` not in ('TS' , 'GS', 'TEST SCHOOL'))
            and (not ((`School`.`short_name` like '%Test%'))))