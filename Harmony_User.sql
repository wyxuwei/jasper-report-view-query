CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Harmony_User` AS
    select 
        `User_Profile`.`mongo_id` AS `User Mongo ID`,
        `User_Profile`.`first_name` AS `User First Name`,
        `User_Profile`.`last_name` AS `User Last Name`,
        concat(`User_Profile`.`first_name`,
                ' ',
                `User_Profile`.`last_name`) AS `User Full Name`,
        `User_Profile`.`email` AS `User Email`,
        `User_Profile`.`phone` AS `User Phone Number`,
        `User_Profile`.`wechat` AS `User WeChat`,
        `User_Profile`.`user_type` AS `User Type`,
        `User_Profile`.`title` AS `User Title`,
        `User_Profile`.`department` AS `User Department`,
        `User_Profile`.`location` AS `User Location`,
        `User_Profile`.`office` AS `User Office`,
        `User_Profile`.`username` AS `User Name`,
        `User_Profile`.`team_lead` AS `Team Lead`,
        `User_Profile`.`created_on` AS `User Profile Created Date`,
        `User_Profile`.`regist_status` AS `Regist Status`,
        `User_Profile`.`last_login_date` AS `Last login Date`,
        `User_Profile`.`login_times` AS `Login Times`,
        if(((locate('AS_Coordinator,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('AS_Coordinator')) = 'AS_Coordinator')),
            'Yes',
            NULL) AS `AS_Coordinator (Y/N)`,
        if(((locate('BI_FT,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('BI_FT')) = 'BI_FT')),
            'Yes',
            NULL) AS `BI_FT (Y/N)`,
        if(((locate('CT_Customer_Service_Rep,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('CT_Customer_Service_Rep')) = 'CT_Customer_Service_Rep')),
            'Yes',
            NULL) AS `CT_Customer_Service_Rep (Y/N)`,
        if(((locate('CT_Customer_Service_Rep_TL,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('CT_Customer_Service_Rep_TL')) = 'CT_Customer_Service_Rep_TL')),
            'Yes',
            NULL) AS `CT_Customer_Service_Rep_TL (Y/N)`,
        if(((locate('CT_Processing_Rep,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('CT_Processing_Rep')) = 'CT_Processing_Rep')),
            'Yes',
            NULL) AS `CT_Processing_Rep (Y/N)`,
        if(((locate('CT_Processing_Rep_TL,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('CT_Processing_Rep_TL')) = 'CT_Processing_Rep_TL')),
            'Yes',
            NULL) AS `CT_Processing_Rep_TL (Y/N)`,
        if(((locate('CT_Program_Rep,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('CT_Program_Rep')) = 'CT_Program_Rep')),
            'Yes',
            NULL) AS `CT_Program_Rep (Y/N)`,
        if(((locate('CT_Program_Rep_TL,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('CT_Program_Rep_TL')) = 'CT_Program_Rep_TL')),
            'Yes',
            NULL) AS `CT_Program_Rep_TL (Y/N)`,
        if(((locate('GP_APA,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('GP_APA')) = 'GP_APA')),
            'Yes',
            NULL) AS `GP_APA (Y/N)`,
        if(((locate('GP_HPC,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('GP_HPC')) = 'GP_HPC')),
            'Yes',
            NULL) AS `GP_HPC (Y/N)`,
        if(((locate('GP_Manager,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('GP_Manager')) = 'GP_Manager')),
            'Yes',
            NULL) AS `GP_Manager (Y/N)`,
        if(((locate('GP_Marketing,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('GP_Marketing')) = 'GP_Marketing')),
            'Yes',
            NULL) AS `GP_Marketing (Y/N)`,
        if(((locate('GP_RC,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('GP_RC')) = 'GP_RC')),
            'Yes',
            NULL) AS `GP_RC (Y/N)`,
        if(((locate('GP_RM,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('GP_RM')) = 'GP_RM')),
            'Yes',
            NULL) AS `GP_RM (Y/N)`,
        if(((locate('IT_Admin,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('IT_Admin')) = 'IT_Admin')),
            'Yes',
            NULL) AS `IT_Admin (Y/N)`,
        if(((locate('managers,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('managers')) = 'managers')),
            'Yes',
            NULL) AS `managers (Y/N)`,
        if(((locate('PD_Associate,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PD_Associate')) = 'PD_Associate')),
            'Yes',
            NULL) AS `PD_Associate (Y/N)`,
        if(((locate('PD_Director,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PD_Director')) = 'PD_Director')),
            'Yes',
            NULL) AS `PD_Director (Y/N)`,
        if(((locate('PD_TL,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PD_TL')) = 'PD_TL')),
            'Yes',
            NULL) AS `PD_TL (Y/N)`,
        if(((locate('PM_Director,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_Director')) = 'PM_Director')),
            'Yes',
            NULL) AS `PM_Director (Y/N)`,
        if(((locate('PM_IPC,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_IPC')) = 'PM_IPC')),
            'Yes',
            NULL) AS `PM_IPC (Y/N)`,
        if(((locate('PM_SSA,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_SSA')) = 'PM_SSA')),
            'Yes',
            NULL) AS `PM_SSA (Y/N)`,
        if(((locate('PM_SSA_TL_And_Manager,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_SSA_TL_And_Manager')) = 'PM_SSA_TL_And_Manager')),
            'Yes',
            NULL) AS `PM_SSA_TL_And_Manager (Y/N)`,
        if(((locate('PM_SSC,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_SSC')) = 'PM_SSC')),
            'Yes',
            NULL) AS `PM_SSC (Y/N)`,
        if(((locate('PM_SSM,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_SSM')) = 'PM_SSM')),
            'Yes',
            NULL) AS `PM_SSM (Y/N)`,
        if(((locate('PM_TL,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PM_TL')) = 'PM_TL')),
            'Yes',
            NULL) AS `PM_TL (Y/N)`,
        if(((locate('PSAR_Downloaders,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PSAR_Downloaders')) = 'PSAR_Downloaders')),
            'Yes',
            NULL) AS `PSAR_Downloaders (Y/N)`,
        if(((locate('PST_Director,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PST_Director')) = 'PST_Director')),
            'Yes',
            NULL) AS `PST_Director (Y/N)`,
        if(((locate('PST_PSA,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PST_PSA')) = 'PST_PSA')),
            'Yes',
            NULL) AS `PST_PSA (Y/N)`,
        if(((locate('PST_TL,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('PST_TL')) = 'PST_TL')),
            'Yes',
            NULL) AS `PST_TL (Y/N)`,
        if(((locate('SRPlanReviewer,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('SRPlanReviewer')) = 'SRPlanReviewer')),
            'Yes',
            NULL) AS `SRPlanReviewer (Y/N)`,
        if(((locate('Translator_And_TL,',
                    `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('Translator_And_TL')) = 'Translator_And_TL')),
            'Yes',
            NULL) AS `Translator_And_TL (Y/N)`,
        if(((locate('UPPER_MANAGER,', `User_Profile`.`roles`) <> 0)
                or (right(`User_Profile`.`roles`,
                length('UPPER_MANAGER')) = 'UPPER_MANAGER')),
            'Yes',
            NULL) AS `UPPER_MANAGER (Y/N)`,
        `User_Profile`.`roles` AS `Access Level`
    from
        `User_Profile`
    where
        (`User_Profile`.`regist_status` = 'ACTIVE')