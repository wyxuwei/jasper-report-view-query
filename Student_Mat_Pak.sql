CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Student_Mat_Pak` AS
    select 
        `Student_To_School`.`mongo_id` AS `Application Mongo ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`english_name` AS `Student English Name`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        `Student_To_School`.`season` AS `Recruiment Season`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`region` AS `School Region`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`SR_Plan`.`re_mat_pak_required` = '1'),
            'Yes',
            'No') AS `Re-MatPak Required (Y/N)`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Regional Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        `Student`.`processing_rep_first_name` AS `Processing Rep First Name`,
        `Student`.`processing_rep_last_name` AS `Processing Rep Last Name`,
        concat(`Student`.`processing_rep_first_name`,
                ' ',
                `Student`.`processing_rep_last_name`) AS `Processing Rep Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `Student_To_School`.`deposit_date` AS `Deposit Date of Payment`,
        `Student_To_School`.`tuition_date` AS `Tuition Date of Payment`,
        `SR_Plan`.`new_arrival_date` AS `New Students Arrival Date`,
        `SR_Plan`.`new_latest_arrvial_date` AS `New Students Latest Arrvial Date`,
        `SR_Plan`.`returning_arrival_date` AS `Returning Students Arrival Date`,
        `SR_Plan`.`returning_latest_arrvial_date` AS `Returning Students Latest Arrvial Date`,
        `Mat_Pak_School`.`docuSign_id` AS `DocuSign ID`,
        `Mat_Pak_School`.`mat_pak_combination_mat_pak_type` AS `Mat-Pak Type`,
        `Mat_Pak_Student`.`send_date` AS `Student Mat-Pak Send Date`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`Student`.`processing_rep_secondary_first_name`,
                ' ',
                `Student`.`processing_rep_secondary_last_name`) AS `Secondary Processing Rep Full Name`,
        if(isnull(`Mat_Pak_Student`.`receive_date`),
            'No',
            'Yes') AS `Student Mat-Pak Received (Y/N)`,
        `Mat_Pak_Student`.`receive_date` AS `Student Mat-Pak Received Date`,
        if((`Mat_Pak_Student`.`is_completed` = '1'),
            'Yes',
            'No') AS `Student Mat-Pak Completed (Y/N)`,
        if((`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'BOARDING_THROUGH_CAMBRIDGE',
                'FAMILY_REFERRAL',
                'HOUSING_OPT_OUT_HOMESTAY')),
            'Yes',
            'No') AS `Cambridge Homestay(Y/N)`,
        if((`SR_Plan`.`re_mat_pak_required` = '1'),
            'Yes',
            'No') AS `Re-Mat-Pak_Required`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`
    from
        (((((`Student_To_School`
        left join `Mat_Pak_School` ON (((`Student_To_School`.`school_mongo_id` = `Mat_Pak_School`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `Mat_Pak_School`.`season`))))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `SR_Plan` ON (((`Student_To_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `Mat_Pak_Student` ON (((`Student_To_School`.`mongo_id` = `Mat_Pak_Student`.`student_to_school_mongo_id`)
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = `Mat_Pak_Student`.`tag`))))
    where
        ((`Student_To_School`.`status` in ('ENROLLED' , 'EXPELLED', 'WITHDREW'))
            and (((`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            and (`School`.`is_GP` = 0)
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'School Mat-Pak'))
            or ((`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            and (`School`.`is_GP` = 1)
            and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` in ('School Mat-Pak' , 'GP Mat-Pak', 'Host Mat-Pak')))
            or ((`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            and (`School`.`is_GP` = 1)
            and (`Student_To_School`.`residential_type` = 'BOARDING_THROUGH_CAMBRIDGE')
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` in ('School Mat-Pak' , 'GP Boarding Mat-Pak')))
            or ((`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            and (`School`.`is_GP` = 1)
            and (`Student_To_School`.`residential_type` = 'HOUSING_OPT_OUT')
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'School Mat-Pak'))
            or ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`School`.`is_GP` = 0)
            and (`SR_Plan`.`re_mat_pak_required` = 1)
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-School Mat-Pak'))
            or ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`School`.`is_GP` = 1)
            and (`Student_To_School`.`residential_type` = 'HOST_FAMILY_THROUGH_CAMBRIDGE')
            and (`SR_Plan`.`re_mat_pak_required` = 1)
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` in ('Re-School Mat-Pak' , 'Re-GP Mat-Pak', 'Re-GP-CA Mat-Pak')))
            or ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`School`.`is_GP` = 1)
            and (`Student_To_School`.`residential_type` = 'BOARDING_THROUGH_CAMBRIDGE')
            and (`SR_Plan`.`re_mat_pak_required` = 1)
            and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` in ('Re-School Mat-Pak' , 'Re-GP Boarding-Mat-Pak'))))
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%'))))
    group by `Student`.`mongo_id` , `Mat_Pak_School`.`mat_pak_combination_mat_pak_type`
    order by `Student_To_School`.`season` , `School`.`short_name`