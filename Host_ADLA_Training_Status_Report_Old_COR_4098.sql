CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_ADLA_Training_Status_Report_Test` AS
    select distinct
        `Host`.`mongo_id` AS `Host Mongo ID`,
        concat(`Host_Member`.`First_Name`,
                ' ',
                `Host_Member`.`Last_Name`) AS `Host Member Full Name`,
        `Host`.`primary_contact_name` AS `Primary Host Family Name`,
        `Host_Season`.`status` AS `Host Family Season Status`,
        `Student_To_Host`.`status` AS `Matched Host Status`,
        if((`Host`.`Virtus_Training_Completed` = '1'),
            'Yes',
            'No') AS `Virtus Training Complete (Y/N)`,
        `Host_Member`.`Virtus_Training_Completion_Date` AS `Virtus Training Completion Date`,
        `Host_Member`.`Virtus_Training_Expiration_Date` AS `Virtus Training Expiration Date`,
        `School`.`short_name` AS `Connected school Short Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HPC Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `School`.`region` AS `School Region`,
        `Host_Season`.`school_year` AS `Current School Year`,
        (`Host`.`current_housing_capacity` - `Host`.`current_open_beds`) AS `Number of Current Hosting Student`
    from
        ((((`Host_Member`
        left join `Host` ON ((`Host_Member`.`Host_Mongo_Id` = `Host`.`mongo_id`)))
        left join `Host_Season` ON ((`Host_Member`.`Host_Mongo_Id` = `Host_Season`.`host_mongo_id`)))
        left join `School` ON ((`Host_Member`.`Connected_School_Mongo_Id` = `School`.`mongo_id`)))
        left join `Student_To_Host` ON (((`Student_To_Host`.`host_mongo_id` = `Host_Member`.`Host_Mongo_Id`)
            and (`Host_Season`.`school_year` = `Student_To_Host`.`school_year`))))
    where
        ((`Host_Member`.`Age` > 18)
            and (`Host_Member`.`Currently_Hosting` = '1')
            and (`Student_To_Host`.`status` in ('SELECTED' , 'PREVIOUS_HOST', 'TEMPORARY'))
            and (`Host_Season`.`school_year` = (select 
                `Current_Recruitment_Season`.`Current School Year`
            from
                `Current_Recruitment_Season`)))
    order by `Host`.`mongo_id`