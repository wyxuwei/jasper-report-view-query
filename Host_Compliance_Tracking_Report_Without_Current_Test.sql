CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Compliance_Tracking_Report_Without_Current_Test` AS
    select 
        `Host`.`mongo_id` AS `Host Mongo ID`,
        `Host`.`record_id` AS `Host ID`,
        `Host`.`primary_contact_name` AS `Primary Host Family Name`,
        `Host_Season`.`school_year` AS `School Year`,
        `Host_Season`.`status` AS `Host Family Season Status`,
        `Host`.`status` AS `Host Family Compliance Status`,
        `School`.`short_name` AS `Connected school Short Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HPC Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `School`.`region` AS `School Region`,
        `Host`.`Latest_Background_Check_Completion_Date` AS `Background Check Completion Date(Latest)`,
        `Host`.`most_recent_expiring_bgc_date` AS `Background Check Expiration Date (Earliest)`,
        `Host`.`host_training_completed` AS `Host Family Training Completed`,
        `Host`.`date_training_completed` AS `Training Completed Date`,
        `Host_Season`.`Alternative_Method_For_Contract` AS `Contract Sent Via`,
        `Host_Season`.`DocuSign_Contract_Status` AS `DocuSign Contract Status`,
        `Host_Season`.`Contract_Url` AS `Contract Uploaded(URL)`,
        if((`Host_Season`.`Bank_Information_Needs_Update` = '1'),
            'Yes',
            'No') AS `Bank Information Needs Update(Y/N)`,
        `Host_Season`.`Voided_Check_Received` AS `Voided Check On File(Y/N)`,
        `Host_Season`.`Direct_Deposit_Form_Received` AS `Direct Deposit Form On File(Y/N)`,
        `Host_Season`.`Date_W9_Received` AS `Up-to-date W9 On File`,
        `Host_Season`.`Matched_Students` AS `Matched Student Info`,
        `Host`.`payee_name` AS `Payee Name`,
        `Host`.`vendor_id` AS `Vendor ID`,
        concat('https://www.ciiedu.net/ciie.html#/hostfamily/hostfamilyDetail/compliance?hostfamilyId=',
                `Host`.`mongo_id`) AS `Host Family Compliance Page URL`
    from
        ((`Host`
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))
        left join `Host_Season` ON ((`Host_Season`.`host_mongo_id` = `Host`.`mongo_id`)))
    where
        (`Host_Season`.`Matched` = '1')
    order by `Host`.`mongo_id`