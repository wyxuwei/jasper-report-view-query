CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `School_Program_Size_By_Season_Matched` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`record_id` AS `School ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`school_status` AS `School Status`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        `School`.`current_open_bed_number` AS `Open Beds`,
        `School`.`current_bed_capacity` AS `GP School Housing Capacity`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_Host`.`status` = 'SELECTED')
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Fall Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            when isnull(`Student_To_School`.`application_type`) then 1
            else NULL
        end)) AS `Matched GP Re-enrolled Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` not in ('GPHOMESTAY_ONLY' , 'RE_ENROLLING'))
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_Host`.`status` = 'SELECTED')
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            when isnull(`Student_To_School`.`application_type`) then 1
            else NULL
        end)) AS `Matched GP New Enrolled Students`
    from
        (((`School`
        left join `Student_To_School` ON ((`School`.`mongo_id` = `Student_To_School`.`school_mongo_id`)))
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Student_To_Host` ON ((`Student`.`mongo_id` = `Student_To_Host`.`student_mongo_id`)))
    where
        ((`Student_To_School`.`status` = 'ENROLLED')
            and (`School`.`short_name` not in ('MockInt' , 'DirectApply',
            'Prescreen',
            'TEST SCHOOL',
            'GPAMERICA',
            'MPP'))
            and (`Student_To_School`.`status` <> 'DELETED'))
    group by `School`.`mongo_id` , `Student_To_School`.`season`