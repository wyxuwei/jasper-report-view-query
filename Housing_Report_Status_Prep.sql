CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Housing_Report_Status_Prep` AS
    select 
        `Monthly_Housing_Report`.`id` AS `id`,
        `Monthly_Housing_Report`.`student_mongo_id` AS `student_mongo_id`,
        `Monthly_Housing_Report`.`school_mongo_id` AS `school_mongo_id`,
        `Monthly_Housing_Report`.`host_name` AS `host_name`,
        `Monthly_Housing_Report`.`host_email` AS `host_email`,
        `Monthly_Housing_Report`.`resco` AS `resco`,
        `Monthly_Housing_Report`.`resco_email` AS `resco_email`,
        `Monthly_Housing_Report`.`status` AS `status`,
        `Monthly_Housing_Report`.`decision_time` AS `decision_time`,
        `Monthly_Housing_Report`.`month` AS `month`,
        `Monthly_Housing_Report`.`created_on` AS `created_on`,
        `Monthly_Housing_Report`.`submitted_date` AS `submitted_date`,
        `Monthly_Housing_Report`.`last_modified_on` AS `last_modified_on`,
        `Monthly_Housing_Report`.`flag` AS `flag`,
        `Monthly_Housing_Report`.`type` AS `type`,
        (case
            when sum((`Monthly_Housing_Report`.`status` = 'TRANSLATED')) then 'TRANSLATED'
            when sum((`Monthly_Housing_Report`.`status` = 'SUBMITTED')) then 'SUBMITTED'
            when sum((`Monthly_Housing_Report`.`status` = 'DRAFTED')) then 'DRAFTED'
            else NULL
        end) AS `adjusted_status`,
        max(`Monthly_Housing_Report`.`submitted_date`) AS `adjusted_submitted_date`
    from
        (`Monthly_Housing_Report`
        left join `Student_To_School` ON ((`Student_To_School`.`mongo_id` = `Monthly_Housing_Report`.`student_to_school_mongo_id`)))
    group by `Monthly_Housing_Report`.`student_mongo_id` , `Monthly_Housing_Report`.`month`