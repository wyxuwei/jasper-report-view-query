CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Family_BGC_Expiration` AS
    select 
        `Host_Member`.`Host_Mongo_Id` AS `Host Mongo Id`,
        `Host_Member`.`Connected_School_Mongo_Id` AS `Connected School Mongo Id`,
        `School`.`short_name` AS `Host Connected School Short Name`,
        `School`.`full_name` AS `Host Connected School Full Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HPC Full Name`,
        `Host_Member`.`First_Name` AS `Host Member First Name`,
        `Host_Member`.`Last_Name` AS `Host Member Last Name`,
        concat(`Host_Member`.`First_Name`,
                ' ',
                `Host_Member`.`Last_Name`) AS `Host Member Full Name`,
        if((`Host_Member`.`Is_Primary_Member` = '1'),
            'Yes',
            'No') AS `Primary Host (Y/N)`,
        `Host_Member`.`Age` AS `Host Member Age`,
        `Host_Member`.`Email` AS `Host Member Email`,
        `Host_Member`.`Cell_Phone` AS `Host Member Phone`,
        `Host_Member`.`Bgc_Exp_Date` AS `Host Member BGC Exp Date`,
        if((`Host_Member`.`Active` = '1'),
            'Yes',
            'No') AS `Host Member is Active (Y/N)`,
        `Host_Member`.`Annual_Background_Check_Status` AS `BGC Status`,
        if((`Host_Member`.`Turn_18_In_One_Month` = '1'),
            'Yes',
            'No') AS `Host Member Turns To 18 In 30 Days(Y/N)`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`
    from
        (`Host_Member`
        left join `School` ON ((`Host_Member`.`Connected_School_Mongo_Id` = `School`.`mongo_id`)))
    where
        ((`Host_Member`.`Compliance_Status` in ('APPROVED' , 'BACKGROUND_CHECK_EXPIRED'))
            and (`Host_Member`.`Currently_Hosting` > 0)
            and (`Host_Member`.`Age` >= 18)
            and ((`Host_Member`.`Bgc_Exp_In_One_Month` = '1')
            or (`Host_Member`.`Turn_18_In_One_Month` = '1')))