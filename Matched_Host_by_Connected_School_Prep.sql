CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Matched_Host_by_Connected_School_Prep` AS
    select 
        `Host`.`connected_school_mongo_id` AS `Connected School Mongo ID`,
        `Student_To_Host`.`school_year` AS `School Year`,
        count(if((`Student_To_Host`.`status` = 'SELECTED'),
            1,
            NULL)) AS `Number of Matched Beds`,
        count(distinct if((`Student_To_Host`.`status` = 'SELECTED'),
                `Host`.`mongo_id`,
                NULL)) AS `Number of Matched Hosts`
    from
        (`Student_To_Host`
        left join `Host` ON ((`Student_To_Host`.`host_mongo_id` = `Host`.`mongo_id`)))
    group by `Host`.`connected_school_mongo_id` , `Student_To_Host`.`school_year`