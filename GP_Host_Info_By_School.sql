CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `GP_Host_Info_By_School` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`region` AS `School Region`,
        `School`.`current_open_bed_number_for_male` AS `Open Beds Only for Male`,
        `School`.`current_open_bed_number_for_female` AS `Open Beds Only for Female`,
        (`SR_Plan`.`agreed_target` + count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student_To_School`.`interview_result` in ('APPROVED' , 'CONDITIONAL')))
            then
                1
            else NULL
        end))) AS `Max Number of Students`,
        `School`.`current_new_host_number` AS `Approved New Hosts`,
        `School`.`current_new_host_capacity` AS `Approved New Beds`,
        (`School`.`current_approved_nearby_host_number` - `School`.`current_new_host_number`) AS `Approved Re-enrolled Hosts`,
        (`School`.`current_bed_capacity` - `School`.`current_new_host_capacity`) AS `Approved Re-enrolled Beds`,
        `School`.`current_capacity_incremental_of_existing_host` AS `Increased Num of Beds from Returning Hosts`,
        `School`.`current_approved_nearby_host_number` AS `Total Approved Hosts`,
        `School`.`current_bed_capacity` AS `Total Approved Beds`,
        `School`.`current_pipeline_host_number` AS `Num of Hosts in Pipeline`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and ((`Student_To_School`.`deposit_amount` is not null)
                    or (`Student_To_School`.`tuition_amount` is not null)))
            then
                1
            else NULL
        end)) AS `Confirmed Re-Enrolled Students`,
        count((case
            when
                (((`Student_To_School`.`application_type` in ('SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER' , 'SCHOOL_PLACEMENT_NEW',
                    'EXISTING_APPLICATION',
                    'IN_NETWORK_TRANSFER'))
                    or isnull(`Student_To_School`.`application_type`))
                    and ((`Student_To_School`.`deposit_amount` is not null)
                    or (`Student_To_School`.`tuition_amount` is not null)))
            then
                1
            else NULL
        end)) AS `Confirmed New Students`,
        count((case
            when
                (((`Student_To_School`.`application_type` in ('SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER' , 'SCHOOL_PLACEMENT_NEW',
                    'EXISTING_APPLICATION',
                    'IN_NETWORK_TRANSFER',
                    'RE_ENROLLING'))
                    or isnull(`Student_To_School`.`application_type`))
                    and ((`Student_To_School`.`deposit_amount` is not null)
                    or (`Student_To_School`.`tuition_amount` is not null)))
            then
                1
            else NULL
        end)) AS `Total Confirmed Enrolled Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and ((select distinct
                        `Student_To_Host`.`status`
                    from
                        `Student_To_Host`
                    where
                        ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                            and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                            and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                            and (`Student_To_Host`.`status` = 'SELECTED'))) = 'SELECTED'))
            then
                1
            else NULL
        end)) AS `Matched Re-Enrolled Students`,
        count((case
            when
                (((`Student_To_School`.`application_type` in ('SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER' , 'SCHOOL_PLACEMENT_NEW',
                    'EXISTING_APPLICATION',
                    'IN_NETWORK_TRANSFER'))
                    or isnull(`Student_To_School`.`application_type`))
                    and ((select distinct
                        `Student_To_Host`.`status`
                    from
                        `Student_To_Host`
                    where
                        ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                            and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                            and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                            and (`Student_To_Host`.`status` = 'SELECTED'))) = 'SELECTED'))
            then
                1
            else NULL
        end)) AS `Matched New Students`,
        count((case
            when
                (((`Student_To_School`.`application_type` in ('SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER' , 'SCHOOL_PLACEMENT_NEW',
                    'EXISTING_APPLICATION',
                    'IN_NETWORK_TRANSFER',
                    'RE_ENROLLING'))
                    or isnull(`Student_To_School`.`application_type`))
                    and ((select distinct
                        `Student_To_Host`.`status`
                    from
                        `Student_To_Host`
                    where
                        ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                            and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                            and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                            and (`Student_To_Host`.`status` = 'SELECTED'))) = 'SELECTED'))
            then
                1
            else NULL
        end)) AS `Total Matched Students`
    from
        ((`School`
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW'))
            and (`SR_Plan`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end)))))
        left join `Student_To_School` ON (((`School`.`mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Student_To_School`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end)))))
    where
        (`School`.`short_name` not in ('TEST SCHOOL' , 'MockInt', 'DirectApply', 'Prescreen'))
    group by `School`.`mongo_id`