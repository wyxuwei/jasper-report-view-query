CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Current_Enrollment_New` AS
    select distinct
        `Student_To_School`.`school_year` AS `School Year`,
        `Student_To_School`.`enrolled_grade` AS `Student Enrolled Grade`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`chinese_name` AS `Student Chinese Name`,
        `Student`.`gender` AS `Student Gender`,
        date_format(`Student`.`date_of_birth`, '%m/%d/%Y') AS `Student Date of Birth (MM/DD/YYYY)`,
        `Student`.`passport_citizenship` AS `Student Country of Origin`,
        `Student`.`email` AS `Student Email`,
        `Student`.`phone_number` AS `Student Phone`,
        `Student`.`wechat` AS `Student WeChat`,
        `Student`.`father_email` AS `Father Email`,
        `Student`.`father_phone` AS `Father Phone`,
        `Student`.`Father_Wechat` AS `Father WeChat`,
        `Student`.`mother_email` AS `Mother Email`,
        `Student`.`mother_phone` AS `Mother Phone`,
        `Student`.`Mother_Wechat` AS `Mother WeChat`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        `School`.`city` AS `School City`,
        `School`.`partnership_type` AS `School Partnership Type`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`DSO_first_name`,
                ' ',
                `School`.`DSO_last_name`) AS `DSO Full Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        `Agency`.`english_name` AS `Agency Name`,
        `Agency`.`name` AS `Agency Name in Native Language`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        `Student`.`Father_Is_Verified_On_WeChat` AS `Father Mini Program Installed (Y/N)`,
        `Student`.`Mother_Is_Verified_On_WeChat` AS `Mother Mini Program Installed (Y/N)`,
        `Student`.`Mother_Role` AS `Mother Role`,
        `Student`.`Father_Role` AS `Father Role`,
        `Agency`.`city` AS `Agency City`,
        `Agency`.`city_english` AS `Agency City(English)`,
        `Agency`.`country` AS `Agency Country`
    from
        (((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Agency` ON ((`Agency`.`agency_id` = `Student`.`agency_id`)))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (not ((`School`.`short_name` like '%Test%')))
            and (`Student_To_School`.`school_year` = (select 
                `Current_Recruitment_Season`.`Current School Year`
            from
                `Current_Recruitment_Season`)))