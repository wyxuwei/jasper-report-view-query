CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `School_Program_Size_By_Season` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`record_id` AS `School ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`school_status` AS `School Status`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        `School`.`current_open_bed_number` AS `Open Beds`,
        `School`.`current_bed_capacity` AS `GP School Housing Capacity`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        count((case
            when
                ((`Student_To_School`.`application_type` not in ('GPHOMESTAY_ONLY' , 'HOMESTAY_ONLY'))
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            when isnull(`Student_To_School`.`application_type`) then 1
            else NULL
        end)) AS `Total Enrolled Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Fall Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            else NULL
        end)) AS `Total Re-Enrolled Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` not in ('GPHOMESTAY_ONLY' , 'RE_ENROLLING', 'HOMESTAY_ONLY'))
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Season`
                    from
                        `Current_Recruitment_Season`))
                    and (`Student_To_School`.`status` = 'ENROLLED'))
            then
                1
            else NULL
        end)) AS `Total New Enrolled Students`,
        count((case
            when
                (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_SCHOOL' , 'HOUSING_OPT_OUT',
                    'HOMESTYLE_BOARDING_THROUGH_SCHOOL',
                    'BOARDING_THROUGH_SCHOOL',
                    'FAMILY_REFERRAL',
                    'NONE'))
            then
                1
            else NULL
        end)) AS `Residential Choice(Non-GP)`,
        count((case
            when (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL')) then 1
            else NULL
        end)) AS `Residential Choice(GP Homestay)`,
        count((case
            when (`Student_To_School`.`residential_type` = 'HOST_FAMILY_THROUGH_CAMBRIDGE') then 1
            else NULL
        end)) AS `Residential Choice(GP: Host Family Through GP Homestay)`,
        count((case
            when (`Student_To_School`.`residential_type` = 'BOARDING_THROUGH_CAMBRIDGE') then 1
            else NULL
        end)) AS `Residential Choice(GP: Boarding Through GP Homestay)`,
        count((case
            when (`Student`.`passport_citizenship` = 'China') then 1
            else NULL
        end)) AS `Enrolled Chinese Students`,
        if((count((case
                when (`Student`.`passport_citizenship` = 'China') then 1
                else NULL
            end)) > 6),
            'Yes',
            'No') AS `Enrolled Chinese Students Greater than or Equal to 7`,
        count((case
            when
                ((`Student_To_School`.`application_type` not in ('GPHOMESTAY_ONLY' , 'RE_ENROLLING', 'HOMESTAY_ONLY'))
                    and (`Student`.`gender` = 'F')
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            when
                (isnull(`Student_To_School`.`application_type`)
                    and (`Student`.`gender` = 'F'))
            then
                1
            else NULL
        end)) AS `Number of New Enrolled Female Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` not in ('GPHOMESTAY_ONLY' , 'RE_ENROLLING', 'HOMESTAY_ONLY'))
                    and (`Student`.`gender` = 'M')
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            when
                (isnull(`Student_To_School`.`application_type`)
                    and (`Student`.`gender` = 'M'))
            then
                1
            else NULL
        end)) AS `Number of New Enrolled Male Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student`.`gender` = 'F')
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Fall Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            else NULL
        end)) AS `Number of Re-Enrolled Female Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student`.`gender` = 'M')
                    and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'FAMILY_REFERRAL'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Fall Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            else NULL
        end)) AS `Number of Re-Enrolled Male Students`,
        count((case `Student`.`gender`
            when 'F' then 1
            else NULL
        end)) AS `Total Female Students`,
        count((case `Student`.`gender`
            when 'M' then 1
            else NULL
        end)) AS `Total Male Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Fall Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            else NULL
        end)) AS `Total Re-Enrolled Students(For Host Tracking View)`
    from
        ((`School`
        left join `Student_To_School` ON ((`School`.`mongo_id` = `Student_To_School`.`school_mongo_id`)))
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
    where
        ((`Student_To_School`.`status` = 'ENROLLED')
            and (`School`.`short_name` not in ('MockInt' , 'DirectApply',
            'Prescreen',
            'TEST SCHOOL',
            'GPAMERICA',
            'MPP'))
            and (`Student_To_School`.`status` <> 'DELETED'))
    group by `School`.`mongo_id` , `Student_To_School`.`season`