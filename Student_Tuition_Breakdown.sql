CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Student_Tuition_Breakdown` AS
    select distinct
        `Student_To_School`.`mongo_id` AS `Application Mongo ID`,
        `Student_To_School`.`id` AS `Application ID`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`school_year` AS `School Year`,
        `Student_To_School`.`applying_grade` AS `Student Applying Grade`,
        `Student_To_School`.`enrolled_grade` AS `Student Enrolled Grade`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`passport_citizenship` AS `Student Country of Origin`,
        `School`.`id` AS `School ID`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`school_manager_TL_first_name` AS `Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Regional Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `Student_To_School`.`tpb_enrollment_contract_total` AS `Enrollment Contract Total`,
        (`Student_To_School`.`tpb_school_tuition` + `Student_To_School`.`tpb_international_student_consulting_fee`) AS `Tuition Fees`,
        `Student_To_School`.`tpb_school_tuition` AS `School Tuition`,
        `Student_To_School`.`tpb_international_student_consulting_fee` AS `International Student Consulting Fee`,
        ((`Student_To_School`.`tpb_admin_fee` + `Student_To_School`.`tpb_stipend_fee`) + `Student_To_School`.`tpb_transportation_fee`) AS `GP Residential Fees`,
        `Student_To_School`.`tpb_admin_fee` AS `Admin Fee`,
        `Student_To_School`.`tpb_stipend_fee` AS `Stipend Fee`,
        `Student_To_School`.`tpb_transportation_fee` AS `Transportation Fee`,
        (`Student_To_School`.`tpb_academic_support_provider` + `Student_To_School`.`tpb_academic_support_fee`) AS `Academic Fees`,
        `Student_To_School`.`tpb_academic_support_provider` AS `ESL Program Provider`,
        `Student_To_School`.`tpb_academic_support_fee` AS `Academic Support Fee`,
        ((`Student_To_School`.`tpb_cambridge_insurance` + `Student_To_School`.`tpb_other_insurance`) + `Student_To_School`.`tpb_insurance_note`) AS `Insurance Fee and Arrangement`,
        `Student_To_School`.`tpb_cambridge_insurance` AS `Cambridge Insurance`,
        `Student_To_School`.`tpb_other_insurance` AS `Other Insurance NOT Through Cambridge Insurance`,
        `Student_To_School`.`tpb_insurance_note` AS `Insurance Note`,
        (((`Student_To_School`.`tpb_cambridge_in_country_orientation` + `Student_To_School`.`tpb_gphomestay_orientation`) + `Student_To_School`.`tpb_school_orientation`) + `Student_To_School`.`tpb_i20_fee`) AS `First Year Fees`,
        `Student_To_School`.`tpb_cambridge_in_country_orientation` AS `Cambridge In-Country Orientation`,
        `Student_To_School`.`tpb_gphomestay_orientation` AS `GPhomestary Orientation`,
        `Student_To_School`.`tpb_school_orientation` AS `School Orientation`,
        `Student_To_School`.`tpb_i20_fee` AS `I-20 Fee`,
        `Student_To_School`.`tpb_other_fees` AS `Other Fees Description`,
        `Student_To_School`.`tpb_other_fees_total` AS `Other Fees Total`,
        `Student_To_School`.`tpb_scholarships` AS `Scholarships Description`,
        `Student_To_School`.`tpb_scholarships_total` AS `Scholarships Total`,
        `Student_To_School`.`tpb_additional_fees` AS `Additional Fees Description`,
        `Student_To_School`.`tpb_additional_fees_total` AS `Additional Fees Total`,
        `Student_To_School`.`tpb_discounts` AS `Discount Description`,
        `Student_To_School`.`tpb_discounts_total` AS `Discount Total`,
        concat(`Student`.`program_rep_first_name`,
                ' ',
                `Student`.`program_rep_last_name`) AS `Program Rep Full Name`,
        if((`Student_To_School`.`Tuition_Received` = '1'),
            'Yes',
            'No') AS `Tuition Received(Y/N)`,
        `Student_To_School`.`Tuition_Amount_Confirmed` AS `Amount Confirmed`,
        `Student_To_School`.`Tuition_Amount_Discrepancy` AS `Tuition Amount Discrepancy`,
        if((`Student_To_School`.`Full_Tuition_Paid` = '1'),
            'Yes',
            'No') AS `Student Paid Full Tuition(Y/N)`
    from
        ((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (not ((`School`.`short_name` like '%Test%'))))