CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `School_Prep_Summary` AS
    select 
        `SR_Plan`.`season` AS `Recruitment Season`,
        count(0) AS `Number of Open Schools`,
        count(`SR_Plan`.`acceptance_enrollment_contract_created_date`) AS `Number of Contracts Finished`,
        count(`SR_Plan`.`re_enrollment_offer_contract_created_date`) AS `Number of Re-enrollment Contracts Finished`,
        count((select 
                `Mat_Pak_School`.`ready_date`
            from
                `Mat_Pak_School`
            where
                ((`Mat_Pak_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
                    and (`Mat_Pak_School`.`season` = `SR_Plan`.`season`)
                    and (`Mat_Pak_School`.`language` = 'Chinese-English')
                    and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'School Mat-Pak'))
            group by `Mat_Pak_School`.`school_mongo_id` , `Mat_Pak_School`.`season` , `Mat_Pak_School`.`mat_pak_combination_mat_pak_type` , `Mat_Pak_School`.`language`)) AS `Number of Mat-Paks Finished`,
        count((select 
                `Mat_Pak_School`.`ready_date`
            from
                `Mat_Pak_School`
            where
                ((`Mat_Pak_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
                    and (`Mat_Pak_School`.`season` = `SR_Plan`.`season`)
                    and (`Mat_Pak_School`.`language` = 'Chinese-English')
                    and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-School Mat-Pak'))
            group by `Mat_Pak_School`.`school_mongo_id` , `Mat_Pak_School`.`season` , `Mat_Pak_School`.`mat_pak_combination_mat_pak_type` , `Mat_Pak_School`.`language`)) AS `Number of Re-Mat-Paks Finished`
    from
        (`SR_Plan`
        left join `School` ON ((`SR_Plan`.`school_mongo_id` = `School`.`mongo_id`)))
    where
        ((`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW'))
            and (`SR_Plan`.`school_short_name` not in ('TS' , 'GS', 'TEST SCHOOL'))
            and (`SR_Plan`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '_Fall')
            else concat(year(curdate()), '_Fall')
        end)))