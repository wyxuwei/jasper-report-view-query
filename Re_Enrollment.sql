CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Re_Enrollment` AS
    select distinct
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`current_enrolled_grade` AS `Student Current Grade`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`current_status` AS `Student Status`,
        `Student`.`current_enrolled_school_short_name` AS `Current Enrolled School Short Name`,
        if((`Student`.`current_enrolled_school_short_name` = `School`.`short_name`),
            'Yes',
            'No') AS `Re-Enrollment Application (Y/N)`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`id` AS `School ID`,
        `School`.`short_name` AS `Re-Enrolling School Short Name`,
        `School`.`state` AS `School State`,
        `School`.`region` AS `School Region`,
        `School`.`school_status` AS `School Status`,
        `Student`.`processing_rep_first_name` AS `Processing Rep First Name`,
        `Student`.`processing_rep_last_name` AS `Processing Rep Last Name`,
        concat(`Student`.`processing_rep_first_name`,
                ' ',
                `Student`.`processing_rep_last_name`) AS `Processing Rep Full Name`,
        `Student`.`processing_rep_office` AS `Processing Rep Office`,
        `Student`.`customer_service_rep_first_name` AS `Customer Service Rep First Name`,
        `Student`.`customer_service_rep_last_name` AS `Customer Service Rep Last Name`,
        concat(`Student`.`customer_service_rep_first_name`,
                ' ',
                `Student`.`customer_service_rep_last_name`) AS `Customer Service Rep Full Name`,
        `Student`.`customer_service_rep_office` AS `Customer Service Rep Office`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `School`.`SSA_TL_first_name` AS `SSA Team Lead First Name`,
        `School`.`SSA_TL_last_name` AS `SSA Team Lead Last Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        if((`SR_Plan`.`tuition_confirmed_for_season` = '1'),
            'Yes',
            'No') AS `Tuition Confirmed (Y/N)`,
        if(isnull(`SR_Plan`.`re_enrollment_offer_contract_created_date`),
            'No',
            'Yes') AS `Confirmed Re-enrollment Contract Uploaded (Y/N)`,
        `SR_Plan`.`re_enrollment_offer_contract_created_date` AS `Re-enrollment Contract Uploaded Date`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student_To_School`.`re_enrollment_offer_contract_link` AS `Re-enrollment Offer Contract Link`,
        `Student_To_School`.`re_enrollment_offer_contract_uploaded_date` AS `Re-enrollment Offer Contract Upload Date`,
        `Student_To_School`.`signed_re_enrollment_contract_link` AS `Signed Re-enrollment Contract Link`,
        `Student_To_School`.`signed_re_enrollment_contract_uploaded_date` AS `Signed Re-erollment Contract Upload Date`,
        `Student_To_School`.`deposit_amount` AS `Deposit Amount`,
        `Student_To_School`.`deposit_date` AS `Deposit Date of Payment`,
        `Student_To_School`.`tuition_amount` AS `Tuition Amount`,
        `Student_To_School`.`tuition_date` AS `Tuition Date of Payment`,
        `Student_To_School`.`deposit_recipient` AS `Deposit Recipient`,
        `Student_To_School`.`tuition_recipient` AS `Tuition Recipient`,
        `Student_To_School`.`interview_result` AS `School Decision`,
        `Student_To_School`.`reason_for_school_reject` AS `School Reject Reason`,
        `Student_To_School`.`student_decision` AS `Student Decision`,
        `Student_To_School`.`reason_for_student_reject` AS `Student Reject Reason`,
        `Student_To_School`.`Reason_for_student_reject_other` AS `Student Other Reject Reason`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`
    from
        ((((`Student`
        left join `Student_To_School` ON (((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)
            and (`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`Student_To_School`.`status` <> 'IN_NETWORK_TRANSFER')
            and (`Student_To_School`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '_Fall')
            else concat(year(curdate()), '_Fall')
        end)))))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `School` `Current_Enrolled_School` ON ((`Student`.`current_enrolled_school_mongo_id` = `Current_Enrolled_School`.`mongo_id`)))
        left join `SR_Plan` ON (((`Student`.`current_enrolled_school_mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`SR_Plan`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '_Fall')
            else concat(year(curdate()), '_Fall')
        end)))))
    where
        ((`Student`.`current_status` = 'Enrolled')
            and ((`Student`.`current_enrolled_grade` <> 'Grade_12')
            or isnull(`Student`.`current_enrolled_grade`)
            or ((`Student`.`current_enrolled_grade` = 'Grade_12')
            and (`Student_To_School`.`school_mongo_id` is not null)))
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (not ((`Current_Enrolled_School`.`short_name` like '%Test%'))))
    order by `Student_To_School`.`student_mongo_id`