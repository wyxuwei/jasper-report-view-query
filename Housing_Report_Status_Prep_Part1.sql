CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Housing_Report_Status_Prep_Part1` AS
    select 
        `Monthly_Housing_Report`.`id` AS `id`,
        `Monthly_Housing_Report`.`student_mongo_id` AS `student_mongo_id`,
        `Monthly_Housing_Report`.`school_mongo_id` AS `school_mongo_id`,
        `Monthly_Housing_Report`.`host_name` AS `host_name`,
        `Monthly_Housing_Report`.`host_email` AS `host_email`,
        `Monthly_Housing_Report`.`resco` AS `resco`,
        `Monthly_Housing_Report`.`resco_email` AS `resco_email`,
        `Monthly_Housing_Report`.`status` AS `status`,
        `Monthly_Housing_Report`.`decision_time` AS `decision_time`,
        `Monthly_Housing_Report`.`month` AS `month`,
        `Monthly_Housing_Report`.`created_on` AS `created_on`,
        `Monthly_Housing_Report`.`submitted_date` AS `submitted_date`,
        `Monthly_Housing_Report`.`last_modified_on` AS `last_modified_on`,
        `Monthly_Housing_Report`.`flag` AS `flag`,
        `Monthly_Housing_Report`.`type` AS `type`
    from
        `Monthly_Housing_Report`
    order by `Monthly_Housing_Report`.`student_mongo_id` , `Monthly_Housing_Report`.`month` , find_in_set(`Monthly_Housing_Report`.`status`,
            'DRAFTED,SUBMITTED,TRANSLATED')