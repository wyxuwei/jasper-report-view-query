CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Grievance_Report` AS
    select 
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        concat(`Student`.`customer_service_rep_first_name`,
                ' ',
                `Student`.`customer_service_rep_last_name`) AS `Customer Service Rep Full Name`,
        `Student`.`customer_service_rep_office` AS `Customer Service Rep Office`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `Grievance`.`academic_year` AS `Academic Year`,
        `Grievance`.`created_on` AS `Date Created`,
        `Grievance`.`last_modified_on` AS `Date Modified`,
        `Grievance`.`age_in_days` AS `Age in Days`,
        `Grievance`.`age_in_range` AS `Age In Range`,
        `Grievance`.`status` AS `Grievance Status`,
        `Grievance`.`first_complain_category` AS `Primary Complian Category`,
        `Grievance`.`first_complain_subcategory` AS `Primary Complain Subcategory`,
        `Grievance`.`last_note_added_on` AS `Note Update`,
        `Grievance`.`confirm_action_plan_date` AS `Confirm Action Plan Date`,
        `Grievance`.`Case_Category` AS `Case Category`,
        if((`Grievance`.`blank_action_plan` = 1),
            'No',
            'Yes') AS `Action Plan`,
        if((`Grievance`.`flag` = '1'),
            'Yes',
            'No') AS `Flag (Y/N)`,
        `Agency`.`english_name` AS `Agency Name`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`
    from
        (((`Grievance`
        left join `Student` ON ((`Grievance`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Grievance`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Agency` ON ((`Student`.`agency_id` = `Agency`.`agency_id`)))
    where
        (`School`.`short_name` <> 'TEST SCHOOL')