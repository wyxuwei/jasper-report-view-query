CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Interviews` AS
    select 
        `Student_To_School`.`mongo_id` AS `Application ID`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `School`.`mongo_id` AS `School Mongo Id`,
        `School`.`record_id` AS `School Id`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student`.`mongo_id` AS `Student Mongo Id`,
        `Student`.`record_id` AS `Student Id`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Interview_Status`.`skype_id` AS `Skype ID`,
        `Interview_Status`.`start_at_Boston_time` AS `Interview Starts at Boston Time`,
        `Interview_Status`.`start_at_school_time` AS `Interview Starts at School Time`,
        `Interview_Status`.`interview_status` AS `Status of Interview Window`,
        `Interview_Status`.`result` AS `School Decision`,
        (case
            when
                (isnull(`Student_To_School`.`interview_school_decision_date`)
                    and (`Interview_Status`.`result` = 'APPROVED'))
            then
                `Student_To_School`.`status_turn_arround_date_accepted`
            when
                (isnull(`Student_To_School`.`interview_school_decision_date`)
                    and (`Interview_Status`.`result` = 'REJECTED'))
            then
                `Student_To_School`.`status_turn_arround_date_rejected`
            when
                (isnull(`Student_To_School`.`interview_school_decision_date`)
                    and (`Interview_Status`.`result` = 'CONDITIONAL'))
            then
                `Student_To_School`.`status_turn_arround_date_conditional_acepted`
            else `Student_To_School`.`interview_school_decision_date`
        end) AS `School Decision Upload Date`,
        if(((((to_days(now()) - to_days(`Interview_Status`.`start_at_Boston_time`)) > 7)
                and isnull((case
                        when
                            (isnull(`Student_To_School`.`interview_school_decision_date`)
                                and (`Interview_Status`.`result` = 'APPROVED'))
                        then
                            `Student_To_School`.`status_turn_arround_date_accepted`
                        when
                            (isnull(`Student_To_School`.`interview_school_decision_date`)
                                and (`Interview_Status`.`result` = 'REJECTED'))
                        then
                            `Student_To_School`.`status_turn_arround_date_rejected`
                        when
                            (isnull(`Student_To_School`.`interview_school_decision_date`)
                                and (`Interview_Status`.`result` = 'CONDITIONAL'))
                        then
                            `Student_To_School`.`status_turn_arround_date_conditional_acepted`
                        else `Student_To_School`.`interview_school_decision_date`
                    end)))
                or ((to_days((case
                        when
                            (isnull(`Student_To_School`.`interview_school_decision_date`)
                                and (`Interview_Status`.`result` = 'APPROVED'))
                        then
                            `Student_To_School`.`status_turn_arround_date_accepted`
                        when
                            (isnull(`Student_To_School`.`interview_school_decision_date`)
                                and (`Interview_Status`.`result` = 'REJECTED'))
                        then
                            `Student_To_School`.`status_turn_arround_date_rejected`
                        when
                            (isnull(`Student_To_School`.`interview_school_decision_date`)
                                and (`Interview_Status`.`result` = 'CONDITIONAL'))
                        then
                            `Student_To_School`.`status_turn_arround_date_conditional_acepted`
                        else `Student_To_School`.`interview_school_decision_date`
                    end)) - to_days(`Interview_Status`.`start_at_Boston_time`)) > 7)),
            'Yes',
            'No') AS `Alert: Turnaround Time over 7 days`,
        (to_days((case
                    when
                        (isnull(`Student_To_School`.`interview_school_decision_date`)
                            and (`Interview_Status`.`result` = 'APPROVED'))
                    then
                        `Student_To_School`.`status_turn_arround_date_accepted`
                    when
                        (isnull(`Student_To_School`.`interview_school_decision_date`)
                            and (`Interview_Status`.`result` = 'REJECTED'))
                    then
                        `Student_To_School`.`status_turn_arround_date_rejected`
                    when
                        (isnull(`Student_To_School`.`interview_school_decision_date`)
                            and (`Interview_Status`.`result` = 'CONDITIONAL'))
                    then
                        `Student_To_School`.`status_turn_arround_date_conditional_acepted`
                    else `Student_To_School`.`interview_school_decision_date`
                end)) - to_days(`Interview_Status`.`start_at_Boston_time`)) AS `Turnaround Time Between Interview and School Decision`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`Student`.`program_rep_first_name`,
                ' ',
                `Student`.`program_rep_last_name`) AS `Program Rep Full Name`,
        `Student`.`program_rep_office` AS `Program Rep Office`
    from
        (((`Interview_Status`
        left join `School` ON ((`Interview_Status`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Student` ON ((`Interview_Status`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Student_To_School` ON ((`Interview_Status`.`student_to_school_mongo_id` = `Student_To_School`.`mongo_id`)))
    where
        (((`Student_To_School`.`application_type` in ('EXISTING_APPLICATION' , 'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            or isnull(`Student_To_School`.`application_type`))
            and (`Interview_Status`.`start_at_Boston_time` is not null)
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%'))))