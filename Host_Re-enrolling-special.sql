CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Re-enrolling-special` AS
    select distinct
        `Host`.`mongo_id` AS `Host Mongo ID`,
        `Host`.`primary_contact_name` AS `Host Full Name`,
        `Host`.`state` AS `Host State`,
        `Host`.`city` AS `Host City`,
        `Host`.`status` AS `Host Compliance Status`,
        `School`.`short_name` AS `Host Family Connected School`,
        `School`.`full_name` AS `Host Family Connected School Full Name`,
        `School`.`region` AS `School Region`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`HPC_first_name` AS `HPC First Name`,
        `School`.`HPC_last_name` AS `HPC Last Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        `Host_Current_Season`.`school_year` AS `Host Current Season`,
        `Host_Current_Season`.`host_type` AS `Host Type (Current Season)`,
        `Host_Current_Season`.`status` AS `Host Current Season Status`,
        `Host_Next_Season`.`school_year` AS `Host Next Season`,
        `Host_Next_Season`.`host_type` AS `Host Type (Next Season)`,
        `Host_Next_Season`.`status` AS `Host Next Season Status`,
        if((`Host_Next_Season`.`do_we_want_the_host` = '1'),
            'Yes',
            if((`Host_Next_Season`.`do_we_want_the_host` = '0'),
                'No',
                NULL)) AS `Do we want the host to return`,
        `Host_Next_Season`.`reason_we_dont_want_the_host` AS `Why we rejected the returning host`,
        if((`Host_Next_Season`.`does_host_want_to_host` = '1'),
            'Yes',
            if((`Host_Next_Season`.`does_host_want_to_host` = '0'),
                'No',
                NULL)) AS `Does the host want to return`,
        `Host_Next_Season`.`reason_host_doesnt_want_to_host` AS `Why the host rejected to return`,
        concat('https://www.ciiedu.net/ciie.html#/hostfamily/hostfamilyDetail/information?hostfamilyId=',
                `Host`.`mongo_id`) AS `Link to host family page in Harmony (URL) host page`,
        count((case `Student_To_Host`.`school_year`
            when
                (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat(year(curdate()),
                                '/',
                                (right(year(curdate()), 2) + 1))
                    else concat((year(curdate()) - 1),
                            '/',
                            right(year(curdate()), 2))
                end)
            then
                1
            else NULL
        end)) AS `Currently/Previously Matched Students (Current Year)`,
        group_concat(if((`Student_To_Host`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat(year(curdate()),
                                '/',
                                (right(year(curdate()), 2) + 1))
                    else concat((year(curdate()) - 1),
                            '/',
                            right(year(curdate()), 2))
                end)),
                concat(`Student`.`first_name`,
                        ' ',
                        `Student`.`last_name`),
                NULL)
            separator '; ') AS `Currently/Previously Matched Student Name(Current Year)`,
        count((case `Student_To_Host`.`school_year`
            when
                (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat((year(curdate()) + 1),
                                '/',
                                (right(year(curdate()), 2) + 2))
                    else concat(year(curdate()),
                            '/',
                            (right(year(curdate()), 2) + 1))
                end)
            then
                1
            else NULL
        end)) AS `Currently/Previously Matched Students (Next Year)`,
        group_concat(if((`Student_To_Host`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat((year(curdate()) + 1),
                                '/',
                                (right(year(curdate()), 2) + 2))
                    else concat(year(curdate()),
                            '/',
                            (right(year(curdate()), 2) + 1))
                end)),
                concat(`Student`.`first_name`,
                        ' ',
                        `Student`.`last_name`),
                NULL)
            separator '; ') AS `Hosted Student Name (Currently/Previously Matched Next Year)`,
        count(if(((`Student_To_Host`.`school_year` = (case
                when
                    (month(curdate()) >= 9)
                then
                    concat(year(curdate()),
                            '/',
                            (right(year(curdate()), 2) + 1))
                else concat((year(curdate()) - 1),
                        '/',
                        right(year(curdate()), 2))
            end))
                and (`Student_To_Host`.`status` = 'SELECTED')),
            1,
            NULL)) AS `Matched Students (Current Year)`,
        group_concat(if(((`Student_To_Host`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat(year(curdate()),
                                '/',
                                (right(year(curdate()), 2) + 1))
                    else concat((year(curdate()) - 1),
                            '/',
                            right(year(curdate()), 2))
                end))
                    and (`Student_To_Host`.`status` = 'SELECTED')),
                concat(`Student`.`first_name`,
                        ' ',
                        `Student`.`last_name`),
                NULL)
            separator '; ') AS `Matched Student Name (Current Year)`,
        count(if(((`Student_To_Host`.`school_year` = (case
                when
                    (month(curdate()) >= 9)
                then
                    concat((year(curdate()) + 1),
                            '/',
                            (right(year(curdate()), 2) + 2))
                else concat(year(curdate()),
                        '/',
                        (right(year(curdate()), 2) + 1))
            end))
                and (`Student_To_Host`.`status` = 'SELECTED')),
            1,
            NULL)) AS `Matched Students (Next Year)`,
        group_concat(if(((`Student_To_Host`.`school_year` = (case
                    when
                        (month(curdate()) >= 9)
                    then
                        concat((year(curdate()) + 1),
                                '/',
                                (right(year(curdate()), 2) + 2))
                    else concat(year(curdate()),
                            '/',
                            (right(year(curdate()), 2) + 1))
                end))
                    and (`Student_To_Host`.`status` = 'SELECTED')),
                concat(`Student`.`first_name`,
                        ' ',
                        `Student`.`last_name`),
                NULL)
            separator '; ') AS `Matched Student Name (Next Year)`,
        `Host`.`Next_School_Year_DocuSign_Contract_Initial_Sent_Date` AS `Next School Year DocuSign Contract Sent Date`,
        `Host`.`Next_School_Year_DocuSign_Contract_Signed_Date` AS `Next School Year DocuSign Contract Signed Date`,
        if((`Host`.`Next_School_Year_Alternative_Method_For_Contract` = '1'),
            'Yes',
            'No') AS `Next School Year Alternative Method For Contract(Y/N)`,
        `Host`.`Next_School_Year_Offline_Contract_Signed_Date` AS `Next School Year Offline Contract Signed Date`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`
    from
        (((((`Host`
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))
        left join `Host_Season` `Host_Current_Season` ON (((`Host`.`mongo_id` = `Host_Current_Season`.`host_mongo_id`)
            and (`Host_Current_Season`.`school_year` = (case
            when (month(curdate()) >= 9) then concat(year(curdate()), '/', (right(year(curdate()), 2) + 1))
            else concat((year(curdate()) - 1), '/', right(year(curdate()), 2))
        end)))))
        left join `Host_Season` `Host_Next_Season` ON (((`Host`.`mongo_id` = `Host_Next_Season`.`host_mongo_id`)
            and (`Host_Next_Season`.`school_year` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '/', (right(year(curdate()), 2) + 2))
            else concat(year(curdate()), '/', (right(year(curdate()), 2) + 1))
        end)))))
        left join `Student_To_Host` ON (((`Host`.`mongo_id` = `Student_To_Host`.`host_mongo_id`)
            and (`Student_To_Host`.`status` in ('SELECTED' , 'PREVIOUS_HOST')))))
        left join `Student` ON ((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)))
    where
        ((`Host`.`status` in ('APPROVED' , 'BACKGROUND_CHECK_EXPIRED'))
            and (isnull(`Host_Next_Season`.`host_type`)
            or (`Host_Next_Season`.`host_type` = 'REENROLLING')))
    group by `Host`.`mongo_id`