CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `FEA_ParentTalk_Notes_Tracking_Report` AS
    select 
        `Student`.`record_id` AS `Student ID`,
        `Student_To_School`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `Student_To_School`.`enrolled_grade` AS `Student Enrolled Grade`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student`.`passport_citizenship` AS `Country of Origin`,
        `Student`.`agency_id` AS `Agency ID`,
        `Student`.`agency_english_name` AS `Agency Name (English)`,
        `Student`.`Agency_Name` AS `Agency Name (Native Language)`,
        `Parent_Talk_Note`.`Created_On` AS `Date of Notes Created On`,
        `Parent_Talk_Note`.`Message` AS `Notes Content`
    from
        (((`Parent_Talk_Note`
        left join `Student_To_School` ON ((`Parent_Talk_Note`.`Student_To_School_Mongo_Id` = `Student_To_School`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
    where
        (`Student_To_School`.`status` in ('ENROLLED' , 'EXPELLED',
            'WITHDREW',
            'IN_NETWORK_TRANSFER',
            'OUT_OF_NETWORK_TRANSFER'))