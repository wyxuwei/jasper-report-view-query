CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Initial_Wellness_Report_Status` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `School`.`SSA_TL_first_name` AS `SSA Team Lead First Name`,
        `School`.`SSA_TL_last_name` AS `SSA Team Lead Last Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        `Student`.`customer_service_rep_first_name` AS `Customer Service Rep First Name`,
        `Student`.`customer_service_rep_last_name` AS `Customer Service Rep Last Name`,
        concat(`Student`.`customer_service_rep_first_name`,
                ' ',
                `Student`.`customer_service_rep_last_name`) AS `Customer Service Rep Full Name`,
        `School`.`region` AS `School Region`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`english_name` AS `Student English Name`,
        `Student`.`email` AS `Student Email(Hidden)`,
        `Student`.`current_phone` AS `Student US Cell Number(Hidden)`,
        `Student`.`wechat` AS `Student WeChat(Hidden)`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        cast(`SR_Plan`.`new_arrival_date` as date) AS `New Student Arrival Date`,
        cast(`SR_Plan`.`returning_arrival_date` as date) AS `Returning Student Arrival Date`,
        `Initial_Housing_Report`.`submitted_date` AS `Initial Wellness Report Submitted Date`,
        `Initial_Housing_Report`.`flag` AS `Initial Wellness Report Flag`,
        concat('Name: ',
                `User_Profile`.`first_name`,
                ' ',
                `User_Profile`.`last_name`,
                '; Title: ',
                `User_Profile`.`title`) AS `Last Modified By`,
        (to_days(`Initial_Housing_Report`.`submitted_date`) - to_days(`Initial_Housing_Report`.`arrival_date`)) AS `Turnaround Time since Arrival`,
        `Initial_Housing_Report`.`arrival_date` AS `Student Arrival Date`,
        concat('https://www.ciiedu.net/ciie.html#/student/studentDetail/studentReportingCards?studentId=',
                `Student`.`mongo_id`) AS `Link to Initial Wellness Report`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        `Initial_Housing_Report`.`today` AS `Todays Date`,
        `Initial_Housing_Report`.`status` AS `Status`,
        `Student_To_School`.`status` AS `Application Status`,
        if((`Student_To_School`.`Full_Tuition_Paid` = '1'),
            'Yes',
            'No') AS `Student Paid Full Tuition(Y/N)`
    from
        (((((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Initial_Housing_Report` ON (((`Initial_Housing_Report`.`student_mongo_id` = `Student`.`mongo_id`)
            and (`Initial_Housing_Report`.`school_mongo_id` = `School`.`mongo_id`)
            and (`Initial_Housing_Report`.`season` = `Student_To_School`.`season`))))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `User_Profile` ON ((`Initial_Housing_Report`.`last_modified_by` = `User_Profile`.`mongo_id`)))
    where
        ((((`Student_To_School`.`application_type` in ('NEW' , 'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER'))
            and (`Student_To_School`.`residential_type` not in ('HOUSING_OPT_OUT' , 'HOUSING_OPT_OUT_SCHOOL',
            'HOUSING_OPT_OUT_HOMESTAY')))
            or ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'BOARDING_THROUGH_CAMBRIDGE'))))
            and (`Student_To_School`.`status` in ('ENROLLED' , 'EXPELLED', 'WITHDREW')))