CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Current_Academic_Year_School_Program_Size` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`record_id` AS `School ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        `School`.`partnership_type_china` AS `School Partnership Type Of China`,
        `School`.`partnership_type_vietnam` AS `School Partnership Type Of Vietnam`,
        `School`.`partnership_type_hongkong` AS `School Partnership Type Of HongKong`,
        `School`.`partnership_type_residential` AS `School Partnership Type Of Residential`,
        `School`.`partnership_type_taiwan` AS `School Partnership Type Of TaiWan`,
        `School`.`partnership_type_macau` AS `School Partnership Type Of Macau`,
        `School`.`partnership_type_global` AS `School Partnership Type Of Global`,
        `School`.`partnership_type_korea` AS `School Partnership Type Of Korea`,
        `School`.`partnership_type_original_china` AS `School Partnership Original Type Of China`,
        `School`.`partnership_type_original_vietnam` AS `School Partnership Original Type Of Vietnam`,
        `School`.`partnership_type_original_hongkong` AS `School Partnership Original Type Of HongKong`,
        `School`.`partnership_type_original_residential` AS `School Partnership Original Type Of Residential`,
        `School`.`partnership_type_original_taiwan` AS `School Partnership Original Type Of TaiWan`,
        `School`.`partnership_type_original_macau` AS `School Partnership Original Type Of Macau`,
        `School`.`partnership_type_original_global` AS `School Partnership Original Type Of Global`,
        `School`.`partnership_type_original_korea` AS `School Partnership Original Type Of Korea`,
        `School`.`partnership_type_original_all` AS `School Partnership Original Type Of All`,
        `School`.`school_status` AS `School Partnership Status`,
        `School`.`relationship_status` AS `School Relationship Status`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        `School`.`first_recruitment_season` AS `First Recruitment Season`,
        `School`.`current_open_bed_number` AS `Open Beds`,
        `School`.`current_bed_capacity` AS `GP School Housing Capacity`,
        'Current Academic Year' AS `Recuritment Season`,
        `School`.`school_manager_TL_first_name` AS `Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Regional Manager Last Name`,
        `SR_Plan`.`boys_residential_system` AS `Boys Residential Accomodation`,
        `SR_Plan`.`girls_residential_system` AS `Girls Residential Accomodation`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        count(`Student_To_School`.`student_mongo_id`) AS `Num of Total Current Students (Inc. Grade 12)`,
        count((case `Student`.`current_enrolled_grade`
            when 'Grade_12' then 1
            else NULL
        end)) AS `Num of Graduating Seniors`,
        count((case `Student`.`current_enrolled_grade`
            when 'Grade_11' then 1
            else NULL
        end)) AS `Num of Grade 11 Students`,
        count((case `Student`.`current_enrolled_grade`
            when 'Grade_10' then 1
            else NULL
        end)) AS `Num of Grade 10 Students`,
        count((case `Student`.`current_enrolled_grade`
            when 'Grade_9' then 1
            else NULL
        end)) AS `Num of Grade 9 Students`,
        count((case `Student`.`current_enrolled_grade`
            when 'Grade_8' then 1
            else NULL
        end)) AS `Num of Grade 8 Students`,
        count((case `Student`.`current_enrolled_grade`
            when 'Grade_7' then 1
            when 'Grade_6' then 1
            when 'Grade_5' then 1
            when 'Grade_4' then 1
            when 'Grade_3' then 1
            when 'Grade_2' then 1
            when 'Grade_1' then 1
            else NULL
        end)) AS `Num of Grade 7 or lower Students`,
        count((case `Student`.`gender`
            when 'F' then 1
            else NULL
        end)) AS `Num of Female Students`,
        count((case `Student`.`gender`
            when 'M' then 1
            else NULL
        end)) AS `Num of Male Students`,
        count((case
            when
                (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_SCHOOL' , 'HOUSING_OPT_OUT',
                    'HOMESTYLE_BOARDING_THROUGH_SCHOOL',
                    'BOARDING_THROUGH_SCHOOL'))
            then
                1
            else NULL
        end)) AS `Residential Choice(Non-GP)`,
        count((case
            when (`Student_To_School`.`residential_type` in ('HOST_FAMILY_THROUGH_CAMBRIDGE' , 'BOARDING_THROUGH_CAMBRIDGE')) then 1
            else NULL
        end)) AS `Residential Choice(GP Homestay)`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`
    from
        (((`School`
        left join `Student_To_School` ON ((`School`.`mongo_id` = `Student_To_School`.`school_mongo_id`)))
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `SR_Plan` ON (((`SR_Plan`.`school_mongo_id` = `School`.`mongo_id`)
            and (`SR_Plan`.`season` = `Student_To_School`.`season`))))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` = 'ENROLLED')
            and ((`Student_To_School`.`season` = (case
            when (month(curdate()) >= 9) then concat(year(curdate()), '_Fall')
            else concat((year(curdate()) - 1), '_Fall')
        end))
            or (`Student_To_School`.`season` = (case
            when (month(curdate()) >= 9) then ''
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                ''
            else concat(year(curdate()), '__Spring')
        end))))
    group by `School`.`mongo_id`