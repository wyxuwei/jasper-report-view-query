CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Tracking` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`mailing_address` AS `School Mailing Address`,
        `School`.`region` AS `School Region`,
        `School`.`relationship_status` AS `School Relationship Status`,
        `School`.`partnership_type` AS `School Partnership Type`,
        `School`.`school_status` AS `School Partnership Status`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        `SR_Plan_with_Calculation`.`Recruitment Season` AS `Recruitment Season`,
        (case
            when
                (month(curdate()) < 9)
            then
                concat((year(curdate()) - 1),
                        '/',
                        right(year(curdate()), 2))
            else concat(year(curdate()),
                    '/',
                    (right(year(curdate()), 2) + 1))
        end) AS `Hosting Year`,
        `School`.`HPC_first_name` AS `HCC First Name`,
        `School`.`HPC_last_name` AS `HCC Last Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        `School`.`HRC_first_name` AS `HRC First Name`,
        `School`.`HRC_last_name` AS `HRC Last Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`inquiry_number` AS `Host Inquiries (Include Applications Sent)`,
        `School`.`number_of_hosts_at_background_check` AS `Host Apps - Outstanding BGC`,
        `School`.`number_Of_hosts_at_site_visit` AS `Host Apps - Outstanding SV`,
        `School`.`number_of_hosts_at_contract` AS `Host Apps - Outstanding Contract`,
        `School`.`number_of_hosts_at_reference` AS `Host Apps - Outstanding Reference Checks`,
        `School`.`current_pipeline_host_number` AS `Hosts in Pipeline`,
        (`School`.`current_approved_connected_host_number` - `School`.`current_approved_connected_no_student_host_number`) AS `Matched/Approved Host`,
        `School`.`current_approved_connected_no_student_host_number` AS `Unmatched/Approved Host`,
        `School`.`current_approved_connected_host_number` AS `Total Approved Hosts`,
        (`School`.`current_active_connected_host_number` - `School`.`current_active_connected_no_student_host_number`) AS `Matched Active/UR/Pending Hosts`,
        `School`.`current_active_connected_no_student_host_number` AS `Unmatched Active/UR/Pending Hosts`,
        `School`.`current_active_connected_host_number` AS `Total Active/UR/Pending Hosts`,
        `School`.`bed_capacity_from_current_active_connected_host` AS `Beds Available`,
        `School`.`number_of_hosting_hosts_we_want_to_return` AS `Currently Matched Hosts We Want to Return`,
        `School`.`number_of_hosting_hosts_will_return` AS `Currently Matched Hosts Will Return`,
        `School`.`number_of_not_hosting_hosts_we_want_to_return` AS `Currently Unmatched Hosts We Want to Return`,
        `School`.`number_of_not_hosting_hosts_will_return` AS `Currently Unmatched Hosts Will Return`,
        (`School`.`number_of_hosting_hosts_will_return` + `School`.`number_of_not_hosting_hosts_will_return`) AS `Estimated Returning Hosts`,
        `School`.`bed_capacity_from_returning_hosts` AS `Estimated Beds of Returning Hosts`,
        (`School`.`number_of_confirmed_returning_hosts` - `School`.`number_of_confirmed_returning_not_hosting_hosts`) AS `Confirmed Returning/Currently Matched Hosts`,
        `School`.`number_of_confirmed_returning_not_hosting_hosts` AS `Confirmed Returning/Currently Unmatched Hosts`,
        `School`.`number_of_confirmed_new_hosts` AS `Confirmed New Hosts`,
        (`School`.`number_of_confirmed_returning_hosts` + `School`.`number_of_confirmed_new_hosts`) AS `Total Confirmed Hosts`,
        (`School`.`bed_capacity_from_confirmed_returning_hosts` - `School`.`bed_capacity_from_confirmed_returning_not_hosting_hosts`) AS `Beds of Returning/Currently Matched Hosts`,
        `School`.`bed_capacity_from_confirmed_returning_not_hosting_hosts` AS `Beds of Returning/Currently Unmatched Hosts`,
        (`School`.`bed_capacity_from_confirmed_hosts` - `School`.`bed_capacity_from_confirmed_returning_hosts`) AS `total number of confirmed beds of brand new hosts`,
        `School`.`bed_capacity_from_confirmed_hosts` AS `Total Confirmed Beds`,
        `School_Program_Size_By_Season`.`Number of New Enrolled Female Students` AS `New GP Enrolled Students - Female`,
        `School_Program_Size_By_Season`.`Number of New Enrolled Male Students` AS `New GP Enrolled Students - Male`,
        `School_Program_Size_By_Season`.`Total New Enrolled Students` AS `Total New GP Enrolled Students`,
        `School_Program_Size_By_Season`.`Number of Re-Enrolled Female Students` AS `GP Re-Enrolled Students - Female`,
        `School_Program_Size_By_Season`.`Number of Re-Enrolled Male Students` AS `GP Re-Enrolled Students - Male`,
        `School_Program_Size_By_Season`.`Total Re-Enrolled Students` AS `Total GP Re-enrolled Students`,
        `School_Program_Size_By_Season`.`Total Re-Enrolled Students(For Host Tracking View)` AS `Total Re-enrolled Students`,
        `School_Program_Size_By_Season`.`Total Enrolled Students` AS `Total GP Enrolled Student (New + Re-enrolled)`,
        `School_Program_Size_By_Season`.`Residential Choice(GP Homestay)` AS `Total GP Students`,
        `School_Program_Size_By_Season`.`Residential Choice(Non-GP)` AS `Total Non-GP Students`,
        `SR_Plan_Spring`.`agreed_target` AS `Spring Student Recruitment Agreed Target`,
        `SR_Plan_Spring`.`sales_approved_target` AS `Spring Student Recruitment Committed Target`,
        `SR_Plan_Fall`.`agreed_target` AS `Fall Student Recruitment Agreed Target`,
        `SR_Plan_Fall`.`sales_approved_target` AS `Fall Student Recruitment Committed Target`,
        `Re_Enrollment_Summary_By_School`.`Potential Re-Enrollments` AS `Potential Re-enrolling Students for The Coming Fall Season`,
        `Re_Enrollment_Summary_By_School`.`Offers Uploaded` AS `Number of Student Re-enrollment Offer Uploaded`,
        (`Re_Enrollment_Summary_By_School`.`Potential Re-Enrollments` + `SR_Plan_Fall`.`agreed_target`) AS `Maximum Potential Program Size (Fall Season)`,
        `Matched_Host_by_Connected_School_Prep`.`Number of Matched Hosts` AS `Matched Hosts for Current Student Recruitment Season`,
        `Matched_Host_by_Connected_School_Prep`.`Number of Matched Beds` AS `Matched Beds for Current Student Recruitment Season`,
        if(((month(curdate()) >= 9)
                or ((month(curdate()) = 1)
                and (dayofmonth(curdate()) <= 15))),
            (`School`.`current_active_connected_host_number` - `Matched_Host_by_Connected_School_Prep`.`Number of Matched Hosts`),
            ((`School`.`number_of_confirmed_returning_hosts` + `School`.`number_of_confirmed_new_hosts`) - `Matched_Host_by_Connected_School_Prep`.`Number of Matched Hosts`)) AS `Unmatched Hosts for Current Student Recruitment Season`,
        if(((month(curdate()) >= 9)
                or ((month(curdate()) = 1)
                and (dayofmonth(curdate()) <= 15))),
            (`School`.`current_bed_capacity` - `Matched_Host_by_Connected_School_Prep`.`Number of Matched Beds`),
            (`School`.`bed_capacity_from_confirmed_hosts` - `Matched_Host_by_Connected_School_Prep`.`Number of Matched Beds`)) AS `Unmatched Beds for Current Student Recruitment Season`,
        `SR_Plan_with_Calculation`.`Number of Applicant` AS `New Student Applicants`,
        `SR_Plan_with_Calculation`.`Number of Offer Letter` AS `New Student Enrollment Offer Sent Out`,
        `SR_Plan_with_Calculation`.`Number of Deposit` AS `New Enrolled Student`,
        `SR_Plan_with_Calculation`.`New GP Enrolled Student` AS `New GP Enrolled Student`,
        `School_Program_Size_By_Season_Matched`.`Matched GP New Enrolled Students` AS `Matched GP New Enrolled Students`,
        `School_Program_Size_By_Season_Matched`.`Matched GP Re-enrolled Students` AS `Matched GP Re-enrolled Students`,
        `SR_Plan_Spring`.`status` AS `Recruitment Season SR Plan Status`,
        concat(left(((`School_Program_Size_By_Season_Matched`.`Matched GP New Enrolled Students` / `School_Program_Size_By_Season`.`Total New Enrolled Students`) * 100),
                    5),
                '%') AS `% Of Matched New GP Students By Enrolled New GP Students`,
        `School`.`priority_level` AS `School Priority Level`,
        `School`.`cluster` AS `School Cluster`,
        `School`.`market_ability_x` AS `School Marketability (X)`,
        `School`.`market_ability_y` AS `School Marketability (Y)`,
        `School`.`Last_School_Year_Host_Reenroll_Rate` AS `Last School Year Host Reenroll Rate`,
        `School`.`Number_Of_Confirmed_New_Hosts_Next_School_Year` AS `Number Of Confirmed New Hosts Next School Year`,
        `School`.`Number_Of_Confirmed_Returning_Hosts_Next_School_Year` AS `Number Of Confirmed Returning Hosts Next School Year`,
        `School`.`Current_Season_Student_Application_Number` AS `Current Season Student Application Number`,
        `School`.`Current_Season_Potential_Reenroll_Number` AS `Current Season Potential Reenroll Number`,
        `School`.`Last_Season_Student_Reenroll_Rate` AS `Last Season Student Reenroll Rate`,
        `School`.`Last_Season_Student_New_Enroll_Rate` AS `Last Season Student New Enroll Rate`,
        `School`.`Next_Season_Confirmed_New_Enroll_Student_Number` AS `Next Season Confirmed New Enroll Student Number`,
        `School`.`Next_Season_Confirmed_Reenroll_Student_Number` AS `Next Season Confirmed Reenroll Student Number`,
        `School`.`Host_Recruitment_Threshold` AS `Host Inventory Threshold`,
        `School`.`Host_Inventory_Indicator_Projected_Enrollment` AS `Host Inventory Indicator-Projected Enrollment (Number)`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_Inventory_Indicator_Projected_Enrollment` >= 1)
                        and (`School`.`Host_Inventory_Indicator_Projected_Enrollment` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Inventory_Indicator_Projected_Enrollment` >= 3) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Inventory_Indicator_Projected_Enrollment`)
                        or (`School`.`Host_Inventory_Indicator_Projected_Enrollment` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_Inventory_Indicator_Projected_Enrollment` >= 1)
                        and (`School`.`Host_Inventory_Indicator_Projected_Enrollment` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Inventory_Indicator_Projected_Enrollment` >= 1.25) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Inventory_Indicator_Projected_Enrollment`)
                        or (`School`.`Host_Inventory_Indicator_Projected_Enrollment` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end)) AS `Host Inventory Indictor-Projected Enrollment (Text)`,
        `School`.`Host_Inventory_Indicator_Actual_Enrollment` AS `Active Host Inventory Indicator-Actual Enrollment (Number)`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_Inventory_Indicator_Actual_Enrollment` >= 1)
                        and (`School`.`Host_Inventory_Indicator_Actual_Enrollment` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Inventory_Indicator_Actual_Enrollment` >= 3) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Inventory_Indicator_Actual_Enrollment`)
                        or ((`School`.`Host_Inventory_Indicator_Actual_Enrollment` = 0)
                        and (`School_Program_Size_By_Season`.`Total Enrolled Students` = 0)))
                then
                    'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_Inventory_Indicator_Actual_Enrollment` >= 1)
                        and (`School`.`Host_Inventory_Indicator_Actual_Enrollment` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Inventory_Indicator_Actual_Enrollment` >= 1.25) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Inventory_Indicator_Actual_Enrollment`)
                        or ((`School`.`Host_Inventory_Indicator_Actual_Enrollment` = 0)
                        and (`School_Program_Size_By_Season`.`Total Enrolled Students` = 0)))
                then
                    'NA'
                else 'Inventory Warning'
            end)) AS `Active Host Inventory Indictor-Actual Enrollment (Text)`,
        `School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` AS `Host Bed Inventory Indicator Projected Enrollment (Number)`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` >= 1)
                        and (`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` >= 3) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment`)
                        or (`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` >= 1)
                        and (`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` >= 1.25) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment`)
                        or (`School`.`Host_Bed_Inventory_Indicator_Projected_Enrollment` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end)) AS `Host Bed Inventory Indicator Projected Enrollment (Text)`,
        `School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` AS `Active Host Inventory Indicator(Bed)-Actual Enrollment`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` >= 1)
                        and (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` >= 3) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment`)
                        or (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` >= 1)
                        and (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` >= 1.25) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment`)
                        or (`School`.`Host_Bed_Inventory_Indicator_Actual_Enrollment` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end)) AS `Active Host Inventory Indicator(Bed)-Actual Enrollment(Text)`,
        `School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` AS `AUR Host Inventory Indicator for Fall Recruitment Season`,
        if((`School`.`Host_Recruitment_Threshold` = '300%'),
            (case
                when
                    ((`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` >= 1)
                        and (`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` < 3))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` >= 3) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status`)
                        or (`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end),
            (case
                when
                    ((`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` >= 1)
                        and (`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` < 1.25))
                then
                    'Inventory Insufficient'
                when (`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` >= 1.25) then 'Inventory Sufficient'
                when
                    (isnull(`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status`)
                        or (`School`.`Host_II_Actual_Enrollment_Special_Host_Season_Status` = 0))
                then
                    'NA'
                else 'Inventory Warning'
            end)) AS `AUR Host Inventory Indicator for Fall Recruitment Season(Text)`,
        `School`.`Current_Under_Review_Connected_Approved_Bgce_Host_Number` AS `UR/Pending Hosts`,
        `School_Season`.`Number_Of_Abandoned_New_Application` AS `Number Of Abandoned New Students`,
        `School_Season`.`Number_Of_Abandoned_Returning_Application` AS `Number Of Abandoned Returning Students`,
        `School_Season`.`Number_Of_Declined_New_Application` AS `Number Of Declined New Students`,
        `School_Season`.`Number_Of_Declined_Returning_Application` AS `Number Of Declined Returning Students`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        `SR_Plan_with_Calculation`.`School SR Plan Status` AS `SR_Plan Status`,
        `School_Season`.`Number_Of_Homestay_Only_Student_Application` AS `Number Of Homestay Only Student Application`,
        `School_Season`.`Number_Of_Homestay_Only_Student_Application_With_Deposit` AS `Number Of Homestay Only Student Application With Deposit`,
        `School_Season`.`Number_Of_Potential_Or_Applying_Application` AS `Number Of Potential Or Applying Application`
    from
        ((((((((`School`
        left join `SR_Plan` `SR_Plan_Spring` ON (((`SR_Plan_Spring`.`school_mongo_id` = `School`.`mongo_id`)
            and (`SR_Plan_Spring`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            else concat(year(curdate()), '__Spring')
        end))
            and (`SR_Plan_Spring`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `SR_Plan` `SR_Plan_Fall` ON (((`SR_Plan_Fall`.`school_mongo_id` = `School`.`mongo_id`)
            and (`SR_Plan_Fall`.`season` = (case
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '_Fall')
            else concat(year(curdate()), '_Fall')
        end))
            and (`SR_Plan_Fall`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `SR_Plan_with_Calculation` ON (((`School`.`mongo_id` = `SR_Plan_with_Calculation`.`School Mongo ID`)
            and (`SR_Plan_with_Calculation`.`Recruitment Season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end)))))
        left join `School_Program_Size_By_Season` ON (((`School`.`mongo_id` = `School_Program_Size_By_Season`.`School Mongo ID`)
            and (`School_Program_Size_By_Season`.`Recruitment Season` = `SR_Plan_with_Calculation`.`Recruitment Season`))))
        left join `School_Program_Size_By_Season_Matched` ON (((`School`.`mongo_id` = `School_Program_Size_By_Season_Matched`.`School Mongo ID`)
            and (`School_Program_Size_By_Season_Matched`.`Recruitment Season` = `SR_Plan_with_Calculation`.`Recruitment Season`))))
        left join `Re_Enrollment_Summary_By_School` ON ((`School`.`mongo_id` = `Re_Enrollment_Summary_By_School`.`School Mongo ID`)))
        left join `Matched_Host_by_Connected_School_Prep` ON (((`School`.`mongo_id` = `Matched_Host_by_Connected_School_Prep`.`Connected School Mongo ID`)
            and (`Matched_Host_by_Connected_School_Prep`.`School Year` = (case
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat((year(curdate()) - 1), '/', right(year(curdate()), 2))
            else concat(year(curdate()), '/', (right(year(curdate()), 2) + 1))
        end)))))
        left join `School_Season` ON (((`School_Season`.`school_mongo_id` = `School`.`mongo_id`)
            and (`School_Season`.`season` = (select 
                `Current_Recruitment_Season`.`Current Recruitment Season`
            from
                `Current_Recruitment_Season`)))))
    where
        ((`School`.`short_name` not in ('TS' , 'GS', 'TEST SCHOOL'))
            and (`School`.`school_status` in ('Active' , 'Terminated_Active')))