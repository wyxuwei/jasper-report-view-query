CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Compliance_Tracking_Report` AS
    select 
        `Host`.`mongo_id` AS `Host Mongo ID`,
        `Host`.`record_id` AS `Host ID`,
        `Host`.`primary_contact_name` AS `Primary Host Family Name`,
        `Host_Season`.`school_year` AS `School Year`,
        `Host_Season`.`status` AS `Host Family Season Status`,
        `Host`.`status` AS `Host Family Compliance Status`,
        `School`.`short_name` AS `Connected school Short Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `School`.`region` AS `School Region`,
        `Host`.`Latest_Background_Check_Completion_Date` AS `Background Check Completion Date(Latest)`,
        `Host`.`most_recent_expiring_bgc_date` AS `Background Check Expiration Date (Earliest)`,
        `Host`.`Primary_Host_Training_Completed_Date` AS `Training Completed Date`,
        (case
            when (`Host_Season`.`Alternative_Method_For_Contract` = '1') then 'Althernative Method'
            when (`Host_Season`.`Alternative_Method_For_Contract` = '0') then 'DocuSign'
            else `Host_Season`.`Alternative_Method_For_Contract`
        end) AS `Contract Sent Via`,
        `Host_Season`.`DocuSign_Contract_Status` AS `DocuSign Contract Status`,
        `Host_Season`.`Contract_Url` AS `Contract Uploaded(URL)`,
        `Host_Season`.`Matched_Students_With_Previous_Host_And_Selected` AS `Matched Student Info`,
        `Host`.`payee_name` AS `Payee Name`,
        concat('https://www.ciiedu.net/ciie.html#/hostfamily/hostfamilyDetail/compliance?hostfamilyId=',
                `Host`.`mongo_id`) AS `Host Family Compliance Page URL`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `Host`.`another_member_name` AS `Secondary Host Member Name`,
        `Host`.`primary_email_address` AS `Host Family Email Address(Hidden)`,
        `Host`.`primary_phone` AS `Primary Host Phone Number(Hidden)`,
        cast(`SR_Plan`.`new_arrival_date` as date) AS `New Student Arrival Date`,
        cast(`SR_Plan`.`returning_arrival_date` as date) AS `Returning Student Arrival Date`,
        `SR_Plan`.`season` AS `Season`
    from
        (((`Host`
        left join `School` ON ((`Host`.`connected_school_mongo_id` = `School`.`mongo_id`)))
        left join `Host_Season` ON ((`Host_Season`.`host_mongo_id` = `Host`.`mongo_id`)))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Host_Season`.`school_year` = `SR_Plan`.`school_year`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
    where
        (`Host_Season`.`Matched_With_Previous_Host_And_Selected` = '1')
    order by `Host`.`mongo_id`