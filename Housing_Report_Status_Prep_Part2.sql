CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Housing_Report_Status_Prep_Part2` AS
    select 
        `Housing_Report_Status_Prep_Part1`.`id` AS `id`,
        `Housing_Report_Status_Prep_Part1`.`student_mongo_id` AS `student_mongo_id`,
        `Housing_Report_Status_Prep_Part1`.`school_mongo_id` AS `school_mongo_id`,
        `Housing_Report_Status_Prep_Part1`.`host_name` AS `host_name`,
        `Housing_Report_Status_Prep_Part1`.`host_email` AS `host_email`,
        `Housing_Report_Status_Prep_Part1`.`resco` AS `resco`,
        `Housing_Report_Status_Prep_Part1`.`resco_email` AS `resco_email`,
        `Housing_Report_Status_Prep_Part1`.`status` AS `status`,
        `Housing_Report_Status_Prep_Part1`.`decision_time` AS `decision_time`,
        `Housing_Report_Status_Prep_Part1`.`month` AS `month`,
        `Housing_Report_Status_Prep_Part1`.`created_on` AS `created_on`,
        `Housing_Report_Status_Prep_Part1`.`submitted_date` AS `submitted_date`,
        `Housing_Report_Status_Prep_Part1`.`last_modified_on` AS `last_modified_on`,
        `Housing_Report_Status_Prep_Part1`.`flag` AS `flag`,
        `Housing_Report_Status_Prep_Part1`.`type` AS `type`
    from
        `Housing_Report_Status_Prep_Part1`
    group by `Housing_Report_Status_Prep_Part1`.`student_mongo_id` , `Housing_Report_Status_Prep_Part1`.`month`