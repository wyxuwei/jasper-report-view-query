CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Agency_Performence` AS
    select 
        `Agency`.`mongo_id` AS `Agency Mongo ID`,
        `Agency`.`agency_id` AS `Agency ID`,
        `Agency`.`name` AS `Agency Name (In Native Language)`,
        `Agency`.`english_name` AS `Agency Name`,
        `Agency`.`city` AS `Agency City (In Native Language)`,
        `Agency`.`city_english` AS `Agency City`,
        count(`Student_To_School`.`mongo_id`) AS `Accumulative Number of Applications Passed Though`,
        count(distinct `Student_To_School`.`student_mongo_id`) AS `Accumulative Number of Applicants Passed Though`,
        count((case `Student_To_School`.`season`
            when
                (case
                    when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
                    when
                        ((month(curdate()) = 1)
                            and (dayofmonth(curdate()) <= 15))
                    then
                        concat(year(curdate()), '__Spring')
                    else concat(year(curdate()), '_Fall')
                end)
            then
                `Student_To_School`.`mongo_id`
            else NULL
        end)) AS `Number of Applications Passed Though in New Season`,
        count(distinct (case `Student_To_School`.`season`
                when
                    (case
                        when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
                        when
                            ((month(curdate()) = 1)
                                and (dayofmonth(curdate()) <= 15))
                        then
                            concat(year(curdate()), '__Spring')
                        else concat(year(curdate()), '_Fall')
                    end)
                then
                    `Student_To_School`.`student_mongo_id`
                else NULL
            end)) AS `Number of Applicants Passed Though in New Season`,
        count(distinct (case `Student_To_School`.`status`
                when 'ENROLLED' then `Student_To_School`.`student_mongo_id`
                else NULL
            end)) AS `Accumulative Number of Applicants Enrolled`,
        count(distinct (case
                when
                    ((`Student_To_School`.`status` = 'ENROLLED')
                        and (`Student_To_School`.`season` = (case
                        when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
                        when
                            ((month(curdate()) = 1)
                                and (dayofmonth(curdate()) <= 15))
                        then
                            concat(year(curdate()), '__Spring')
                        else concat(year(curdate()), '_Fall')
                    end)))
                then
                    `Student_To_School`.`student_mongo_id`
                else NULL
            end)) AS `Number of Applicants Enrolled in New Season`,
        (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '__Spring')
            when
                ((month(curdate()) = 1)
                    and (dayofmonth(curdate()) <= 15))
            then
                concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end) AS `New Season`
    from
        ((`Student_To_School`
        join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        join `Agency` ON ((`Student`.`agency_id` = `Agency`.`agency_id`)))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` <> 'DELETED'))
    group by `Agency`.`agency_id`