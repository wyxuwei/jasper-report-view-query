CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Price_Book_Sub` AS
    select distinct
        `SR_Plan`.`school_mongo_id` AS `School Mongo ID`,
        `SR_Plan`.`school_record_id` AS `School ID`,
        `SR_Plan`.`school_short_name` AS `School Short Name`,
        `SR_Plan`.`season` AS `Recruitment Season`,
        `SR_Plan`.`date_opened` AS `School SR Plan Date Opened`,
        `School`.`city` AS `School City`,
        `School`.`zip` AS `School ZIP`,
        `Tuition_Price_Book`.`stipend_fee` AS `School Stipend Amount`,
        if((locate('Spring', `SR_Plan`.`season`) > 0),
            round((`Tuition_Price_Book`.`stipend_fee` / 6),
                    0),
            round((`Tuition_Price_Book`.`stipend_fee` / 10),
                    0)) AS `School Stipend Monthly Amount`,
        if((`Tuition_Price_Book`.`is_confirmed` = '1'),
            'Yes',
            'No') AS `Tuition Confirmed (Y/N)`,
        max(`Tuition_Price_Book`.`confirmed_date`) AS `Tuition Confirmed Date`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `School`.`region` AS `School Region`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`
    from
        ((`SR_Plan`
        left join `Tuition_Price_Book` ON ((`SR_Plan`.`mongo_id` = `Tuition_Price_Book`.`srplan_id`)))
        left join `School` ON ((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)))
    where
        ((`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW'))
            and (`SR_Plan`.`school_short_name` not in ('TS' , 'GS', 'TEST SCHOOL'))
            and (`Tuition_Price_Book`.`is_confirmed` = '1')
            and (`School`.`is_GP` = '1')
            and (`Tuition_Price_Book`.`stipend_fee` <> 0))
    group by `SR_Plan`.`school_short_name` , `SR_Plan`.`season`
    order by `SR_Plan`.`season` desc , `SR_Plan`.`school_short_name`