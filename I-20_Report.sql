CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `I-20_Report` AS
    select distinct
        `Student_To_School`.`mongo_id` AS `Application Mongo ID`,
        `Student_To_School`.`id` AS `Application ID`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`chinese_name` AS `Student Chinese Name`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        date_format(`Student`.`date_of_birth`, '%m/%d/%Y') AS `Student Date of Birth (MM/DD/YYYY)`,
        `Student`.`passport_citizenship` AS `Student Country of Origin`,
        `Student`.`processing_rep_first_name` AS `Processing Rep First Name`,
        `Student`.`processing_rep_last_name` AS `Processing Rep Last Name`,
        concat(`Student`.`processing_rep_first_name`,
                ' ',
                `Student`.`processing_rep_last_name`) AS `Processing Rep Full Name`,
        `Student`.`processing_rep_office` AS `Processing Rep Office`,
        `Student`.`program_rep_first_name` AS `Program Rep First Name`,
        `Student`.`program_rep_last_name` AS `Program Rep Last Name`,
        concat(`Student`.`program_rep_first_name`,
                ' ',
                `Student`.`program_rep_last_name`) AS `Program Rep Full Name`,
        `Student`.`program_rep_office` AS `Program Rep Office`,
        `School`.`id` AS `School ID`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        `School`.`city` AS `School City`,
        `School`.`mailing_address` AS `School Mailing Address`,
        `School`.`zip` AS `School Zip`,
        `School`.`partnership_type` AS `School Partnership Type`,
        if((`School`.`is_affiliated_with_diocese` = '1'),
            'Yes',
            'No') AS `Affiliated with Diocese (Y/N)`,
        `School`.`diocese_name` AS `Diocese Name (if affiliated)`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`DSO_first_name`,
                ' ',
                `School`.`DSO_last_name`) AS `DSO Full Name`,
        `School`.`DSO_email` AS `DSO Email`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        `Student_To_School`.`deposit_date` AS `Deposit Date of Payment`,
        `Student_To_School`.`tuition_date` AS `Tuition Date of Payment`,
        `Student_To_School`.`i20_sent_date` AS `I-20 Send Out Date`,
        if((isnull(`Student_To_School`.`i20_sent_date`)
                and isnull(`Student_To_School`.`i20_tracking_number`)
                and isnull(`Student_To_School`.`i20_courier`)),
            'Not Sent',
            'Sent') AS `I-20 Status`,
        `Student_To_School`.`i20_tracking_number` AS `I-20 Tracking Number`,
        `Student_To_School`.`i20_courier` AS `I-20 Courier`,
        if((`Student_To_School`.`i20_transferred` = '1'),
            'Yes',
            'No') AS `I-20 Transferred (Y/N)`,
        `Student_To_School`.`in_network_transfer_date` AS `In Network Student Transfer Date`,
        `Student_To_School`.`out_of_network_transfer_date` AS `Out of Network Student Transfer Date`,
        `Student_To_School`.`Turnaround_Student_Decision_To_I20` AS `Turnaround Time Between Deposit Payment and I-20 Sent Out`,
        `Student_To_School`.`Pending_I20_Days` AS `Pending I-20 Send Out Days`,
        cast(`SR_Plan`.`new_arrival_date` as date) AS `New Student Arrival Date`,
        date_format(`SR_Plan`.`new_arrival_date`, '%m/%d/%Y') AS `New Student Arrival Date (MM/DD/YYYY)`,
        cast(`SR_Plan`.`new_latest_arrvial_date` as date) AS `New Student Latest Arrival Date`,
        `SR_Plan`.`first_day_of_class` AS `First Day of Classes`,
        `SR_Plan`.`domestic_orientation_date` AS `Domestic Orientation Date`,
        `Agency`.`english_name` AS `Agency Name`,
        `Agency`.`name` AS `Agency Name in Native Language`,
        if(((`Student_To_School`.`application_type` = 'IN_NETWORK_TRANSFER')
                and (`Student_To_School`.`status` = 'ENROLLED')
                and (right(`Student_To_School`.`season`, 4) = 'Fall')),
            (select 
                    `Previous_Enrolled_School1`.`short_name`
                from
                    (`Student_To_School` `Previous_Enrollment1`
                    left join `School` `Previous_Enrolled_School1` ON ((`Previous_Enrollment1`.`school_mongo_id` = `Previous_Enrolled_School1`.`mongo_id`)))
                where
                    ((`Previous_Enrollment1`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
                        and (`Previous_Enrollment1`.`school_year` = concat((left(`Student_To_School`.`season`, 4) - 1),
                            '/',
                            substr(`Student_To_School`.`season`,
                                3,
                                2)))
                        and (`Previous_Enrollment1`.`status` = 'ENROLLED'))
                limit 1),
            if(((`Student_To_School`.`application_type` = 'IN_NETWORK_TRANSFER')
                    and (`Student_To_School`.`status` = 'ENROLLED')
                    and (right(`Student_To_School`.`season`, 6) = 'Spring')),
                (select 
                        `Previous_Enrolled_School2`.`short_name`
                    from
                        (`Student_To_School` `Previous_Enrollment2`
                        left join `School` `Previous_Enrolled_School2` ON ((`Previous_Enrollment2`.`school_mongo_id` = `Previous_Enrolled_School2`.`mongo_id`)))
                    where
                        ((`Previous_Enrollment2`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
                            and (`Previous_Enrollment2`.`season` = concat((left(`Student_To_School`.`season`, 4) - 1),
                                '_Fall'))
                            and (`Previous_Enrollment2`.`status` = 'ENROLLED'))
                    limit 1),
                NULL)) AS `Transfered out School Short Name`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`
    from
        ((((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Agency` ON ((`Agency`.`agency_id` = `Student`.`agency_id`)))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` <> 'DELETED')
            and (not ((`School`.`short_name` like '%Test%')))
            and (`Student_To_School`.`season` not in ('2015_Fall' , '2015__Spring',
            '2014_Fall',
            '2014__Spring',
            '2013_Fall',
            '2013__Spring',
            '2012_Fall',
            '2012__Spring',
            '2011_Fall',
            '2011__Spring'))
            and (`Student_To_School`.`application_type` not in ('RE_ENROLLING' , 'HOMESTAY_ONLY'))
            and (`Student_To_School`.`status` = 'ENROLLED'))