CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `CRM` AS
    select distinct
        `SchoolCRM`.`school_short_name` AS `School Short Name`,
        `SchoolCRM`.`person_entering_activity` AS `Person Entering Activity`,
        `SchoolCRM`.`activity_type` AS `Activity Type`,
        `SchoolCRM`.`activity_date` AS `Activity Date`,
        `SchoolCRM`.`first_category_type` AS `Activity First(Primary) Category Type`,
        `SchoolCRM`.`all_category_types` AS `Activity All Category Types`,
        `SchoolCRM`.`contact_with` AS `Contact With`,
        `SchoolCRM`.`other_internal_contacts` AS `Other Internal Contacts`,
        `SchoolCRM`.`status` AS `Activity Status`,
        `SchoolCRM`.`first_escalation_receiver` AS `Initial Escalation Receiver`,
        `SchoolCRM`.`first_escalation_time` AS `Initial Escalation Time`,
        `SchoolCRM`.`all_escalations` AS `All Escalation Records`,
        `SchoolCRM`.`number_of_docs` AS `Num of Related Docs`,
        concat('https://www.ciiedu.net/ciie.html#/school/schoolDetail/schoolCRM?schoolId=',
                `SchoolCRM`.`school_id`) AS `Link to School CRM Page`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        `School`.`region` AS `School Region`
    from
        (`SchoolCRM`
        left join `School` ON ((`SchoolCRM`.`school_id` = `School`.`mongo_id`)))