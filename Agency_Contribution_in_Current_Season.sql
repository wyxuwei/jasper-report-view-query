CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Agency_Contribution_in_Current_Season` AS
    select 
        `Agency`.`mongo_id` AS `Agency Mongo ID`,
        `Agency`.`agency_id` AS `Agency ID`,
        `Agency`.`name` AS `Agency Name(Native)`,
        `Agency`.`english_name` AS `Agency Name`,
        count(`Student`.`mongo_id`) AS `Number of Enrolled Students Passed Though in Current Season`
    from
        (`Agency`
        join `Student` ON (((`Student`.`agency_id` = `Agency`.`agency_id`)
            and (`Student`.`current_status` = 'Enrolled'))))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%'))))
    group by `Agency`.`agency_id`