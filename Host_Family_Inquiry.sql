CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Family_Inquiry` AS
    select 
        `Inquiry`.`id` AS `Inquiry Id`,
        `Inquiry`.`mongo_id` AS `Inquiry Mongo ID`,
        `Inquiry`.`first_name` AS `Host Family First Name`,
        `Inquiry`.`last_name` AS `Host Family Last Name`,
        `Inquiry`.`street` AS `Host Family Address`,
        `Inquiry`.`city` AS `Host Family City`,
        `Inquiry`.`state` AS `Host Family State`,
        `Inquiry`.`zip` AS `Host Family Zip Code`,
        `Inquiry`.`phone` AS `Host Family Phone Number`,
        `Inquiry`.`email` AS `Host Family Email Address`,
        `Inquiry`.`how_long_to_host` AS `How Long to Host`,
        `Inquiry`.`provide_transportation` AS `Provide Transportation (Y/N)`,
        `Inquiry`.`separate_bedroom` AS `Separate Bedroom  (Y/N)`,
        `Inquiry`.`failure_reason` AS `Failure Reason`,
        `Inquiry`.`created_date` AS `Created Date`,
        `Inquiry`.`comments` AS `Comments`,
        `Inquiry`.`closest_school_short_name` AS `School Short Name`
    from
        `Inquiry`
    where
        (`Inquiry`.`failure_reason` is not null)