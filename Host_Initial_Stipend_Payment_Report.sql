CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Initial_Stipend_Payment_Report` AS
    select distinct
        `Student_To_Host`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        if(isnull(`School`.`short_name`),
            `Managed_By_School_Info`.`short_name`,
            `School`.`short_name`) AS `Enrolled School Short Name`,
        if(isnull(`School`.`mongo_id`),
            `Managed_By_School_Info`.`school_mongo_id`,
            `School`.`mongo_id`) AS `Enrolled School Mongo ID`,
        `Student_To_School`.`season` AS `Student Recruitment Season`,
        `Student_To_Host`.`school_year` AS `School Year`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Host`.`mongo_id` AS `Host Family Mongo ID`,
        `Host`.`record_id` AS `Host Family ID`,
        `Host`.`primary_contact_name` AS `Host Family Full Name`,
        `Connected_School`.`short_name` AS `Connected School Short Name`,
        `Connected_School`.`mongo_id` AS `Connected School Mongo ID`,
        `School`.`region` AS `School Region`,
        date_format(`SR_Plan`.`new_arrival_date`, '%m/%d/%Y') AS `New Student Arrival Date (MM/DD/YYYY)`,
        date_format(`SR_Plan`.`returning_arrival_date`,
                '%m/%d/%Y') AS `Returning Student Arrival Date (MM/DD/YYYY)`,
        concat(`Connected_School`.`SSM_first_name`,
                ' ',
                `Connected_School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`Connected_School`.`SSC_first_name`,
                ' ',
                `Connected_School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`Connected_School`.`RD_First_Name`,
                ' ',
                `Connected_School`.`RD_Last_Name`) AS `RD Full Name`,
        `Student_To_Host`.`status` AS `Host Matching Status`,
        `Student_To_School`.`student_travel_arrival_time` AS `Student Arrival Time`,
        `Student_To_Host`.`Initial_Stipend_Date` AS `Initial Stipend Date`,
        `Student_To_Host`.`start_date` AS `Move In Date`,
        `Student_To_Host`.`end_date` AS `Move Out Date`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        `Host_Member`.`Member_Id` AS `Host Member ID`,
        if((`Student_To_School`.`Full_Tuition_Paid` = '1'),
            'Yes',
            'No') AS `Student Paid Full Tuition (Y/N)`,
        `Host`.`Payee_Member_Id` AS `Payee Member ID`,
        `Host`.`payee_name` AS `Payee Name`
    from
        (((((((((`Student_To_Host`
        left join `Student` ON ((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Host` ON ((`Student_To_Host`.`host_mongo_id` = `Host`.`mongo_id`)))
        left join `School` `Connected_School` ON ((`Host`.`connected_school_mongo_id` = `Connected_School`.`mongo_id`)))
        left join `Host_Season` ON (((`Student_To_Host`.`host_mongo_id` = `Host_Season`.`host_mongo_id`)
            and (`Student_To_Host`.`school_year` = `Host_Season`.`school_year`))))
        left join `Student_To_School` ON (((`Student_To_Host`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
            and (`Student_To_Host`.`season` = `Student_To_School`.`season`)
            and (`Student_To_Host`.`student_enrolled_school_mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Student_To_School`.`status` = 'ENROLLED'))))
        left join `School` ON ((`Student_To_Host`.`student_enrolled_school_mongo_id` = `School`.`mongo_id`)))
        left join `Managed_By_School_Info` ON ((`Student_To_Host`.`student_mongo_id` = `Managed_By_School_Info`.`student_mongo_id`)))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `Host_Member` ON (((`Host_Member`.`Host_Mongo_Id` = `Student_To_Host`.`host_mongo_id`)
            and (`Host_Member`.`Is_Primary_Member` = '1'))))
    where
        ((`Student_To_Host`.`status` in ('SELECTED' , 'PREVIOUS_HOST', 'TEMPORARY'))
            and ((not ((`School`.`short_name` like '%Test%')))
            or isnull(`School`.`mongo_id`))
            and (not ((`Host`.`primary_contact_name` like '%Dorm')))
            and (`Student_To_Host`.`school_year` = '2017/18'))