CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Managed_By_School_Info` AS
    select distinct
        `Student_To_Host`.`student_mongo_id` AS `student_mongo_id`,
        `Student_To_School`.`school_mongo_id` AS `school_mongo_id`,
        `School`.`short_name` AS `short_name`
    from
        ((`Student_To_Host`
        left join `Student_To_School` ON ((`Student_To_Host`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
    where
        ((`Student_To_School`.`season` = '2017_Fall')
            and (`Student_To_School`.`school_mongo_id` is not null)
            and (`Student_To_School`.`status` = 'MANAGED_BY'))