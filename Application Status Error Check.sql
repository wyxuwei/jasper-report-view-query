CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Application Status Error Check` AS
    select distinct
        (case
            when
                ((isnull(`Student_To_School`.`interview_result`)
                    or (`Student_To_School`.`interview_result` = 'NONE'))
                    and (isnull(`Student_To_School`.`student_decision`)
                    or (`Student_To_School`.`student_decision` = 'NONE'))
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'APPLYING')
                    or (`Student_To_School`.`status` = 'POTENTIAL')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'PENDING')
                    and (isnull(`Student_To_School`.`student_decision`)
                    or (`Student_To_School`.`student_decision` = 'NONE'))
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'APPLYING')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'REJECTED')
                    and (isnull(`Student_To_School`.`student_decision`)
                    or (`Student_To_School`.`student_decision` = 'NONE'))
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'REJECTED')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'CONDITIONAL')
                    and (isnull(`Student_To_School`.`student_decision`)
                    or (`Student_To_School`.`student_decision` = 'NONE'))
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'CONDITIONAL_ACCEPTANCE')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'APPROVED')
                    and (isnull(`Student_To_School`.`student_decision`)
                    or (`Student_To_School`.`student_decision` = 'NONE'))
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'ACCEPTED')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'CONDITIONAL')
                    and (`Student_To_School`.`student_decision` = 'SELECTED'))
            then
                ((`Student_To_School`.`status` = 'SELECTED')
                    or (`Student_To_School`.`status` = 'ENROLLED')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'APPROVED')
                    and (`Student_To_School`.`student_decision` = 'SELECTED'))
            then
                ((`Student_To_School`.`status` = 'SELECTED')
                    or (`Student_To_School`.`status` = 'ENROLLED')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'CONDITIONAL')
                    and (`Student_To_School`.`student_decision` = 'REJECTED')
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'DECLINED')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'APPROVED')
                    and (`Student_To_School`.`student_decision` = 'REJECTED')
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                ((`Student_To_School`.`status` = 'DECLINED')
                    or (`Student_To_School`.`status` = 'ABANDONED'))
            when
                ((`Student_To_School`.`interview_result` = 'NA')
                    and (isnull(`Student_To_School`.`student_decision`)
                    or (`Student_To_School`.`student_decision` = 'NONE'))
                    and (isnull(`Student_To_School`.`deposit_date`)
                    or isnull(`Student_To_School`.`tuition_date`)))
            then
                (`Student_To_School`.`status` = 'ABANDONED')
            when (`Student_To_School`.`abandon_reason` is not null) then (`Student_To_School`.`status` = 'ABANDONED')
            else 0
        end) AS `Applciation Status Error`,
        `Student_To_School`.`mongo_id` AS `Application Mongo ID`,
        `Student_To_School`.`id` AS `Application ID`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`abandon_reason` AS `Abandon Reason`,
        `Student_To_School`.`applying_grade` AS `Student Applying Grade`,
        `Student_To_School`.`enrolled_grade` AS `Student Enrolled Grade`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        date_format(`Student`.`date_of_birth`, '%m/%d/%Y') AS `Student Date of Birth (MM/DD/YYYY)`,
        `Student`.`passport_citizenship` AS `Student Country of Origin`,
        `Student`.`current_enrolled_grade` AS `Student Current Grade`,
        `Student`.`language_test` AS `English Proficiency Test Score`,
        `Student`.`current_status` AS `Student Enrolled Status`,
        `Student`.`current_enrolled_school_short_name` AS `Student Enrolled School Short Name`,
        `Student`.`processing_rep_first_name` AS `Processing Rep First Name`,
        `Student`.`processing_rep_last_name` AS `Processing Rep Last Name`,
        concat(`Student`.`processing_rep_first_name`,
                ' ',
                `Student`.`processing_rep_last_name`) AS `Processing Rep Full Name`,
        `Student`.`program_rep_first_name` AS `Program Rep First Name`,
        `Student`.`program_rep_last_name` AS `Program Rep Last Name`,
        concat(`Student`.`program_rep_first_name`,
                ' ',
                `Student`.`program_rep_last_name`) AS `Program Rep Full Name`,
        `Student`.`program_rep_office` AS `Program Rep Office`,
        `Student`.`customer_service_rep_first_name` AS `Customer Service Rep First Name`,
        `Student`.`customer_service_rep_last_name` AS `Customer Service Rep Last Name`,
        concat(`Student`.`customer_service_rep_first_name`,
                ' ',
                `Student`.`customer_service_rep_last_name`) AS `Customer Service Rep Full Name`,
        `School`.`id` AS `School ID`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        `School`.`city` AS `School City`,
        `School`.`mailing_address` AS `School Mailing Address`,
        `School`.`zip` AS `School Zip`,
        `School`.`partnership_type` AS `School Partnership Type`,
        if((`School`.`is_affiliated_with_diocese` = '1'),
            'Yes',
            'No') AS `Affiliated with Diocese (Y/N)`,
        `School`.`diocese_name` AS `Diocese Name (if affiliated)`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`school_manager_TL_first_name` AS `Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Regional Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `School`.`SSM_first_name` AS `SSM First Name`,
        `School`.`SSM_last_name` AS `SSM Last Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        if((`Student_To_School`.`status` = 'ABANDONED'),
            'Yes',
            'No') AS `Abandon (Y/N)`,
        `Student_To_School`.`interview_date` AS `Interview Date`,
        `Student_To_School`.`interview_result` AS `School Decision`,
        `Student_To_School`.`reason_for_school_reject` AS `School Reject Reason`,
        `Student_To_School`.`interview_school_decision_date` AS `Interview Result Upload Date`,
        `Student_To_School`.`student_decision` AS `Student Decision`,
        `Student_To_School`.`reason_for_student_reject` AS `Student Reject Reason`,
        `Student_To_School`.`Reason_for_student_reject_other` AS `Student Other Reject Reason`,
        `Student_To_School`.`application_fee_amount` AS `Application Fee Amount`,
        `Student_To_School`.`application_fee_date` AS `Application Fee Date of Payment`,
        `Student_To_School`.`deposit_amount` AS `Deposit Amount`,
        `Student_To_School`.`deposit_date` AS `Deposit Date of Payment`,
        (to_days(now()) - to_days(`Student_To_School`.`deposit_date`)) AS `Days Since Enrollment`,
        `Student_To_School`.`tuition_amount` AS `Tuition Amount`,
        `Student_To_School`.`tuition_date` AS `Tuition Date of Payment`,
        `Student_To_School`.`ssa_queue_app_status` AS `Application Workflow Status`,
        `Student_To_School`.`ssa_queue_app_created_on` AS `Application Date`,
        `Student_To_School`.`ssa_queue_school_decision_status` AS `School Decision Workflow Status`,
        `Student_To_School`.`ssa_queue_school_decision_created_on` AS `School Decision Date`,
        `Student_To_School`.`ssa_queue_student_decision_status` AS `Student Decision Workflow Status`,
        `Student_To_School`.`ssa_queue_student_decision_created_on` AS `Student Decision Date`,
        `Student_To_School`.`ssa_queue_i20_status` AS `I-20 Workflow Status`,
        `Student_To_School`.`ssa_queue_i20_created_on` AS `I-20 Date`,
        `Student_To_School`.`i20_sent_date` AS `I-20 Send Out Date`,
        `Student_To_School`.`ssa_queue_mat_pak_status` AS `Mat-Pak Workflow Status`,
        `Student_To_School`.`ssa_queue_mat_pak_created_on` AS `Mat-Pak Date`,
        `Student_To_School`.`i20_tracking_number` AS `I-20 Tracking Number`,
        `Student_To_School`.`i20_courier` AS `I-20 Courier`,
        (to_days(`Student_To_School`.`i20_sent_date`) - to_days(`Student_To_School`.`deposit_date`)) AS `Turnaround Time Between Deposit Payment and I-20 Sent Date`,
        `Student_To_School`.`student_decision_deadline_date` AS `Student Decision Deadline`,
        if((`Student_To_School`.`extension_requested` = '1'),
            'Yes',
            'No') AS `Extension Request (Y/N)`,
        (to_days(`Student_To_School`.`student_decision_deadline_date`) - to_days(`Student_To_School`.`interview_school_decision_date`)) AS `Turnaround Time Between School and Student Decision Deadline`,
        `Student_To_School`.`student_travel_pick_up_person_first_name` AS `Pick-up Person First Name`,
        `Student_To_School`.`student_travel_pick_up_person_last_name` AS `Pick-up Person Last Name`,
        concat(`Student_To_School`.`student_travel_pick_up_person_first_name`,
                ' ',
                `Student_To_School`.`student_travel_pick_up_person_last_name`) AS `Pick-up Person Full Name`,
        `Student_To_School`.`student_travel_departure_city` AS `Departure City`,
        `Student_To_School`.`student_travel_departure_time` AS `Departure Time`,
        `Student_To_School`.`student_travel_arrvial_city` AS `Arrival City`,
        `Student_To_School`.`student_travel_arrival_time` AS `Arrival Time`,
        `Student_To_School`.`student_travel_arrvial_city_time_zone` AS `Arrival City Time Zone`,
        `Student_To_School`.`student_travel_arrvial_flight` AS `Arrival Flight`,
        `Student_To_School`.`student_travel_needs_pick_up` AS `Pick Up Needed`,
        `Student_To_School`.`student_travel_pick_up_form_link` AS `Pick Up Form URL`,
        `Student_To_School`.`student_travel_parent_accompany` AS `Student Parent Accompany`,
        `Student_To_School`.`student_travel_note` AS `Travel Notes`,
        substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                        ',',
                        1),
                ': ',
                -(1)) AS `Flight NO.1`,
        substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                        ', ',
                        2),
                ': ',
                -(1)) AS `Flight NO.1 From`,
        substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                        '; ',
                        1),
                ': ',
                -(1)) AS `Flight NO.1 To`,
        if((locate('; ',
                    `Student_To_School`.`student_travel_flight_information`) = 0),
            NULL,
            substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                            ', ',
                            3),
                    ': ',
                    -(1))) AS `Flight NO.2`,
        if((locate('; ',
                    `Student_To_School`.`student_travel_flight_information`) = 0),
            NULL,
            substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                            ', ',
                            4),
                    ': ',
                    -(1))) AS `Flight NO.2 From`,
        if((locate('; ',
                    `Student_To_School`.`student_travel_flight_information`) = 0),
            NULL,
            substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                            '; ',
                            2),
                    ': ',
                    -(1))) AS `Flight NO.2 To`,
        if((locate('; ',
                    `Student_To_School`.`student_travel_flight_information`,
                    (locate('; ',
                            `Student_To_School`.`student_travel_flight_information`) + 1)) = 0),
            NULL,
            substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                            ', ',
                            5),
                    ': ',
                    -(1))) AS `Flight NO.3`,
        if((locate('; ',
                    `Student_To_School`.`student_travel_flight_information`,
                    (locate('; ',
                            `Student_To_School`.`student_travel_flight_information`) + 1)) = 0),
            NULL,
            substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                            ', ',
                            6),
                    ': ',
                    -(1))) AS `Flight NO.3 From`,
        if((locate('; ',
                    `Student_To_School`.`student_travel_flight_information`,
                    (locate('; ',
                            `Student_To_School`.`student_travel_flight_information`) + 1)) = 0),
            NULL,
            substring_index(substring_index(`Student_To_School`.`student_travel_flight_information`,
                            '; ',
                            3),
                    ': ',
                    -(1))) AS `Flight NO.3 To`,
        if((`Student_To_School`.`safety_arrival` = '1'),
            'Yes',
            'No') AS `Student Safe Arrival (Y/N)`,
        `Student_To_School`.`insurance_approved` AS `Insurance Approved`,
        `Student_To_School`.`insurance_purchased` AS `Insurance Purchased`,
        `SR_Plan`.`status` AS `SR Plan Status`,
        cast(`SR_Plan`.`new_arrival_date` as date) AS `New Student Arrival Date`,
        date_format(`SR_Plan`.`new_arrival_date`, '%m/%d/%Y') AS `New Student Arrival Date (MM/DD/YYYY)`,
        (`SR_Plan`.`new_arrival_date` + interval 10 month) AS `10 months Since New Student Arrival`,
        date_format((`SR_Plan`.`new_arrival_date` + interval 10 month),
                '%m/%d/%Y') AS `10 months Since New Student Arrival (MM/DD/YYYY)`,
        cast(`SR_Plan`.`new_latest_arrvial_date` as date) AS `New Student Latest Arrival Date`,
        cast(`SR_Plan`.`returning_arrival_date` as date) AS `Returning Student Arrival Date`,
        date_format(`SR_Plan`.`returning_arrival_date`,
                '%m/%d/%Y') AS `Returning Student Arrival Date (MM/DD/YYYY)`,
        (`SR_Plan`.`returning_arrival_date` + interval 10 month) AS `10 months Since Returning Student Arrival`,
        date_format((`SR_Plan`.`returning_arrival_date` + interval 10 month),
                '%m/%d/%Y') AS `10 months Since Returning Student Arrival (MM/DD/YYYY)`,
        cast(`SR_Plan`.`returning_latest_arrvial_date`
            as date) AS `Returning Student Latest Arrival Date`,
        `SR_Plan`.`first_day_of_class` AS `First Day of Classes`,
        `Agency`.`agency_id` AS `Agency ID`,
        `Agency`.`country` AS `Agency Country`,
        `Agency`.`city_english` AS `Agency City`,
        `Agency`.`english_name` AS `Agency Name`,
        `Agency`.`name` AS `Agency Name in Native Language`,
        `Agency`.`phone` AS `Agency Phone Number`,
        `Agency`.`email` AS `Agency Email Address`,
        if(((select distinct
                    `Student_To_Host`.`status`
                from
                    `Student_To_Host`
                where
                    ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                        and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                        and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                        and (`Student_To_Host`.`status` = 'SELECTED'))) = 'SELECTED'),
            'SELECTED',
            if(((select distinct
                        `Student_To_Host`.`status`
                    from
                        `Student_To_Host`
                    where
                        ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                            and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                            and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                            and (`Student_To_Host`.`status` = 'PREVIOUS_HOST'))) = 'PREVIOUS_HOST'),
                'PREVIOUS_HOST',
                if(((select distinct
                            `Student_To_Host`.`status`
                        from
                            `Student_To_Host`
                        where
                            ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                                and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                                and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                                and (`Student_To_Host`.`status` = 'DECLINED'))) = 'DECLINED'),
                    'DECLINED',
                    if(((select distinct
                                `Student_To_Host`.`status`
                            from
                                `Student_To_Host`
                            where
                                ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                                    and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                                    and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                                    and (`Student_To_Host`.`status` = 'POTENTIAL'))) = 'POTENTIAL'),
                        'POTENTIAL',
                        if(((select distinct
                                    `Student_To_Host`.`status`
                                from
                                    `Student_To_Host`
                                where
                                    ((`Student_To_School`.`student_mongo_id` = `Student_To_Host`.`student_mongo_id`)
                                        and (`Student_To_School`.`school_mongo_id` = `Student_To_Host`.`student_enrolled_school_mongo_id`)
                                        and (`Student_To_School`.`season` = `Student_To_Host`.`season`)
                                        and (`Student_To_Host`.`status` = 'EXPIRED'))) = 'EXPIRED'),
                            'EXPIRED',
                            NULL))))) AS `Host Family Matching Status for Current Recruitment Season`
    from
        ((((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Agency` ON ((`Agency`.`agency_id` = `Student`.`agency_id`)))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`)
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` <> 'DELETED'))