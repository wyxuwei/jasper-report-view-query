CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Academic_Services_Team_Leads_Report` AS
    select 
        `Student_To_School`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `School`.`short_name` AS `School Short Name`,
        `Student_To_School`.`interview_result` AS `School Decision`,
        `School`.`region` AS `School Region`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS(Y/N)`,
        `Student_To_School`.`enrolled_grade` AS `Student Enrolled Grade`,
        `Student_To_School`.`Latest_Academic_Progress_Report_Type` AS `Academic Progress Report Type`,
        `Student_To_School`.`Latest_Academic_Progress_Report_Date_Created` AS `Academic Progress Report Date Created`,
        if((`Student_To_School`.`Meet_Academic_Guidelines` = '1'),
            'Yes',
            'No') AS `Did this student meet our Academic Guidelines? (Y/N)`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        `School`.`academic_support_program_types` AS `Academic Service Program Type`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `Grievance`.`status` AS `Grievance_Status`,
        if((((`Grievance`.`All_Complain_Category` like '%Academic Services%')
                or (`Grievance`.`All_Complain_Category` like '%Academic Issues%'))
                and (`Grievance`.`first_complain_subcategory` = 'Recommended for Tutoring')),
            concat('https://www.ciiedu.net/ciie.html#/student/studentGrievance?studentId=',
                    `Grievance`.`student_mongo_id`,
                    '&grievanceId=',
                    `Grievance`.`mongo_id`),
            '') AS `Link to Student Support Cases`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`
    from
        (((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Grievance` ON (((`Grievance`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
            and (`Grievance`.`school_year` = `Student_To_School`.`school_year`)
            and (`Grievance`.`school_mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Grievance`.`first_complain_category` = 'Academic Services')
            and (`Grievance`.`first_complain_subcategory` = 'Recommended for Tutoring'))))
    where
        ((`Student_To_School`.`status` = 'ENROLLED')
            and (`Student_To_School`.`Meet_Academic_Guidelines` = 0))
    order by `Student_To_School`.`student_mongo_id`