CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Current_Enrollment` AS
    select distinct
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`email` AS `Student Email Address(Hidden)`,
        `Student`.`new_phone_number` AS `Student US Cell Number(Hidden)`,
        `Student`.`wechat` AS `Student WeChat(Hidden)`,
        `Student`.`current_enrolled_grade` AS `Student Current Grade`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        timestampdiff(YEAR,
            `Student`.`date_of_birth`,
            now()) AS `Student Age`,
        `Student`.`passport_citizenship` AS `Student Country of Citizenship`,
        `Student`.`program_rep_first_name` AS `Program Rep First Name`,
        `Student`.`program_rep_last_name` AS `Program Rep Last Name`,
        concat(`Student`.`program_rep_first_name`,
                ' ',
                `Student`.`program_rep_last_name`) AS `Program Rep Full Name`,
        concat(`Student`.`customer_service_rep_first_name`,
                ' ',
                `Student`.`customer_service_rep_last_name`) AS `Customer Service Rep Full Name`,
        `Student`.`customer_service_rep_office` AS `Customer Service Rep Office`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`id` AS `School ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`city` AS `School City`,
        `School`.`state` AS `School State`,
        `School`.`region` AS `School Region`,
        `School`.`school_status` AS `School Partnership Status`,
        `School`.`partnership_type` AS `School Partnership Type`,
        `School`.`school_manager_TL_first_name` AS `Regional Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Regional Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Regional Manager Full Name`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        if((`School`.`is_student_recruitment` = '1'),
            'Yes',
            'No') AS `Student Recruitment (Y/N)`,
        `School`.`first_recruitment_season` AS `First Recruitment Season`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Agency`.`english_name` AS `Agency Name`,
        `Agency`.`city_english` AS `Agency City`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student`.`Father_Wechat` AS `Father Wechat`,
        `Student`.`Mother_Wechat` AS `Mother Wechat`
    from
        (((`Student`
        left join `School` ON ((`School`.`mongo_id` = `Student`.`current_enrolled_school_mongo_id`)))
        left join `Student_To_School` ON (((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)
            and (`Student_To_School`.`status` = 'Enrolled')
            and ((`Student_To_School`.`season` = (case
            when (month(curdate()) < 9) then concat(year(curdate()), '__Spring')
            else concat(year(curdate()), '_Fall')
        end))
            or (`Student_To_School`.`season` = (case
            when (month(curdate()) < 9) then concat((year(curdate()) - 1), '_Fall')
            else concat(year(curdate()), '_Fall')
        end))))))
        left join `Agency` ON ((`Student`.`agency_id` = `Agency`.`agency_id`)))
    where
        ((`Student`.`current_status` = 'Enrolled')
            and (not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (not ((`School`.`short_name` like '%Test%'))))