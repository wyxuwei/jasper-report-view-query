CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Application_In_Pipeline` AS
    select 
        count(1) AS `number_of_application_in_pipeline`,
        `Student_To_School`.`school_mongo_id` AS `school_mongo_id`,
        `Student_To_School`.`season` AS `season`
    from
        `Student_To_School`
    where
        ((`Student_To_School`.`status` in ('POTENTIAL' , 'APPLYING',
            'ACCEPTED',
            'CONDITIONAL_ACCEPTANCE',
            'SELECTED'))
            and (`Student_To_School`.`application_type` not in ('GPHOMESTAY_ONLY' , 'HOMESTAY_ONLY', 'RE_ENROLLING')))
    group by `Student_To_School`.`school_mongo_id` , `Student_To_School`.`season`