CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Agency_Master_Table` AS
    select 
        `Agency`.`mongo_id` AS `Agency Mongo ID`,
        `Agency`.`agency_id` AS `Agency ID`,
        `Agency`.`english_name` AS `Agency Name`,
        `Agency`.`name` AS `Agency Name (In Native Language)`,
        `Agency`.`status` AS `Agency Status`,
        `Agency`.`address` AS `Agency Address`,
        `Agency`.`city` AS `Agency City`,
        `Agency`.`country` AS `Agency Country`,
        `Agency`.`phone` AS `Agency Phone #`,
        `Agency`.`email` AS `Agency Email`,
        `Agency_Contribution_in_Current_Season`.`Number of Enrolled Students Passed Though in Current Season` AS `Number of Enrolled Students Passed Though in Current Season`,
        `Agency_Performence`.`Accumulative Number of Applications Passed Though` AS `Accumulative Number of Applications Passed Though`,
        `Agency_Performence`.`Accumulative Number of Applicants Passed Though` AS `Accumulative Number of Applicants Passed Though`,
        `Agency_Performence`.`Number of Applications Passed Though in New Season` AS `Number of Applications Passed Though in New Season`,
        `Agency_Performence`.`Number of Applicants Passed Though in New Season` AS `Number of Applicants Passed Though in New Season`,
        `Agency_Performence`.`Accumulative Number of Applicants Enrolled` AS `Accumulative Number of Applicants Enrolled`,
        `Agency_Performence`.`Number of Applicants Enrolled in New Season` AS `Number of Applicants Enrolled in New Season`,
        `Agency_Performence`.`New Season` AS `New Season`
    from
        ((`Agency`
        left join `Agency_Contribution_in_Current_Season` ON ((`Agency`.`mongo_id` = `Agency_Contribution_in_Current_Season`.`Agency Mongo ID`)))
        left join `Agency_Performence` ON ((`Agency`.`mongo_id` = `Agency_Performence`.`Agency Mongo ID`)))