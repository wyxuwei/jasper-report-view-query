CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Host_Family_Stipend_Updated` AS
    select distinct
        `Host`.`mongo_id` AS `Host Family Mongo ID`,
        `Host`.`record_id` AS `Host Family ID`,
        `Host`.`primary_contact_name` AS `Host Family Full Name`,
        `Student_To_Host`.`school_year` AS `School Year`,
        `Student_To_School`.`season` AS `Student Recruitment Season`,
        `Student_To_Host`.`student_mongo_id` AS `Student Mongo ID`,
        `Student`.`record_id` AS `Student ID`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        if(isnull(`School`.`short_name`),
            `Managed_By_School_Info`.`short_name`,
            `School`.`short_name`) AS `Enrolled School Short Name`,
        `Connected_School`.`short_name` AS `Connected School Short Name`,
        `School`.`region` AS `School Region`,
        concat(`Connected_School`.`SSM_first_name`,
                ' ',
                `Connected_School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`Connected_School`.`SSC_first_name`,
                ' ',
                `Connected_School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`Connected_School`.`RD_First_Name`,
                ' ',
                `Connected_School`.`RD_Last_Name`) AS `RD Full Name`,
        `Student_To_Host`.`status` AS `Hosting Status`,
        if((right(`Student_To_School`.`season`, 4) = 'Fall'),
            round((`Student_To_School`.`tpb_stipend_fee` / 10),
                    2),
            round((`Student_To_School`.`tpb_stipend_fee` / 6),
                    2)) AS `Student Paid Monthly Stipend Amount`,
        `Student_To_Host`.`start_date` AS `Move In Date`,
        `Student_To_Host`.`end_date` AS `Move Out Date`,
        `Student_To_Host`.`Nights_In_May_Former` AS `Nights Stay With Host In May Of Last Season`,
        `Student_To_Host`.`Stipend_In_May_Former` AS `Stipend To Pay In May Of Last Season`,
        `Student_To_Host`.`Nights_In_Jun_Former` AS `Nights Stay With Host In June Of Last Season`,
        `Student_To_Host`.`Stipend_In_Jun_Former` AS `Stipend To Pay In June Of Last Season`,
        `Student_To_Host`.`Nights_In_Jul_Former` AS `Nights Stay With Host In July Of Last Season`,
        `Student_To_Host`.`Stipend_In_Jul_Former` AS `Stipend To Pay In July of last season`,
        `Student_To_Host`.`Nights_In_Aug_Former` AS `Nights Stay With Host In August Of Current Season`,
        `Student_To_Host`.`Stipend_In_Aug_Former` AS `Stipend To Pay In August Of Current Season`,
        `Student_To_Host`.`Nights_In_Sept_Former` AS `Nights Stay With Host In September Of Current Season`,
        `Student_To_Host`.`Stipend_In_Sept_Former` AS `Stipend To Pay In September Of Current Season`,
        `Student_To_Host`.`Nights_In_Oct_Former` AS `Nights Stay With Host In October Of Current Season`,
        `Student_To_Host`.`Stipend_In_Oct_Former` AS `Stipend To Pay In October Of Current Season`,
        `Student_To_Host`.`Nights_In_Nov_Former` AS `Nights Stay With Host In November Of Current Season`,
        `Student_To_Host`.`Stipend_In_Nov_Former` AS `Stipend To Pay In November Of Current Season`,
        `Student_To_Host`.`Nights_In_Dec_Former` AS `Nights Stay With Host In December Of Current Season`,
        `Student_To_Host`.`Stipend_In_Dec_Former` AS `Stipend To Pay In December Of Current Season`,
        `Student_To_Host`.`Nights_In_Jan_Latter` AS `Nights Stay With Host In January Of Current Season`,
        `Student_To_Host`.`Stipend_In_Jan_Latter` AS `Stipend To Pay In January Of Current Season`,
        `Student_To_Host`.`Nights_In_Feb_Latter` AS `Nights Stay With Host In Febuary Of Current Season`,
        `Student_To_Host`.`Stipend_In_Feb_Latter` AS `Stipend To Pay In Febuary Of Current Season`,
        `Student_To_Host`.`Nights_In_Mar_Latter` AS `Nights Stay With Host In March Of Current Season`,
        `Student_To_Host`.`Stipend_In_Mar_Latter` AS `Stipend To Pay In March Of Current Season`,
        `Student_To_Host`.`Nights_In_Apr_Latter` AS `Nights Stay With Host In April Of Current Season`,
        `Student_To_Host`.`Stipend_In_Apr_Latter` AS `Stipend To Pay In April Of Current Season`,
        `Student_To_Host`.`Nights_In_May_Latter` AS `Nights Stay With Host In May Of Current Season`,
        `Student_To_Host`.`Stipend_In_May_Latter` AS `Stipend To Pay In May Of Current Season`,
        `Student_To_Host`.`Nights_In_Jun_Latter` AS `Nights Stay With Host In June Of Current Season`,
        `Student_To_Host`.`Stipend_In_Jun_Latter` AS `Stipend To Pay In June Of Current Season`,
        `Student_To_Host`.`Nights_In_Jul_Latter` AS `Nights Stay With Host In July Of Current Season`,
        `Student_To_Host`.`Stipend_In_Jul_Latter` AS `Stipend To Pay In July Of Current Season`,
        `Student_To_Host`.`Nights_In_Aug_Latter` AS `Nights Stay With Host In August Of Next Season`,
        `Student_To_Host`.`Stipend_In_Aug_Latter` AS `Stipend To Pay In August Of Next Season`,
        if((`Student_To_Host`.`Start_End_Date_Confirmed` = '1'),
            'Yes',
            'No') AS `Confirmation Status(Y/N)`,
        `Student_To_Host`.`Last_Start_End_Date_Confirmed_By` AS `Last Confirmed By`,
        `Student_To_Host`.`Last_Start_End_Date_Confirmed_On` AS `Last Confirmed On`,
        `School`.`record_id` AS `Enrolled School ID`,
        `Connected_School`.`record_id` AS `Connected School ID`,
        `Host_Member`.`Member_Id` AS `Host Member ID`,
        `Host`.`Payee_Member_Id` AS `Payee Member ID`,
        `Host`.`payee_name` AS `Payee Name`
    from
        ((((((((`Student_To_Host`
        left join `Student` ON ((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `Host` ON ((`Student_To_Host`.`host_mongo_id` = `Host`.`mongo_id`)))
        left join `School` `Connected_School` ON ((`Host`.`connected_school_mongo_id` = `Connected_School`.`mongo_id`)))
        left join `Host_Season` ON (((`Student_To_Host`.`host_mongo_id` = `Host_Season`.`host_mongo_id`)
            and (`Student_To_Host`.`school_year` = `Host_Season`.`school_year`))))
        left join `Student_To_School` ON (((`Student_To_Host`.`student_mongo_id` = `Student_To_School`.`student_mongo_id`)
            and (`Student_To_Host`.`season` = `Student_To_School`.`season`)
            and (`Student_To_Host`.`student_enrolled_school_mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Student_To_School`.`status` = 'ENROLLED'))))
        left join `School` ON ((`Student_To_Host`.`student_enrolled_school_mongo_id` = `School`.`mongo_id`)))
        left join `Managed_By_School_Info` ON ((`Student_To_Host`.`student_mongo_id` = `Managed_By_School_Info`.`student_mongo_id`)))
        left join `Host_Member` ON (((`Host_Member`.`Host_Mongo_Id` = `Student_To_Host`.`host_mongo_id`)
            and (`Host_Member`.`Is_Primary_Member` = '1'))))
    where
        ((`Student_To_Host`.`status` in ('SELECTED' , 'PREVIOUS_HOST', 'TEMPORARY'))
            and ((not ((`School`.`short_name` like '%Test%')))
            or isnull(`School`.`mongo_id`))
            and (not ((`Host`.`primary_contact_name` like '%Dorm')))
            and (`Student_To_Host`.`school_year` = '2017/18'))