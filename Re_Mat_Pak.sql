CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Re_Mat_Pak` AS
    select 
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `Student_To_School`.`status` AS `Application Status`,
        `School`.`id` AS `School ID`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `School`.`partnership_type` AS `School Partnership Type`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        concat(`Student`.`customer_service_rep_first_name`,
                ' ',
                `Student`.`customer_service_rep_last_name`) AS `Customer Service Rep Full Name`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`SR_Plan`.`re_mat_pak_required` = '1'),
            'Yes',
            'No') AS `Re-MatPak Required (Y/N)`,
        `SR_Plan`.`returning_arrival_date` AS `Returning Student Arrival Date`,
        `SR_Plan`.`returning_latest_arrvial_date` AS `Returning Student Latest Arrival Date`,
        if(isnull((select 
                            `Mat_Pak_School`.`mat_pak_combination_mat_pak_type`
                        from
                            `Mat_Pak_School`
                        where
                            ((`Mat_Pak_School`.`SRPlan_id` = `SR_Plan`.`mongo_id`)
                                and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-School Mat-Pak'))
                        limit 1)),
            'No',
            'Yes') AS `Re-School Mat-Pak is needed (Y/N)`,
        `Re_School_Mat_Pak`.`receive_date` AS `Re-School Mat-Pak Upload Date`,
        if((`Re_School_Mat_Pak`.`is_completed` = '1'),
            'Yes',
            'No') AS `Re-School Mat-Pak Completed (Y/N)`,
        if(isnull((select 
                            `Mat_Pak_School`.`mat_pak_combination_mat_pak_type`
                        from
                            `Mat_Pak_School`
                        where
                            ((`Mat_Pak_School`.`SRPlan_id` = `SR_Plan`.`mongo_id`)
                                and (`Mat_Pak_School`.`mat_pak_combination_mat_pak_type` = 'Re-GP Mat-Pak'))
                        limit 1)),
            'No',
            'Yes') AS `Re-GP Mat-Pak is needed (Y/N)`,
        `Re_GP_Mat_Pak`.`receive_date` AS `Re-GP Mat-Pak Upload Date`,
        if((`Re_GP_Mat_Pak`.`is_completed` = '1'),
            'Yes',
            'No') AS `Re-GP Mat-Pak Completed (Y/N)`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`
    from
        (((((`Student`
        left join `Student_To_School` ON ((`Student`.`mongo_id` = `Student_To_School`.`student_mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `SR_Plan` ON (((`Student_To_School`.`school_mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`))))
        left join `Mat_Pak_Student` `Re_School_Mat_Pak` ON (((`Student_To_School`.`mongo_id` = `Re_School_Mat_Pak`.`student_to_school_mongo_id`)
            and (`Re_School_Mat_Pak`.`tag` = 'Re-School Mat-Pak'))))
        left join `Mat_Pak_Student` `Re_GP_Mat_Pak` ON (((`Student_To_School`.`mongo_id` = `Re_GP_Mat_Pak`.`student_to_school_mongo_id`)
            and (`Re_GP_Mat_Pak`.`tag` = 'Re-GP Mat-Pak'))))
    where
        ((`SR_Plan`.`re_mat_pak_required` = '1')
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (`Student_To_School`.`application_type` = 'RE_ENROLLING')
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))