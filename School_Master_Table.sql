CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `School_Master_Table` AS
    select 
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`record_id` AS `School ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`chinese_name` AS `School Chinese Name`,
        `School`.`Ring_Name` AS `School Ring Name`,
        `School`.`Ring_Tier` AS `Ring Tier`,
        `School`.`Distance_Rating` AS `School Ring Distance Rating`,
        `School`.`Academics_Rating` AS `School Academic Rating`,
        `School`.`Price_Rating` AS `School Price Rating`,
        `School`.`School_Size` AS `School Size`,
        `School`.`School_Type` AS `School Student Body Type`,
        `School`.`School_Code` AS `School Code`,
        `School`.`Host_Recruitment_Threshold` AS `Host Inventory Threshold`,
        `School`.`school_manager_TL_first_name` AS `Program Manager First Name`,
        `School`.`school_manager_TL_last_name` AS `Program Manager Last Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        `School`.`school_manager_TL_office` AS `Program Manager Office`,
        `School`.`school_manager_TL_phone` AS `Program Manager Phone Number`,
        `School`.`school_manager_TL_email` AS `Program Manager Email`,
        `School`.`school_manager_TL_wechat` AS `Program Manager WeChat`,
        `School`.`SSC_first_name` AS `SSC First Name`,
        `School`.`SSC_last_name` AS `SSC Last Name`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        `School`.`SSC_office` AS `SSC Office`,
        `School`.`SSC_phone` AS `SSC Phone Number`,
        `School`.`SSC_email` AS `SSC Email`,
        `School`.`SSC_wechat` AS `SSC WeChat`,
        `School`.`SSA_first_name` AS `SSA First Name`,
        `School`.`SSA_last_name` AS `SSA Last Name`,
        concat(`School`.`SSA_first_name`,
                ' ',
                `School`.`SSA_last_name`) AS `SSA Full Name`,
        `School`.`SSA_office` AS `SSA Office`,
        `School`.`SSA_phone` AS `SSA Phone Number`,
        `School`.`SSA_email` AS `SSA Email`,
        `School`.`SSA_wechat` AS `SSA WeChat`,
        `School`.`SSA_TL_first_name` AS `SSA Team Lead First Name`,
        `School`.`SSA_TL_last_name` AS `SSA Team Lead Last Name`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`,
        `School`.`SSA_TL_office` AS `SSA Team Lead Office`,
        `School`.`SSA_TL_phone` AS `SSA Team Lead Phone`,
        `School`.`SSA_TL_email` AS `SSA Team Lead Email`,
        `School`.`SSA_TL_wechat` AS `SSA Team Lead Wechat`,
        `School`.`SSM_first_name` AS `SSM First Name`,
        `School`.`SSM_last_name` AS `SSM Last Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `School`.`SSM_office` AS `SSM Office`,
        `School`.`SSM_phone` AS `SSM Phone Number`,
        `School`.`SSM_email` AS `SSM Email`,
        `School`.`SSM_wechat` AS `SSM WeChat`,
        `School`.`HPC_first_name` AS `HCC First Name`,
        `School`.`HPC_last_name` AS `HCC Last Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`,
        `School`.`HPC_office` AS `HCC Office`,
        `School`.`HPC_phone` AS `HCC Phone Number`,
        `School`.`HPC_email` AS `HCC Email`,
        `School`.`HPC_wechat` AS `HCC WeChat`,
        `School`.`HRC_first_name` AS `HRC First Name`,
        `School`.`HRC_last_name` AS `HRC Last Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        `School`.`HRC_office` AS `HRC Office`,
        `School`.`HRC_phone` AS `HRC Phone Number`,
        `School`.`HRC_email` AS `HRC Email`,
        `School`.`HRC_wechat` AS `HRC WeChat`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        `School`.`city` AS `School City`,
        `School`.`zip` AS `School Zip`,
        `School`.`mailing_address` AS `School Mailing Address`,
        `School`.`web_page` AS `School Web Page`,
        `School`.`target_area` AS `School Target Area`,
        `School`.`partnership_type_china` AS `School Partnership Type Of China`,
        `School`.`partnership_type_vietnam` AS `School Partnership Type Of Vietnam`,
        `School`.`partnership_type_hongkong` AS `School Partnership Type Of HongKong`,
        `School`.`partnership_type_residential` AS `School Partnership Type Of Residential`,
        `School`.`partnership_type_taiwan` AS `School Partnership Type Of TaiWan`,
        `School`.`partnership_type_macau` AS `School Partnership Type Of Macau`,
        `School`.`partnership_type_global` AS `School Partnership Type Of Global`,
        `School`.`partnership_type_korea` AS `School Partnership Type Of Korea`,
        `School`.`partnership_type_original_china` AS `School Partnership Original Type Of China`,
        `School`.`partnership_type_original_vietnam` AS `School Partnership Original Type Of Vietnam`,
        `School`.`partnership_type_original_hongkong` AS `School Partnership Original Type Of HongKong`,
        `School`.`partnership_type_original_residential` AS `School Partnership Original Type Of Residential`,
        `School`.`partnership_type_original_taiwan` AS `School Partnership Original Type Of TaiWan`,
        `School`.`partnership_type_original_macau` AS `School Partnership Original Type Of Macau`,
        `School`.`partnership_type_original_global` AS `School Partnership Original Type Of Global`,
        `School`.`partnership_type_original_korea` AS `School Partnership Original Type Of Korea`,
        `School`.`partnership_type_original_all` AS `School Partnership Original Type Of All`,
        `School`.`partnership_length` AS `Age Of The Partnership`,
        `School`.`school_status` AS `School Partnership Status`,
        `School`.`relationship_status` AS `School Relationship Status`,
        `School`.`agreement_status_startdate` AS `Agreement Term Start Date`,
        `School`.`agreement_enddate` AS `Agreement Term End Date`,
        `School`.`partnership_renewal` AS `Partnership Renewal`,
        `School`.`first_recruitment_season` AS `First Recruitment Season`,
        if((`School`.`is_GP` = '1'),
            'Yes',
            'No') AS `Cambridge Homestay (Y/N)`,
        if((`School`.`is_academic` = '1'),
            'Yes',
            'No') AS `AS School (Y/N)`,
        if((`School`.`is_student_recruitment` = '1'),
            'Yes',
            'No') AS `Student Recruitment (Y/N)`,
        `School`.`student_body_type` AS `Student Body Type`,
        `School`.`religious_affiliation` AS `School Religious Affiliation`,
        `SR_Plan`.`status` AS `School SR Plan Status`,
        `SR_Plan`.`new_arrival_date` AS `New Student Arrival Date`,
        `SR_Plan`.`returning_arrival_date` AS `Returning Student Arrival Date`,
        `SR_Plan`.`new_latest_arrvial_date` AS `New Student Latest Arrival Date`,
        `SR_Plan`.`returning_latest_arrvial_date` AS `Returning Student Latest Arrival Date`,
        if(((`School`.`chinese_exclusivity` = 'Yes')
                or (`School`.`korea_exclusivity` = 'Yes')
                or (`School`.`vietnam_exclusivity` = 'Yes')
                or (`School`.`global_exclusivity` = 'Yes')
                or (`School`.`chinese_exclusivity` = 'Limited')
                or (`School`.`korea_exclusivity` = 'Limited')
                or (`School`.`vietnam_exclusivity` = 'Limited')
                or (`School`.`global_exclusivity` = 'Limited')),
            'Yes',
            'No') AS `Market Exclusivity`,
        `School`.`chinese_exclusivity` AS `Chinese Exclusivity`,
        `School`.`korea_exclusivity` AS `Korea Exclusivity`,
        `School`.`vietnam_exclusivity` AS `Vietnam Exclusivity`,
        `School`.`global_exclusivity` AS `Global Exclusivity`,
        `School`.`current_open_bed_number` AS `Open Beds`,
        `School`.`current_bed_capacity` AS `GP School Housing Capacity`,
        `Current_Academic_Year_School_Program_Size`.`Num of Total Current Students (Inc. Grade 12)` AS `Num of Total Current Students (Inc. Grade 12)`,
        `Current_Academic_Year_School_Program_Size`.`Num of Graduating Seniors` AS `Num of Graduating Seniors`,
        count((case
            when
                ((`Student_To_School`.`application_type` in ('SCHOOL_PLACEMENT_NEW' , 'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
                    '_NETWORK_TRANSFER',
                    'EXISTING_APPLICATION'))
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            else NULL
        end)) AS `Total Enrolled Students`,
        count((case
            when
                ((`Student_To_School`.`application_type` = 'RE_ENROLLING')
                    and (`Student_To_School`.`season` = (select 
                        `Current_Recruitment_Season`.`Current Recruitment Fall Season`
                    from
                        `Current_Recruitment_Season`)))
            then
                1
            else NULL
        end)) AS `Total Re-Enrolled Students`,
        `School`.`priority_level` AS `School Priority Level`,
        `School`.`cluster` AS `School Cluster`,
        `School`.`market_ability_x` AS `School Marketability (X)`,
        `School`.`market_ability_y` AS `School Marketability (Y)`,
        if((`School`.`is_affiliated_with_diocese` = '1'),
            'Yes',
            'No') AS `Affiliated with Diocese (Y/N)`,
        `School`.`diocese_name` AS `Diocese Name (if affiliated)`,
        `School`.`institution_type` AS `Institution Type`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`FEA_First_Name`,
                ' ',
                `School`.`FEA_Last_Name`) AS `FEA Full Name`,
        concat(`School`.`EM_First_Name`,
                ' ',
                `School`.`EM_Last_Name`) AS `EM Full Name`,
        concat(`School`.`ASC_First_Name`,
                ' ',
                `School`.`ASC_Last_Name`) AS `ASC Full Name`
    from
        (((`School`
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`SR_Plan`.`season` = (case
            when (month(curdate()) >= 9) then concat((year(curdate()) + 1), '_Fall')
            else concat(year(curdate()), '_Fall')
        end))
            and (`SR_Plan`.`status` not in ('DELETED' , 'NON_APPROVED', 'NEW')))))
        left join `Current_Academic_Year_School_Program_Size` ON ((`School`.`mongo_id` = `Current_Academic_Year_School_Program_Size`.`School Mongo ID`)))
        left join `Student_To_School` ON (((`School`.`mongo_id` = `Student_To_School`.`school_mongo_id`)
            and (`Student_To_School`.`status` = 'ENROLLED'))))
    where
        (`School`.`short_name` not in ('TEST SCHOOL' , 'MockInt', 'DirectApply', 'Prescreen'))
    group by `School`.`mongo_id`