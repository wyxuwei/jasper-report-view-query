CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Homestay_Independent_Student_Tracking_Report` AS
    select 
        `Student_To_School`.`mongo_id` AS `Application ID`,
        `Student`.`mongo_id` AS `Student ID`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        concat(`Student`.`program_rep_first_name`,
                ' ',
                `Student`.`program_rep_last_name`) AS `Program Rep Full Name`,
        `Student`.`program_rep_office` AS `Program Rep Office`,
        `Student_To_School`.`application_type` AS `Application Type`,
        `Student_To_School`.`status` AS `Application Status`,
        `Student_To_School`.`season` AS `Recruitment Season`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`region` AS `School Region`,
        `Student_To_School`.`created_on` AS `Application Date`,
        concat(`School`.`SSC_first_name`,
                ' ',
                `School`.`SSC_last_name`) AS `SSC Full Name`,
        concat(`School`.`SSM_first_name`,
                ' ',
                `School`.`SSM_last_name`) AS `SSM Full Name`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student_To_School`.`student_decision` AS `Student Decision`,
        `Student_To_School`.`Homestay_Deposit_Amount` AS `Deposit Amount`,
        `Student_To_School`.`Homestay_Deposit_Date` AS `Deposit Date of Payment`,
        `Student_To_School`.`Homestay_Tuition_Amount` AS `Homestay Full Amount`,
        `Student_To_School`.`Homestay_Tuition_Date` AS `Homestay Full Fee Date of Payment`,
        `Student_To_School`.`host_status` AS `Host Family Matching Status`,
        `Agency`.`agency_id` AS `Agency ID`,
        `Agency`.`english_name` AS `Agency Name (in Enghlish)`,
        `Agency`.`name` AS `Agency Name (in Native Language)`,
        `Agency`.`city_english` AS `Agency City`,
        `Agency`.`country` AS `Agency Country`,
        concat(`School`.`RD_First_Name`,
                ' ',
                `School`.`RD_Last_Name`) AS `RD Full Name`,
        concat(`School`.`school_manager_TL_first_name`,
                ' ',
                `School`.`school_manager_TL_last_name`) AS `Program Manager Full Name`,
        concat(`School`.`HRC_first_name`,
                ' ',
                `School`.`HRC_last_name`) AS `HRC Full Name`,
        concat(`School`.`HPC_first_name`,
                ' ',
                `School`.`HPC_last_name`) AS `HCC Full Name`
    from
        (((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `Agency` ON ((`Agency`.`agency_id` = `Student`.`agency_id`)))
    where
        ((`Student_To_School`.`application_type` in ('HOMESTAY_ONLY' , 'GPHOMESTAY_ONLY'))
            and (`Student_To_School`.`status` not in ('DELETED' , 'ATTENDING')))
    order by `Student_To_School`.`student_mongo_id`