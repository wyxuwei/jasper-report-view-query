CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `ciiedbadmin`@`%` 
    SQL SECURITY DEFINER
VIEW `Insurance_Purchasing_Report` AS
    select 
        `Student`.`record_id` AS `Student ID`,
        `Student`.`mongo_id` AS `Student Mongo ID`,
        `Student`.`first_name` AS `Student First Name`,
        `Student`.`last_name` AS `Student Last Name`,
        concat(`Student`.`first_name`,
                ' ',
                `Student`.`last_name`) AS `Student Full Name`,
        `Student`.`gender` AS `Student Gender`,
        `Student`.`date_of_birth` AS `Student Date of Birth`,
        `Student_To_School`.`season` AS `Season`,
        date_format(`Student`.`date_of_birth`, '%m/%d/%Y') AS `Student Date of Birth (MM/DD/YYYY)`,
        `Student_To_School`.`application_type` AS `Application Type`,
        if((not ((`Student_To_School`.`application_type` like 'RE_ENROLLING'))),
            `SR_Plan`.`new_arrival_date`,
            NULL) AS `New Student Arrival Date`,
        if((not ((`Student_To_School`.`application_type` like 'RE_ENROLLING'))),
            date_format(`SR_Plan`.`new_arrival_date`, '%m/%d/%Y'),
            NULL) AS `New Student Arrival Date (MM/DD/YYYY)`,
        if((not ((`Student_To_School`.`application_type` like 'RE_ENROLLING'))),
            (`SR_Plan`.`new_arrival_date` + interval 10 month),
            NULL) AS `10 months Since New Student Arrival`,
        if((not ((`Student_To_School`.`application_type` like 'RE_ENROLLING'))),
            date_format((`SR_Plan`.`new_arrival_date` + interval 10 month),
                    '%m/%d/%Y'),
            NULL) AS `10 months Since New Student Arrival (MM/DD/YYYY)`,
        if((`Student_To_School`.`application_type` = 'RE_ENROLLING'),
            `SR_Plan`.`returning_arrival_date`,
            NULL) AS `Returning Student Arrival Date`,
        if((`Student_To_School`.`application_type` = 'RE_ENROLLING'),
            date_format(`SR_Plan`.`returning_arrival_date`,
                    '%m/%d/%Y'),
            NULL) AS `Returning Student Arrival Date (MM/DD/YYYY)`,
        if((`Student_To_School`.`application_type` = 'RE_ENROLLING'),
            (`SR_Plan`.`returning_arrival_date` + interval 10 month),
            NULL) AS `10 months Since Returning Student Arrival`,
        if((`Student_To_School`.`application_type` = 'RE_ENROLLING'),
            date_format((`SR_Plan`.`returning_arrival_date` + interval 10 month),
                    '%m/%d/%Y'),
            NULL) AS `10 months Since Returning Student Arrival (MM/DD/YYYY)`,
        `School`.`id` AS `School ID`,
        `School`.`mongo_id` AS `School Mongo ID`,
        `School`.`short_name` AS `School Short Name`,
        `School`.`full_name` AS `School Full Name`,
        `School`.`region` AS `School Region`,
        `School`.`state` AS `School State`,
        `School`.`city` AS `School City`,
        'U.S.' AS `School Country`,
        substring_index(`School`.`mailing_address`, ',', 1) AS `School Mailing Address`,
        `School`.`zip` AS `School Zip`,
        `Student`.`passport_citizenship` AS `Student Country of Origin`,
        `Student_To_School`.`insurance_approved` AS `Insurance Approved`,
        `Student_To_School`.`insurance_purchased` AS `Insurance Purchased`,
        `SR_Plan`.`status` AS `SR Plan Status`,
        `Student_To_School`.`deposit_date` AS `Deposit Date of Payment`,
        `Student_To_School`.`tuition_date` AS `Tuition Date of Payment`,
        `Student_To_School`.`residential_type` AS `Residential Choice`,
        `Student`.`email` AS `Student Email`,
        `Host`.`primary_contact_name` AS `Primary Host Name`,
        substring_index(`Host`.`address`, ',', 1) AS `Primary Host Address`,
        `Host`.`city` AS `Primary Host City`,
        `Host`.`state` AS `Primary Host State`,
        `Host`.`zip` AS `Primary Host Zip`,
        concat(`School`.`SSA_TL_first_name`,
                ' ',
                `School`.`SSA_TL_last_name`) AS `SSA Team Lead Full Name`
    from
        (((((`Student_To_School`
        left join `Student` ON ((`Student_To_School`.`student_mongo_id` = `Student`.`mongo_id`)))
        left join `School` ON ((`Student_To_School`.`school_mongo_id` = `School`.`mongo_id`)))
        left join `SR_Plan` ON (((`School`.`mongo_id` = `SR_Plan`.`school_mongo_id`)
            and (`Student_To_School`.`season` = `SR_Plan`.`season`))))
        left join `Student_To_Host` ON (((`Student_To_Host`.`student_mongo_id` = `Student`.`mongo_id`)
            and (`Student_To_Host`.`season` = `Student_To_School`.`season`)
            and (`Student_To_Host`.`status` = 'SELECTED'))))
        left join `Host` ON ((`Host`.`mongo_id` = `Student_To_Host`.`host_mongo_id`)))
    where
        ((not ((`Student`.`first_name` like '%Test%')))
            and (not ((`Student`.`last_name` like '%Test%')))
            and (`Student_To_School`.`status` = 'ENROLLED')
            and (not ((`School`.`short_name` like '%Test%')))
            and ((not ((`Student_To_School`.`insurance_purchased` like 'Yes')))
            or isnull(`Student_To_School`.`insurance_purchased`))
            and (`Student_To_School`.`insurance_approved` in ('Yes, Green Card Holder' , 'Yes',
            'Yes, Student Holds American Passport'))
            and (`Student_To_School`.`application_type` in ('NEW' , 'RE_ENROLLING',
            'EXISTING_APPLICATION',
            'SCHOOL_PLACEMENT_NEW',
            'SCHOOL_PLACEMENT_OUT_OF_NETWORK_TRANSFER',
            'IN_NETWORK_TRANSFER',
            'GPHOMESTAY_ONLY',
            'HOMESTAY_ONLY')))